﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class RayTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Vector3Proxy), typeof(RayProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);

            return;
        }

        [Test]
        public void CanAccessClassProperties()
        {
            string code = @"
                local ray = Ray.__new(Vector3.zero, Vector3.forward)

                local a = ray.origin
                local b = ray.direction

                return a, b
               ";
            try
            {
                DynValue result = addonCreator.DoString(code);

                Ray ray = new Ray(Vector3.zero, Vector3.forward);

                Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(ray.origin)); // a
                Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(ray.direction)); // b
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }


        [Test]
        public void CanAccessPublicMethods()
        {
            string code = @"
                local ray = Ray.__new(Vector3.zero, Vector3.forward)

                local a = ray.GetPoint(10)
                local b = ray.GetHashCode()
                local c = ray.ToString()

                return a, b, c
               ";

            DynValue result = addonCreator.DoString(code);

            Ray ray = new Ray(Vector3.zero, Vector3.forward);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(ray.GetPoint(10f))); // a
            Assert.That((int)result.Tuple[1].Number == ray.GetHashCode()); // b
            Assert.That(result.Tuple[2].String == ray.ToString()); // c
            return;
        }

        [Test]
        public void CanAccessMetaMethodOperators()
        {
            string code = @"
                local ray1 = Ray.__new(Vector3.up, Vector3.forward)
                local ray2 = Ray.__new(Vector3.zero, Vector3.down)
                local ray3 = Ray.__new(Vector3.up, Vector3.forward)

                local a = (ray1 ~= ray2)
                local b = (ray1 == ray2)
                local c = (ray1 == ray3)

                return a, b, c
               ";

            try
            {
                DynValue result = addonCreator.DoString(code);

                RayProxy ray1 = new RayProxy(new Ray(Vector3.up, Vector3.forward));
                RayProxy ray2 = new RayProxy(new Ray(Vector3.zero, Vector3.down));
                RayProxy ray3 = new RayProxy(new Ray(Vector3.up, Vector3.forward));

                Assert.That(result.Tuple[0].Boolean == (ray1 != ray2)); // a
                Assert.That(result.Tuple[1].Boolean == (ray1 == ray2)); // b
                Assert.That(result.Tuple[2].Boolean == (ray1 == ray3)); // c
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }
    }
}
