﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.TestTools;
using AddonCreator;
using NUnit.Framework;
using System.Collections.Generic;


namespace AddonCreator.Tests
{
    public class ProxyTester
    {
        protected ProxyLibrary proxies;

        protected List<Tuple<bool, Type>> proxyListCache = null;


        public ProxyTester()
        {
        }

        public void Setup(IEnumerable<Type> proxiesToAdd)
        {
            proxies = Resources.Load("ProxyLibrary") as ProxyLibrary;

            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);

            proxyListCache = new List<Tuple<bool, Type>>();
            foreach (Type proxyType in proxiesToAdd)
            {
                // Add it to the cached proxy list, we can use this list later to disable proxies if we need to.
                // This is a snapshot of the proxies as they were on disk at start of run.
                bool enabled = proxies.IsEnabled(proxyType);
                proxyListCache.Add(new Tuple<bool, Type>(enabled, proxyType));

                SetEnableProxyOnDisk(proxyType, true);
            }

            return;
        }

        public void Deconstruct()
        {
            if (proxyListCache == null)
            {
                Debug.LogError("Did you forget to call Setup?");
                return;
            }

            foreach (Tuple<bool, Type> proxy in proxyListCache)
            {
                // This proxy was originally disabled, re-disable it.
                if (proxy.Item1 == false)
                {
                    SetEnableProxyOnDisk(proxy.Item2, false);
                }
            }
            return;
        }

        /// <summary>
        ///ssssssssss
        /// </summary>
        /// <param name="proxyType"></param>
        /// <param name="enabled"></param>
        protected void SetEnableProxyOnDisk(Type proxyType, bool enabled)
        {
            proxies.SetProxyEnabled(proxyType, enabled);
            return;
        }
    }
}
