﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class Vector4Tests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Vector4Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            return;
        }

        [Test]
        public void CanAccessConstructorsAndIndexer()
        {
            string code = @"
                local a = Vector4.__new()
                local b = Vector4.__new(1, 2)
                local c = Vector4.__new(1, 2, 3)
                local d = Vector4.__new(c)
                local e = c[0]
                local f = c[1]
                local g = c[2]
                return a, b, c, d, e, f, g
                ";

            DynValue result = addonCreator.DoString(code);
            try
            {
                Assert.That((Vector4Proxy)result.Tuple[0].UserData.Object == new Vector4Proxy());
                Assert.That((Vector4Proxy)result.Tuple[1].UserData.Object == new Vector4Proxy(new Vector4(1f, 2f)));
                Assert.That((Vector4Proxy)result.Tuple[2].UserData.Object == new Vector4Proxy(new Vector4(1f, 2f, 3f)));
                Assert.That((Vector4Proxy)result.Tuple[3].UserData.Object == new Vector4Proxy(new Vector4(1f, 2f, 3f)));
                Assert.That(result.Tuple[4].Number == 1f);
                Assert.That(result.Tuple[5].Number == 2f);
                Assert.That(result.Tuple[6].Number == 3f);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local a = Vector4.Distance(Vector4.one, Vector4.zero)
                local b = Vector4.Dot(Vector4.one, Vector4.zero)
                local c = Vector4.Lerp(Vector4.one, Vector4.zero, 0.5)
                local d = Vector4.LerpUnclamped(Vector4.one, Vector4.zero, 0.5)
                local e = Vector4.Max(Vector4.one, Vector4.zero)
                local f = Vector4.Min(Vector4.one, Vector4.zero)
                local g = Vector4.MoveTowards(Vector4.one, Vector4.zero, 0.5)
                local h = Vector4.Normalize(Vector4.__new(3, 3, 3, 3))
                local i = Vector4.Project(Vector4.one, Vector4.__new(2, 2, 2, 2))
                local j = Vector4.Scale(Vector4.one, Vector4.__new(2, 2, 2, 2))
                local k = Vector4.SqrMagnitude(Vector4.__new(3, 3, 3, 3))

                return a, b, c, d, e, f, g, h, i, j, k";

            DynValue result = addonCreator.DoString(code);

            Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, Vector4.Distance(Vector4.one, Vector4.zero)));
            Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, Vector4.Dot(Vector4.one, Vector4.zero)));
            Assert.That((Vector4Proxy)result.Tuple[2].UserData.Object == new Vector4Proxy(Vector4.Lerp(Vector4.one, Vector4.zero, 0.5f)));
            Assert.That((Vector4Proxy)result.Tuple[3].UserData.Object == new Vector4Proxy(Vector4.LerpUnclamped(Vector4.one, Vector4.zero, 0.5f)));
            Assert.That((Vector4Proxy)result.Tuple[4].UserData.Object == new Vector4Proxy(Vector4.Max(Vector4.one, Vector4.zero)));
            Assert.That((Vector4Proxy)result.Tuple[5].UserData.Object == new Vector4Proxy(Vector4.Min(Vector4.one, Vector4.zero)));
            Assert.That((Vector4Proxy)result.Tuple[6].UserData.Object == new Vector4Proxy(Vector4.MoveTowards(Vector4.one, Vector4.zero, 0.5f)));
            Assert.That((Vector4Proxy)result.Tuple[7].UserData.Object == new Vector4Proxy(Vector4.Normalize(new Vector4(3f, 3f, 3f, 3f))));
            Assert.That((Vector4Proxy)result.Tuple[8].UserData.Object == new Vector4Proxy(Vector4.Project(Vector4.one, new Vector4(2f, 2f, 2f, 2f))));
            Assert.That((Vector4Proxy)result.Tuple[9].UserData.Object == new Vector4Proxy(Vector4.Scale(Vector4.one, new Vector4(2f, 2f, 2f, 2f))));
            Assert.That(result.Tuple[10].Number == Vector4.SqrMagnitude(new Vector4(3f, 3f, 3f, 3f)));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticProperties()
        {
            string code = @"
                local a = Vector4.one
                local b = Vector4.zero
                local c = Vector4.positiveInfinity
                local d = Vector4.negativeInfinity

                return a, b, c, d";

            DynValue result = addonCreator.DoString(code);

            Assert.That((Vector4Proxy)result.Tuple[0].UserData.Object == new Vector4Proxy(Vector4.one));
            Assert.That((Vector4Proxy)result.Tuple[1].UserData.Object == new Vector4Proxy(Vector4.zero));
            Assert.That((Vector4Proxy)result.Tuple[2].UserData.Object == new Vector4Proxy(Vector4.positiveInfinity));
            Assert.That((Vector4Proxy)result.Tuple[3].UserData.Object == new Vector4Proxy(Vector4.negativeInfinity));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessPublicMethods()
        {
            string code = @"
                local v1 = Vector4.__new(2, 4, 6, 8)
                local v2 = Vector4.__new(1, 2, 3, 4)
                local v3 = Vector4.__new(0, 0, 0, 0)

                v1.Normalize()
                v2.Scale(v1)
                v3.Set(4, 5, 6, 7)

                local a = v1.GetHashCode()
                local b = v1.SqrMagnitude()
                local c = v1.ToString()
                local d = v1.Equals(v1) -- true
                local e = v1.Equals(v2) -- false


                return a, b, c, d, e, v1, v2, v3";

            DynValue result = addonCreator.DoString(code);

            Vector4 v1 = new Vector4(2f, 4f, 6f, 8f);
            Vector4 v2 = new Vector4(1f, 2f, 3f, 4f);
            Vector4 v3 = new Vector4(0f, 0f, 0f, 0f);

            v1.Normalize();
            v2.Scale(v1);
            v3.Set(4f, 5f, 6f, 7f);

            Assert.That((int)result.Tuple[0].Number == v1.GetHashCode());
            Assert.That((float)result.Tuple[1].Number == v1.SqrMagnitude());
            Assert.That(result.Tuple[2].String == v1.ToString());
            Assert.That(result.Tuple[3].Boolean == true);
            Assert.That(result.Tuple[4].Boolean == false);


            Assert.That((Vector4Proxy)result.Tuple[5].UserData.Object == new Vector4Proxy(v1));
            Assert.That((Vector4Proxy)result.Tuple[6].UserData.Object == new Vector4Proxy(v2));
            Assert.That((Vector4Proxy)result.Tuple[7].UserData.Object == new Vector4Proxy(v3));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassProperties()
        {
            string code = @"
                local v4 = Vector4.__new(2, 4, 6, 8)

                local a = v4.normalized
                local b = v4.magnitude
                local c = v4.sqrMagnitude

                return a, b, c";

            DynValue result = addonCreator.DoString(code);

            Vector4 v4 = new Vector4(2f, 4f, 6f, 8f);
            Assert.That((Vector4Proxy)result.Tuple[0].UserData.Object == new Vector4Proxy(v4.normalized));
            Assert.That(result.Tuple[1].Number == v4.magnitude);
            Assert.That(result.Tuple[2].Number == v4.sqrMagnitude);

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessOperators()
        {
            string code = @"
                local a = Vector4.Distance(Vector4.one, Vector4.zero)
                local b = Vector4.Distance(Vector4.one, Vector4.zero)
                return a, b";

            DynValue result = addonCreator.DoString(code);

            Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, Vector4.Distance(Vector4.one, Vector4.zero)));
            Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, Vector4.Distance(Vector4.one, Vector4.zero)));

            return;
        }

        [Test]
        public void CanAccessMetaMethodOperators()
        {
            string code = @"
                local a = Vector4.one * 4
                local b = 4 * Vector4.one
                local c = Vector4.one + Vector4.one
                local d = Vector4.one / 4
                local e = Vector4.one - Vector4.one
                local f = Vector4.one == Vector4.one -- true
                local g = Vector4.one ~= Vector4.one -- false
                local h = -Vector4.one

                return a, b, c, d, e, f, g, h
                ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((Vector4Proxy)result.Tuple[0].UserData.Object == new Vector4Proxy(Vector4.one * 4)); // a
            Assert.That((Vector4Proxy)result.Tuple[1].UserData.Object == new Vector4Proxy(4 * Vector4.one)); // b
            Assert.That((Vector4Proxy)result.Tuple[2].UserData.Object == new Vector4Proxy(Vector4.one + Vector4.one)); // c
            Assert.That((Vector4Proxy)result.Tuple[3].UserData.Object == new Vector4Proxy(Vector4.one / 4)); // d
            Assert.That((Vector4Proxy)result.Tuple[4].UserData.Object == new Vector4Proxy(Vector4.one - Vector4.one)); // e
            Assert.That(result.Tuple[5].Boolean == true); // f
            Assert.That(result.Tuple[6].Boolean == false); // g
            Assert.That((Vector4Proxy)result.Tuple[7].UserData.Object == new Vector4Proxy(-Vector4.one)); // h

            return;
        }

    }
}
