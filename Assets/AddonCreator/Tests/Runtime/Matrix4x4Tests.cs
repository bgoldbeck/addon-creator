﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class Matrix4x4Tests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Matrix4x4Proxy), typeof(QuaternionProxy), typeof(Vector3Proxy), typeof(Vector4Proxy), typeof(PlaneProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            //Assert.IsTrue(proxies.IsEnabled(typeof(Matrix4x4Proxy).Name));

            return;
        }


        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticProperties()
        {
            string code = @"
                local a = Matrix4x4.identity
                local b = Matrix4x4.zero
                return a, b";

            DynValue result = addonCreator.DoString(code);
            int a = 0;
            int b = 1;

            Assert.That((Matrix4x4Proxy)result.Tuple[a].UserData.Object == new Matrix4x4Proxy(Matrix4x4.identity));
            Assert.That((Matrix4x4Proxy)result.Tuple[b].UserData.Object == new Matrix4x4Proxy(Matrix4x4.zero));
            return;
        }

        [Test]
        public void CanAccessClassProperties()
        {

            string code = @"
                local mat = Matrix4x4.identity
                mat.SetTRS(Vector3.__new(3, 4, 5), Quaternion.identity, Vector3.one)

                local a = mat.rotation
                local b = mat.lossyScale
                local c = mat.isIdentity
                local d = mat.determinant
                local e = mat.transpose
                local f = mat.decomposeProjection
                local g = mat.inverse

                return a, b, c, d, e, f, g";

            DynValue result = addonCreator.DoString(code);
            Matrix4x4 mat = Matrix4x4.identity;
            mat.SetTRS(new Vector3(3f, 4f, 5f), Quaternion.identity, Vector3.one);


            Assert.That((QuaternionProxy)result.Tuple[0].UserData.Object == new QuaternionProxy(mat.rotation)); // a
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(mat.lossyScale)); // b
            Assert.That(result.Tuple[2].Boolean == mat.isIdentity); // c (false)
            Assert.That(result.Tuple[3].Number == mat.determinant); // d
            Assert.That((Matrix4x4Proxy)result.Tuple[4].UserData.Object == new Matrix4x4Proxy(mat.transpose)); // e
            Assert.That(result.Tuple[5].UserData.Object.GetType() == typeof(FrustumPlanes)); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).left == mat.decomposeProjection.left); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).right == mat.decomposeProjection.right); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).zNear == mat.decomposeProjection.zNear); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).zFar == mat.decomposeProjection.zFar); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).top == mat.decomposeProjection.top); // f
            Assert.That(((FrustumPlanes)result.Tuple[5].UserData.Object).bottom == mat.decomposeProjection.bottom); // f
            Assert.That((Matrix4x4Proxy)result.Tuple[6].UserData.Object == new Matrix4x4Proxy(mat.inverse)); // g
            return;
        }

        [Test]
        public void CanAccessStaticPublicMethods()
        {
            string code = @"
                local mat = Matrix4x4.identity
                mat.SetTRS(Vector3.__new(3.0, 4.0, 5.0), Quaternion.identity, Vector3.one)

                local a = Matrix4x4.Determinant(mat)
                local b = Matrix4x4.Frustum(0.0, 4.0, 0.0, 8.0, 0.2, 40.0)
                local c = Matrix4x4.Frustum(mat.decomposeProjection)
                local d = Matrix4x4.Inverse(mat)
                local e, f = Matrix4x4.Inverse3DAffine(mat)
                local g = Matrix4x4.LookAt(Vector3.zero, Vector3.one, Vector3.up)
                local h = Matrix4x4.Ortho(0.0, 1280.0, 0.0, 720.0, 0.1, 100)
                local i = Matrix4x4.Perspective(90.0, 1280/720, 0.1, 100.0)
                local j = Matrix4x4.Rotate(Quaternion.identity)
                local k = Matrix4x4.Scale(Vector3.__new(1, 2, 3))
                local l = Matrix4x4.Translate(Vector3.__new(1, 2, 3))
                local m = Matrix4x4.Transpose(mat)
                local n = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one)

                return a, b, c, d, e, f, g, h, i, j, k, l, m, n
               ";

            DynValue result = addonCreator.DoString(code);
            Matrix4x4 mat = Matrix4x4.identity;
            mat.SetTRS(new Vector3(3f, 4f, 5f), Quaternion.identity, Vector3.one);

            Assert.That(result.Tuple[0].Number == Matrix4x4.Determinant(mat)); // a
            Assert.That((Matrix4x4Proxy)result.Tuple[1].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Frustum(0.0f, 4.0f, 0.0f, 8.0f, 0.2f, 40.0f))); // b
            Assert.That((Matrix4x4Proxy)result.Tuple[2].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Frustum(mat.decomposeProjection))); // c
            Assert.That((Matrix4x4Proxy)result.Tuple[3].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Inverse(mat))); // d
            Matrix4x4 f = Matrix4x4.identity;
            bool e = Matrix4x4.Inverse3DAffine(mat, ref f);

            Assert.That(result.Tuple[4].Boolean == e); // e
            Assert.That((Matrix4x4Proxy)result.Tuple[5].UserData.Object == new Matrix4x4Proxy(f)); // f
            Assert.That((Matrix4x4Proxy)result.Tuple[6].UserData.Object == new Matrix4x4Proxy(Matrix4x4.LookAt(Vector3.zero, Vector3.one, Vector3.up))); // g
            Assert.That((Matrix4x4Proxy)result.Tuple[7].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Ortho(0.0f, 1280.0f, 0.0f, 720.0f, 0.1f, 100f))); // h
            Assert.That((Matrix4x4Proxy)result.Tuple[8].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Perspective(90.0f, 1280f / 720f, 0.1f, 100.0f))); // i
            Assert.That((Matrix4x4Proxy)result.Tuple[9].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Rotate(Quaternion.identity))); // j
            Assert.That((Matrix4x4Proxy)result.Tuple[10].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Scale(new Vector3(1, 2, 3)))); // k
            Assert.That((Matrix4x4Proxy)result.Tuple[11].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Translate(new Vector3(1, 2, 3)))); // l
            Assert.That((Matrix4x4Proxy)result.Tuple[12].UserData.Object == new Matrix4x4Proxy(Matrix4x4.Transpose(mat))); // m
            Assert.That((Matrix4x4Proxy)result.Tuple[13].UserData.Object == new Matrix4x4Proxy(Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one))); // n

            return;
        }

        [Test]
        public void CanAccessPublicMethods()
        {
            string code = @"
                local mat1 = Matrix4x4.identity
                mat1.SetTRS(Vector3.__new(1.0, 2.0, 3.0), Quaternion.identity, Vector3.one)

                local mat2 = Matrix4x4.identity
                mat2.SetRow(0, Vector4.__new(1, 2, 3, 4))

                local a = mat1.Equals(mat2)
                local b = mat1.Equals(mat1)
                local c = mat1.GetColumn(0)
                local d = mat1.GetHashCode()
                local e = mat1.GetRow(0)
                local f = mat1.MultiplyPoint(Vector3.one)
                local g = mat1.MultiplyPoint3x4(Vector3.one)
                local h = mat2.MultiplyVector(Vector3.one)
                local i = mat1.ToString()
                local j = mat1.TransformPlane(Plane.__new(Vector3.up, 2.0))

                return a, b, c, d, e, f, g, h, i, j
               ";

            DynValue result = addonCreator.DoString(code);
            Matrix4x4 mat1 = Matrix4x4.identity;
            mat1.SetTRS(new Vector3(1f, 2f, 3f), Quaternion.identity, Vector3.one);

            Matrix4x4 mat2 = Matrix4x4.identity;
            mat2.SetRow(0, new Vector4(1, 2, 3, 4));

            Assert.That(result.Tuple[0].Boolean == mat1.Equals(mat2)); // a
            Assert.That(result.Tuple[1].Boolean == mat1.Equals(mat1)); // b
            Assert.That((Vector4Proxy)result.Tuple[2].UserData.Object == new Vector4Proxy(mat1.GetColumn(0))); // c
            Assert.That(result.Tuple[3].Number == mat1.GetHashCode()); // d
            Assert.That((Vector4Proxy)result.Tuple[4].UserData.Object == new Vector4Proxy(mat1.GetRow(0))); // e
            Assert.That((Vector3Proxy)result.Tuple[5].UserData.Object == new Vector3Proxy(mat1.MultiplyPoint(Vector3.one))); // f
            Assert.That((Vector3Proxy)result.Tuple[6].UserData.Object == new Vector3Proxy(mat1.MultiplyPoint3x4(Vector3.one))); // g
            Assert.That((Vector3Proxy)result.Tuple[7].UserData.Object == new Vector3Proxy(mat2.MultiplyVector(Vector3.one))); // h
            Assert.That(result.Tuple[8].String == mat1.ToString()); // i
            Assert.That((PlaneProxy)result.Tuple[9].UserData.Object == new PlaneProxy(mat1.TransformPlane(new Plane(Vector3.up, 2f)))); // j
        }
    }
}
