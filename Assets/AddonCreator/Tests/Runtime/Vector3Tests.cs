﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using AddonCreator;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;

namespace AddonCreator.Tests
{
    public class Vector3Tests : ProxyTester
    {
        //private AddonCreator addonCreator;



        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Vector3Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            //GameObject.DestroyImmediate(addonCreator);
            return;
        }

        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(Vector3Proxy)));

            return;
        }

        [Test]
        public void CanAccessConstructors()
        {
            string code = @"
                local a = Vector3.__new()
                local b = Vector3.__new(1, 2)
                local c = Vector3.__new(1, 2, 3)
                local d = Vector3.__new(c)
                return a, b, c, d
                ";

            DynValue result = addonCreator.DoString(code);

            // a
            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy());
            // b
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(new Vector3(1f, 2f)));
            // c
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(new Vector3(1f, 2f, 3f)));
            // d
            Assert.That((Vector3Proxy)result.Tuple[3].UserData.Object == new Vector3Proxy(new Vector3(1f, 2f, 3f)));
            return;
        }

        [Test]
        public void CanAccessIndexer()
        {
            // https://www.moonsharp.org/objects.html#operators

            string code = @"
                local a = Vector3.__new(1, 2, 3)
                local b = a[0]
                local c = a[1]
                local d = a[2]
                return a, b, c, d
            ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(1f, 2f, 3f));
            Assert.That(result.Tuple[1].Number == 1f);
            Assert.That(result.Tuple[2].Number == 2f);
            Assert.That(result.Tuple[3].Number == 3f);

            return;
        }

        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local a = Vector3.Distance(Vector3.left, Vector3.right)
                local b, c, d = Vector3.OrthoNormalize(Vector3.forward, Vector3.up, Vector3.right)
                local e = Vector3.Reflect(Vector3.left, Vector3.up)
                local f = Vector3.RotateTowards(Vector3.forward, Vector3.up, 5.0, 0.0)
                local g, h = Vector3.SmoothDamp(Vector3.one, Vector3.zero, Vector3.forward, 1, 2, 3)

                return a, b, c, d, e, f, g, h";

            DynValue result = addonCreator.DoString(code);
            // a
            Assert.That(((float)result.Tuple[0].Number - Vector3.Distance(Vector3.left, Vector3.right)) <= Vector3Proxy.kEpsilon);

            // b, c, d
            Vector3 b = Vector3.forward;
            Vector3 c = Vector3.up;
            Vector3 d = Vector3.right;

            Vector3.OrthoNormalize(ref b, ref c, ref d);

            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(b));
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(c));
            Assert.That((Vector3Proxy)result.Tuple[3].UserData.Object == new Vector3Proxy(d));

            // e
            Vector3Proxy e = new Vector3Proxy(Vector3.Reflect(Vector3.left, Vector3.up));
            Assert.That((Vector3Proxy)result.Tuple[4].UserData.Object == e);

            // f
            Assert.That((Vector3Proxy)result.Tuple[5].UserData.Object == new Vector3Proxy(Vector3.RotateTowards(Vector3.forward, Vector3.up, 5.0f, 0.0f)));

            // g
            Vector3 cv = Vector3.forward;
            Vector3 smooth = Vector3.SmoothDamp(Vector3.one, Vector3.zero, ref cv, 1f, 2f, 3f);
            Assert.That((Vector3Proxy)result.Tuple[6].UserData.Object == new Vector3Proxy(smooth));
            Assert.That((Vector3Proxy)result.Tuple[7].UserData.Object == new Vector3Proxy(cv));

            return;
        }

        [Test]
        public void CanAccessMetaMethodOperators()
        {
            string code = @"
                local a = Vector3.left * 4
                local b = 4 * Vector3.left
                local c = Vector3.left + Vector3.right
                local d = Vector3.left / 4
                local e = Vector3.left - Vector3.right
                local f = Vector3.left == Vector3.left
                local g = Vector3.right ~= Vector3.left
                local h = -Vector3.right

                return a, b, c, d, e, f, g, h
                ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(Vector3.left * 4)); // a
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(4 * Vector3.left)); // b
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(Vector3.left + Vector3.right)); // c
            Assert.That((Vector3Proxy)result.Tuple[3].UserData.Object == new Vector3Proxy(Vector3.left / 4)); // d
            Assert.That((Vector3Proxy)result.Tuple[4].UserData.Object == new Vector3Proxy(Vector3.left - Vector3.right)); // e
            Assert.That(result.Tuple[5].Boolean == true); // f
            Assert.That(result.Tuple[6].Boolean == true); // g
            Assert.That((Vector3Proxy)result.Tuple[7].UserData.Object == new Vector3Proxy(-Vector3.right)); // h

            return;
        }

        [Test]
        public void CanAccessClassMethods()
        {
            string code = @"
                local v1 = Vector3.forward
                local v2 = Vector3.right
                local v3 = Vector3.forward
                local v4 = Vector3.one


                local a = v1:Equals(v2) -- False
                local b = v1:Equals(v3) -- true
                v1:Set(2, 3, 4)
                local c = v1
                local d = v1:ToString()
                local e = v3:GetHashCode()
                v4:Scale(Vector3.one * 2)
                local f = v4
                local g = (Vector3.one * 5)
                g.Normalize()


                return a, b, c, d, e, f, g
                ";
            DynValue result = addonCreator.DoString(code);

            // a
            Assert.That(result.Tuple[0].Boolean == false);
            // b
            Assert.That(result.Tuple[1].Boolean == true);
            // c
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(new Vector3(2, 3, 4)));
            // d
            Assert.That(result.Tuple[3].String == new Vector3(2f, 3f, 4f).ToString());
            // e
            Assert.That(result.Tuple[4].Number == Vector3.forward.GetHashCode());
            // f
            Assert.That((Vector3Proxy)result.Tuple[5].UserData.Object == new Vector3Proxy(new Vector3(2f, 2f, 2f)));
            // g
            Vector3 g = (Vector3.one * 5);
            g.Normalize();
            Assert.That((Vector3Proxy)result.Tuple[6].UserData.Object == new Vector3Proxy(g));

            return;
        }

        [Test]
        public void CanAccessStaticProperties1()
        {
             string code = @"
                local a, b, c, d, e, f = Vector3.left, Vector3.right, Vector3.up, Vector3.down, Vector3.forward, Vector3.back
                return a, b, c, d, e, f
                ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(Vector3.left));
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(Vector3.right));
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(Vector3.up));
            Assert.That((Vector3Proxy)result.Tuple[3].UserData.Object == new Vector3Proxy(Vector3.down));
            Assert.That((Vector3Proxy)result.Tuple[4].UserData.Object == new Vector3Proxy(Vector3.forward));
            Assert.That((Vector3Proxy)result.Tuple[5].UserData.Object == new Vector3Proxy(Vector3.back));

            return;
        }

        [Test]
        public void CanAccessStaticProperties2()
        {
            string code = @"
                local v = Vector3.left
                local a, b, c, d, e, f = v.magnitude, v.normalized, v.sqrMagnitude, v.x, v.y, v.z
                return a, b, c, d, e, f
                ";

            DynValue result = addonCreator.DoString(code);

            Assert.That(result.Tuple[0].Number == Vector3.left.magnitude);
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(Vector3.left.normalized));
            Assert.That(result.Tuple[2].Number == Vector3.left.sqrMagnitude);
            Assert.That(result.Tuple[3].Number == Vector3.left.x);
            Assert.That(result.Tuple[4].Number == Vector3.left.y);
            Assert.That(result.Tuple[5].Number == Vector3.left.z);
            return;
        }
    }
}
