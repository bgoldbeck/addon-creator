﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class ColorTests : ProxyTester
    {
        private AddonCreator addonCreator = null;


        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(ColorProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(ColorProxy)));
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticProperties()
        {
            string code = @"
                local a = Color.black
                local b = Color.blue
                local c = Color.clear
                local d = Color.cyan
                local e = Color.gray
                local f = Color.green
                local g = Color.grey
                local h = Color.magenta
                local i = Color.red
                local j = Color.white
                local k = Color.yellow

                return a, b, c, d, e, f, g, h, i, j, k
            ";

            DynValue result = addonCreator.DoString(code);

            // a
            Assert.That((ColorProxy)result.Tuple[0].UserData.Object == new ColorProxy(Color.black));

            // b
            Assert.That((ColorProxy)result.Tuple[1].UserData.Object == new ColorProxy(Color.blue));

            // c
            Assert.That((ColorProxy)result.Tuple[2].UserData.Object == new ColorProxy(Color.clear));

            // d
            Assert.That((ColorProxy)result.Tuple[3].UserData.Object == new ColorProxy(Color.cyan));

            // e
            Assert.That((ColorProxy)result.Tuple[4].UserData.Object == new ColorProxy(Color.gray));

            // f
            Assert.That((ColorProxy)result.Tuple[5].UserData.Object == new ColorProxy(Color.green));

            // g
            Assert.That((ColorProxy)result.Tuple[6].UserData.Object == new ColorProxy(Color.grey));

            // h
            Assert.That((ColorProxy)result.Tuple[7].UserData.Object == new ColorProxy(Color.magenta));

            // i
            Assert.That((ColorProxy)result.Tuple[8].UserData.Object == new ColorProxy(Color.red));

            // j
            Assert.That((ColorProxy)result.Tuple[9].UserData.Object == new ColorProxy(Color.white));

            // k
            Assert.That((ColorProxy)result.Tuple[10].UserData.Object == new ColorProxy(Color.yellow));

            return;
        }

        [Test]
        public void CanAccessMetaMethods()
        {
            string code = @"
                local c1 = Color.__new(1, 2, 3)
                local c2 = Color.__new(2, 4, 6)
                local c3 = Color.__new(2, 4, 6)

                local a = c1 + c2
                local b = c2 - c1
                local c = c1 * 2
                local d = 2 * c1
                local e = c1 * c2
                local f = c2 / 2
                local g = c1 == c2 -- false
                local h = c2 == c3 -- true
                local i = c2.Equals(c3) -- true

                return a, b, c, d, e, f, g, h, i
            ";

            DynValue result = addonCreator.DoString(code);
            Color c1 = new Color(1f, 2f, 3f);
            Color c2 = new Color(2f, 4f, 6f);
            Color c3 = new Color(2f, 4f, 6f);

            Assert.That((ColorProxy)result.Tuple[0].UserData.Object == new ColorProxy(c1 + c2)); // a
            Assert.That((ColorProxy)result.Tuple[1].UserData.Object == new ColorProxy(c2 - c1)); // b
            Assert.That((ColorProxy)result.Tuple[2].UserData.Object == new ColorProxy(c1 * 2f)); // c
            Assert.That((ColorProxy)result.Tuple[3].UserData.Object == new ColorProxy(2f * c1)); // d
            Assert.That((ColorProxy)result.Tuple[4].UserData.Object == new ColorProxy(c1 * c2)); // e
            Assert.That((ColorProxy)result.Tuple[5].UserData.Object == new ColorProxy(c2 / 2f)); // f
            Assert.That(result.Tuple[6].Boolean == (c1 == c2)); // g
            Assert.That(result.Tuple[7].Boolean == (c2 == c3)); // h
            Assert.That(result.Tuple[8].Boolean == (c2.Equals(c3))); // i

            return;
        }

        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local a = Color.HSVToRGB(0.5, 0.25, 0.75)
                local b = Color.HSVToRGB(0.5, 0.25, 0.75, true)
                local c = Color.Lerp(Color.cyan, Color.white, 0.5)
                local d = Color.LerpUnclamped(Color.cyan, Color.white, 0.5)
                local e, f, g = Color.RGBToHSV(Color.cyan)

                return a, b, c, d, e, f, g
            ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((ColorProxy)result.Tuple[0].UserData.Object == new ColorProxy(Color.HSVToRGB(0.5f, 0.25f, 0.75f))); // a
            Assert.That((ColorProxy)result.Tuple[1].UserData.Object == new ColorProxy(Color.HSVToRGB(0.5f, 0.25f, 0.75f, true))); // b
            Assert.That((ColorProxy)result.Tuple[2].UserData.Object == new ColorProxy(Color.Lerp(Color.cyan, Color.white, 0.5f))); // c
            Assert.That((ColorProxy)result.Tuple[3].UserData.Object == new ColorProxy(Color.LerpUnclamped(Color.cyan, Color.white, 0.5f))); // d

            // e, f, g
            Color.RGBToHSV(Color.cyan, out float H, out float S, out float V);

            Assert.That(result.Tuple[4].Number == H); // e
            Assert.That(result.Tuple[5].Number == S); // f
            Assert.That(result.Tuple[6].Number == V); // g

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessProperties()
        {
            string code = @"
                local color = Color.cyan
                local a = color.a
                local b = color.b
                local c = color.g
                local d = color.gamma
                local e = color.grayscale
                local f = color.linear
                local g = color.maxColorComponent
                local h = color.r
                local i = color[0]
                local j = color[1]
                local k = color[2]
                local l = color[3]

                return a, b, c, d, e, f, g, h, i, j, k, l
            ";

            DynValue result = addonCreator.DoString(code);

            Color color = Color.cyan;
            Assert.That(result.Tuple[0].Number == color.a); // a
            Assert.That(result.Tuple[1].Number == color.b); // b
            Assert.That(result.Tuple[2].Number == color.g); // c
            Assert.That((ColorProxy)result.Tuple[3].UserData.Object == new ColorProxy(color.gamma)); // d
            Assert.That(result.Tuple[4].Number == color.grayscale); // e
            Assert.That((ColorProxy)result.Tuple[5].UserData.Object == new ColorProxy(color.linear)); // f
            Assert.That(result.Tuple[6].Number == color.maxColorComponent); // g
            Assert.That(result.Tuple[7].Number == color.r); // h
            Assert.That(result.Tuple[8].Number == color[0]); // i
            Assert.That(result.Tuple[9].Number == color[1]); // j
            Assert.That(result.Tuple[10].Number == color[2]); // k
            Assert.That(result.Tuple[11].Number == color[3]); // l

            return;
        }
    }
}
