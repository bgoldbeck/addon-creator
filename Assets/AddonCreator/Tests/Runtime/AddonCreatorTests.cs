﻿using System.Collections;
using System.IO;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace AddonCreator.Tests
{
    public class AddonCreatorTests
    {
        private static AddonCreator addonCreator = null;

        [SetUp]
        public void Setup()
        {
            addonCreator = AddonCreator.Instance();
            return;
        }

        [TearDown]
        public void Teardown()
        {
            AddonCreator.Destroy();
            return;
        }


        // A Test behaves as an ordinary method
        [UnityTest]
        public IEnumerator AddonCreatorCreatesAValidCanvasInScene()
        {
            addonCreator = AddonCreator.Instance();
            yield return null;
            Assert.IsFalse(addonCreator is null);

            //AddonCreatorCanvas[] addonCreatorCanvas = GameObject.FindObjectsOfType<AddonCreatorCanvas>();

            //Assert.IsTrue(addonCreatorCanvas.Length > 0);
            yield return null;
        }
    }
}
