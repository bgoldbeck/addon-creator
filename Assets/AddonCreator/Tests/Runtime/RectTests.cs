﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class RectTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(RectProxy), typeof(Vector2Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();


            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            //Assert.IsTrue(proxies.IsEnabled(typeof(RectProxy).Name));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticProperties()
        {
            string code = @"return Rect.zero";

            DynValue result = addonCreator.DoString(code);
            Assert.That((RectProxy)result.UserData.Object == RectProxy.zero);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassProperties()
        {
            string code = @"
                local rect = Rect.__new(0, 1, 2, 3)

                local a = rect.center
                local b = rect.height
                local c = rect.max
                local d = rect.min
                local e = rect.position
                local f = rect.size
                local g = rect.width
                local h = rect.x
                local i = rect.xMax
                local j = rect.xMin
                local k = rect.y
                local l = rect.yMax
                local m = rect.yMin

                return a, b, c, d, e, f, g, h, i, j, k, l, m";
            try
            {
                DynValue result = addonCreator.DoString(code);

                RectProxy rect = new RectProxy(0f, 1f, 2f, 3f);

                Assert.That((Vector2Proxy)result.Tuple[0].UserData.Object == rect.center); // a
                Assert.That(result.Tuple[1].Number == rect.height); // b
                Assert.That((Vector2Proxy)result.Tuple[2].UserData.Object == rect.max); // c
                Assert.That((Vector2Proxy)result.Tuple[3].UserData.Object == rect.min); // d
                Assert.That((Vector2Proxy)result.Tuple[4].UserData.Object == rect.position); // e
                Assert.That((Vector2Proxy)result.Tuple[5].UserData.Object == rect.size); // f
                Assert.That(result.Tuple[6].Number == rect.width); // g
                Assert.That(result.Tuple[7].Number == rect.x); // h
                Assert.That(result.Tuple[8].Number == rect.xMax); // i
                Assert.That(result.Tuple[9].Number == rect.xMin); // j
                Assert.That(result.Tuple[10].Number == rect.y); // k
                Assert.That(result.Tuple[11].Number == rect.yMax); // l
                Assert.That(result.Tuple[12].Number == rect.yMin); // m
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassMethods()
        {
            string code = @"
                local rect = Rect.__new(0, 0, 2, 3)
                local other = Rect.__new(0, 0, 1, 1)

                local a = rect.Contains(Vector2.zero)
                local b = rect.Contains(Vector3.zero)
                local c = rect.Contains(Vector3.zero, true)
                local d = rect.Overlaps(other)
                local e = rect.Overlaps(other, true)
                rect.Set(1, 2, 3, 4)
                local f = rect.ToString()


                return a, b, c, d, e, f";
            try
            {
                DynValue result = addonCreator.DoString(code);

                RectProxy rect = new RectProxy(0f, 0f, 2f, 3f);
                RectProxy other = new RectProxy(0f, 0f, 1f, 1f);

                Assert.That(result.Tuple[0].Boolean == rect.Contains(Vector2Proxy.zero)); // a
                Assert.That(result.Tuple[1].Boolean == rect.Contains(Vector3Proxy.zero)); // b
                Assert.That(result.Tuple[2].Boolean == rect.Contains(Vector3Proxy.zero, true)); // c
                Assert.That(result.Tuple[3].Boolean == rect.Overlaps(other)); // d
                Assert.That(result.Tuple[4].Boolean == rect.Overlaps(other, true)); // e
                rect.Set(1f, 2f, 3f, 4f);
                Assert.That(result.Tuple[5].String == rect.ToString()); // f

            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassMetaMethods()
        {
            string code = @"
                local rect1 = Rect.__new(0, 0, 2, 3)
                local rect2 = Rect.__new(0, 0, 1, 1)
                local rect3 = Rect.__new(0, 0, 2, 3)

                local a = (rect1 ~= rect2)
                local b = (rect1 == rect2)
                local c = (rect1 == rect3)


                return a, b, c";
            try
            {
                DynValue result = addonCreator.DoString(code);


                RectProxy rect1 = new RectProxy(new Rect(0f, 0f, 2f, 3f));
                RectProxy rect2 = new RectProxy(new Rect(0f, 0f, 1f, 1f));
                RectProxy rect3 = new RectProxy(new Rect(0f, 0f, 2f, 3f));

                Assert.That(result.Tuple[0].Boolean == (rect1 != rect2)); // a
                Assert.That(result.Tuple[1].Boolean == (rect1 == rect2)); // b
                Assert.That(result.Tuple[2].Boolean == (rect1 == rect3)); // c
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }
    }
}
