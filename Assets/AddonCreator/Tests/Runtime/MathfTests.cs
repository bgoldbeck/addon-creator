﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class MathfTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(MathfProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();


            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(MathfProxy)));

            return;
        }

        [Test]
        public void CanAccessStaticProperties()
        {

            string code = @"
                    local a = 30.0 * Mathf.Deg2Rad
                    local b = Mathf.Epsilon
                    local c = Mathf.Infinity
                    local d = Mathf.NegativeInfinity
                    local e = Mathf.PI
                    local f = 30.0 * Mathf.Rad2Deg

                    return a, b, c, d, e, f
                ";

            DynValue result = addonCreator.DoString(code);

            Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, 30f * Mathf.Deg2Rad));
            Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, Mathf.Epsilon));
            Assert.That(((float)result.Tuple[2].Number == Mathf.Infinity));
            Assert.That(((float)result.Tuple[3].Number == Mathf.NegativeInfinity));
            Assert.That(Mathf.Approximately((float)result.Tuple[4].Number, Mathf.PI));
            Assert.That(Mathf.Approximately((float)result.Tuple[5].Number, 30f * Mathf.Rad2Deg));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods1()
        {
            string code = @"
                local a = Mathf.Abs(-2)
                local b = Mathf.Acos(0.5)
                local c = Mathf.Approximately(1.0, 1.0) -- true
                local d = Mathf.Approximately(1.0, 1.1) -- false
                local e = Mathf.Asin(0.5)
                local f = Mathf.Atan(0.5)
                local g = Mathf.Atan2(0.5, 0.25)
                local h = Mathf.Ceil(9.6)
                local i = Mathf.CeilToInt(9.6)
                local j = Mathf.Clamp(9.6, 5.0, 9.0)
                local k = Mathf.Clamp01(2.0)
                local l = Mathf.ClosestPowerOfTwo(5)
                local m = Mathf.CorrelatedColorTemperatureToRGB(300.0)
                local n = Mathf.Cos(0.5)
                local o = Mathf.DeltaAngle(50.0, 90.0)
                local p = Mathf.Exp(6)
                local q = Mathf.FloatToHalf(20.0)
                local r = Mathf.Floor(20.7)
                local s = Mathf.FloorToInt(20.7)
                local t = Mathf.Gamma(20.0, 20.0, 5)
                local u = Mathf.GammaToLinearSpace(200)
                local v = Mathf.HalfToFloat(2)
                local v = Mathf.HalfToFloat(2)
                local w = Mathf.InverseLerp(2.0, 4.0, 0.5)
                local x = Mathf.IsPowerOfTwo(15) -- false
                local y = Mathf.IsPowerOfTwo(16) -- true
                local z = Mathf.Lerp(2, 4, 0.5)

                return a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
            ";

            DynValue result = addonCreator.DoString(code);
            try
            {
                Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, Mathf.Abs(-2)));
                Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, Mathf.Acos(0.5f)));
                Assert.That(result.Tuple[2].Boolean == Mathf.Approximately(1f, 1f));
                Assert.That(result.Tuple[3].Boolean == Mathf.Approximately(1.0f, 1.1f));
                Assert.That(Mathf.Approximately((float)result.Tuple[4].Number, Mathf.Asin(0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[5].Number, Mathf.Atan(0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[6].Number, Mathf.Atan2(0.5f, 0.25f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[7].Number, Mathf.Ceil(9.6f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[8].Number, Mathf.CeilToInt(9.6f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[9].Number, Mathf.Clamp(9.6f, 5f, 9f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[10].Number, Mathf.Clamp01(2f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[11].Number, Mathf.ClosestPowerOfTwo(5)));
                Assert.That(((ColorProxy)result.Tuple[12].UserData.Object == new ColorProxy(Mathf.CorrelatedColorTemperatureToRGB(300f))));
                Assert.That(Mathf.Approximately((float)result.Tuple[13].Number, Mathf.Cos(0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[14].Number, Mathf.DeltaAngle(50f, 90f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[15].Number, Mathf.Exp(6)));
                Assert.That(Mathf.Approximately((float)result.Tuple[16].Number, Mathf.FloatToHalf(20f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[17].Number, Mathf.Floor(20.7f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[18].Number, Mathf.FloorToInt(20.7f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[19].Number, Mathf.Gamma(20f, 20f, 5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[20].Number, Mathf.GammaToLinearSpace(200f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[21].Number, Mathf.HalfToFloat(2)));
                Assert.That(Mathf.Approximately((float)result.Tuple[22].Number, Mathf.InverseLerp(2f, 4f, 0.5f)));
                Assert.That(result.Tuple[23].Boolean == Mathf.IsPowerOfTwo(15));
                Assert.That(result.Tuple[24].Boolean == Mathf.IsPowerOfTwo(16));
                Assert.That(Mathf.Approximately((float)result.Tuple[25].Number, Mathf.Lerp(2f, 4f, 0.5f)));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }

        [Test]
        public void SmoothDampTest()
        {
            string code = @"
                local a, b = Mathf.SmoothDamp(2.0, 4.0, 1.0, 0.5)
                local c, d = Mathf.SmoothDamp(2.0, 4.0, 1.0, 0.5, 1.5)
                local e, f = Mathf.SmoothDamp(2.0, 4.0, 1.0, 0.5, 1.5, 2.5)

                local g, h = Mathf.SmoothDampAngle(2.0, 4.0, 1.0, 0.5)
                local i, j = Mathf.SmoothDampAngle(2.0, 4.0, 1.0, 0.5, 1.5)
                local k, l = Mathf.SmoothDampAngle(2.0, 4.0, 1.0, 0.5, 1.5, 2.5)

                return a, b, c, d, e, f, g, h, i, j, k, l
            ";

            DynValue result = addonCreator.DoString(code);

            try
            {

                float currentVelocity = 1f;
                // ab
                float a = Mathf.SmoothDamp(2f, 4f, ref currentVelocity, 0.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, a));
                Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, currentVelocity));
                // cd
                currentVelocity = 1f;
                float c = Mathf.SmoothDamp(2f, 4f, ref currentVelocity, 0.5f, 1.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[2].Number, c));
                Assert.That(Mathf.Approximately((float)result.Tuple[3].Number, currentVelocity));
                // ef
                currentVelocity = 1f;
                float e = Mathf.SmoothDamp(2f, 4f, ref currentVelocity, 0.5f, 1.5f, 2.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[4].Number, e));
                Assert.That(Mathf.Approximately((float)result.Tuple[5].Number, currentVelocity));
                // gh
                currentVelocity = 1f;
                float g = Mathf.SmoothDampAngle(2f, 4f, ref currentVelocity, 0.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[6].Number, g));
                Assert.That(Mathf.Approximately((float)result.Tuple[7].Number, currentVelocity));
                // ij
                currentVelocity = 1f;
                float i = Mathf.SmoothDampAngle(2f, 4f, ref currentVelocity, 0.5f, 1.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[8].Number, i));
                Assert.That(Mathf.Approximately((float)result.Tuple[9].Number, currentVelocity));
                // kl
                currentVelocity = 1f;
                float k = Mathf.SmoothDampAngle(2f, 4f, ref currentVelocity, 0.5f, 1.5f, 2.5f);
                Assert.That(Mathf.Approximately((float)result.Tuple[10].Number, k));
                Assert.That(Mathf.Approximately((float)result.Tuple[11].Number, currentVelocity));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods2()
        {
            string code = @"
                local a = Mathf.LerpAngle(2.0, 4.0, 0.5)
                local b = Mathf.LerpUnclamped(2.0, 4.0, 0.5)
                local c = Mathf.LinearToGammaSpace(20)
                local d = Mathf.Log(2, 4)
                local e = Mathf.Log(4)
                local f = Mathf.Log10(3)
                local g = Mathf.Max(5, 10, 15)
                local h = Mathf.Min(5, 10, 15)
                local i = Mathf.MoveTowards(0, 100, 25)
                local j = Mathf.MoveTowardsAngle(0, 100, 25)
                local k = Mathf.NextPowerOfTwo(50)
                local l = Mathf.PerlinNoise(50, 100)
                local m = Mathf.PingPong(50, 100)
                local n = Mathf.Pow(2, 5)
                local o = Mathf.Repeat(2, 5)
                local p = Mathf.Round(7.75)
                local q = Mathf.RoundToInt(7.75)
                local r = Mathf.Sign(7.75)
                local s = Mathf.Sin(0.75)
                local t = Mathf.SmoothStep(2.0, 20.0, 0.5)
                local u = Mathf.Sqrt(7.75)
                local v = Mathf.Tan(0.75)


                return a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v
            ";

            DynValue result = addonCreator.DoString(code);
            try
            {
                Assert.That(Mathf.Approximately((float)result.Tuple[0].Number, Mathf.LerpAngle(2f, 4f, 0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[1].Number, Mathf.LerpUnclamped(2f, 4f, 0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[2].Number, Mathf.LinearToGammaSpace(20f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[3].Number, Mathf.Log(2f, 4f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[4].Number, Mathf.Log(4f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[5].Number, Mathf.Log10(3f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[6].Number, Mathf.Max(new float[] { 5f, 10f, 15f})));
                Assert.That(Mathf.Approximately((float)result.Tuple[7].Number, Mathf.Min(new float[] { 5f, 10f, 15f})));
                Assert.That(Mathf.Approximately((float)result.Tuple[8].Number, Mathf.MoveTowards(0f, 100f, 25f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[9].Number, Mathf.MoveTowardsAngle(0f, 100f, 25f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[10].Number, Mathf.NextPowerOfTwo(50)));
                Assert.That(Mathf.Approximately((float)result.Tuple[11].Number, Mathf.PerlinNoise(50, 100)));
                Assert.That(Mathf.Approximately((float)result.Tuple[12].Number, Mathf.PingPong(50, 100)));
                Assert.That(Mathf.Approximately((float)result.Tuple[13].Number, Mathf.Pow(2, 5)));
                Assert.That(Mathf.Approximately((float)result.Tuple[14].Number, Mathf.Repeat(2, 5)));
                Assert.That(Mathf.Approximately((float)result.Tuple[15].Number, Mathf.Round(7.75f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[16].Number, Mathf.RoundToInt(7.75f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[17].Number, Mathf.Sign(7.75f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[18].Number, Mathf.Sin(0.75f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[19].Number, Mathf.SmoothStep(2f, 20f, 0.5f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[20].Number, Mathf.Sqrt(7.75f)));
                Assert.That(Mathf.Approximately((float)result.Tuple[21].Number, Mathf.Tan(0.75f)));

            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}
