﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class PlaneTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Matrix4x4Proxy), typeof(QuaternionProxy), typeof(Vector3Proxy), typeof(Vector4Proxy), typeof(RayProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);

            return;
        }

        [Test]
        public void CanAccessClassProperties()
        {
            string code = @"
                local plane = Plane.__new(Vector3.up, 2.0)

                local a = plane.normal
                local b = plane.distance
                local c = plane.flipped

                return a, b, c
               ";

            DynValue result = addonCreator.DoString(code);

            Plane plane = new Plane(Vector3.up, 2.0f);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(plane.normal)); // a
            Assert.That(result.Tuple[1].Number == plane.distance); // b
            Assert.That((PlaneProxy)result.Tuple[2].UserData.Object == new PlaneProxy(plane.flipped)); // c
            return;
        }

        [Test]
        public void CanAccessStaticPublicMethods()
        {
            string code = @"
                local plane = Plane.__new(Vector3.up, 2.0)
                local translation = Vector3.__new(1, 2, 3)

                local a = Plane.Translate(plane, translation)

                return a
               ";

            DynValue result = addonCreator.DoString(code);

            Plane plane = new Plane(Vector3.up, 2.0f);
            Vector3 translation = new Vector3(1f, 2f, 3f);

            Assert.That((PlaneProxy)result.UserData.Object == new PlaneProxy(Plane.Translate(plane, translation)));
            return;
        }

        [Test]
        public void CanAccessPublicMethods()
        {
            string code = @"
                local plane = Plane.__new(Vector3.up, 2.0)
                local test = Vector3.__new(3, 3, 3)

                local a = plane.ClosestPointOnPlane(test)
                local b = plane.GetDistanceToPoint(test)
                plane.Flip()
                local c = plane.normal
                local d = plane.GetSide(Vector3.left)
                local e, f = plane.Raycast(Ray.__new(Vector3.zero, Vector3.forward), 3)
                local g = plane.SameSide(Vector3.zero, Vector3.one)

                plane.Set3Points(Vector3.left, Vector3.up, Vector3.right)
                local h = plane.flipped

                plane.SetNormalAndPosition(Vector3.up, Vector3.forward)
                local i = plane.flipped

                plane.Translate(Vector3.one)

                local j = plane.ToString()
                local k = plane.GetHashCode()

                return a, b, c, d, e, f, g, h, i, j, k
               ";

            DynValue result = addonCreator.DoString(code);

            Plane plane = new Plane(Vector3.up, 2.0f);
            Vector3 test = new Vector3(3f, 3f, 3f);

            Assert.That((Vector3Proxy)result.Tuple[0].UserData.Object == new Vector3Proxy(plane.ClosestPointOnPlane(test))); // a
            Assert.That(result.Tuple[1].Number == plane.GetDistanceToPoint(test)); // b
            plane.Flip();
            Assert.That((Vector3Proxy)result.Tuple[2].UserData.Object == new Vector3Proxy(plane.normal)); // c
            Assert.That(result.Tuple[3].Boolean == plane.GetSide(Vector3.left)); // d

            bool e = plane.Raycast(new Ray(Vector3.zero, Vector3.forward), out float f);
            Assert.That(result.Tuple[4].Boolean == e); // e
            Assert.That(result.Tuple[5].Number == f); // f
            Assert.That(result.Tuple[6].Boolean == plane.SameSide(Vector3.zero, Vector3.one)); // g

            plane.Set3Points(Vector3.left, Vector3.up, Vector3.right);
            Assert.That((PlaneProxy)result.Tuple[7].UserData.Object == new PlaneProxy(plane.flipped)); // h

            plane.SetNormalAndPosition(Vector3.up, Vector3.forward);
            Assert.That((PlaneProxy)result.Tuple[8].UserData.Object == new PlaneProxy(plane.flipped)); // i

            plane.Translate(Vector3.one);
            Assert.That(result.Tuple[9].String == plane.ToString()); // j
            Assert.That((int)result.Tuple[10].Number == plane.GetHashCode()); // k
            return;
        }

        [Test]
        public void CanAccessMetaMethodOperators()
        {
            string code = @"
                local plane1 = Plane.__new(Vector3.up, 2.0)
                local plane2 = Plane.__new(Vector3.down, 7.0)
                local plane3 = Plane.__new(Vector3.down, 2.0)

                local a = (plane1 ~= plane2)
                local b = (plane1 == plane2)
                local c = (plane1 == plane3)

                return a, b, c
               ";

            DynValue result = addonCreator.DoString(code);

            PlaneProxy plane1 = new PlaneProxy(new Plane(Vector3.up, 2.0f));
            PlaneProxy plane2 = new PlaneProxy(new Plane(Vector3.down, 7.0f));
            PlaneProxy plane3 = new PlaneProxy(new Plane(Vector3.down, 2.0f));

            Assert.That(result.Tuple[0].Boolean == (plane1 != plane2)); // a
            Assert.That(result.Tuple[1].Boolean == (plane1 == plane2)); // b
            Assert.That(result.Tuple[2].Boolean == (plane1 == plane3)); // c
            return;
        }
    }
}
