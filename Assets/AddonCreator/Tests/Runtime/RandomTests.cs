﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class RandomTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(RandomProxy), typeof(ColorProxy), typeof(QuaternionProxy), typeof(Vector3Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();


            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(RandomProxy)));
            Assert.IsTrue(proxies.IsEnabled(typeof(ColorProxy)));
            Assert.IsTrue(proxies.IsEnabled(typeof(QuaternionProxy)));

            return;
        }

        [Test]
        public void CanAccessStaticProperties()
        {
            string code = @"
                local a = Random.insideUnitCircle
                local b = Random.insideUnitSphere
                local c = Random.onUnitSphere
                local d = Random.rotation
                local e = Random.rotationUniform
                local f = Random.state
                local g = Random.value

                return a, b, c, d, e, f, g
            ";
            DynValue result = addonCreator.DoString(code);
            // a
            Assert.That(result.Tuple[0].UserData.Object is Vector3Proxy);

            // b
            Assert.That(result.Tuple[1].UserData.Object is Vector3Proxy);

            // c
            Assert.That(result.Tuple[2].UserData.Object is Vector3Proxy);

            // d
            Assert.That(result.Tuple[3].UserData.Object is QuaternionProxy);

            // e
            Assert.That(result.Tuple[4].UserData.Object is QuaternionProxy);

            // f
            Assert.That(result.Tuple[5].UserData.Object is UnityEngine.Random.State);

            // g
            Assert.That(result.Tuple[6].Number >= 0f && result.Tuple[6].Number <= 1f);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local a = Random.Range(-10, 10)
                local b = Random.ColorHSV(0, 1)
                local c = Random.ColorHSV(0, 1, 0, 1)
                local d = Random.ColorHSV(0, 1, 0, 1, 0, 1)
                local e = Random.ColorHSV(0, 1, 0, 1, 0, 1, 0, 1)
                return a, b, c, d, e
            ";


            for (int i = 0; i < 10000; ++i)
            {
                DynValue result = addonCreator.DoString(code);

                // a
                Assert.That(result.Tuple[0].Number >= -10 && result.Tuple[0].Number <= 10);
                // b
                Assert.That(result.Tuple[1].UserData.Object is ColorProxy);
                // c
                Assert.That(result.Tuple[2].UserData.Object is ColorProxy);
                // d
                Assert.That(result.Tuple[3].UserData.Object is ColorProxy);
                // e
                Assert.That(result.Tuple[4].UserData.Object is ColorProxy);

            }
            return;
        }
    }
}
