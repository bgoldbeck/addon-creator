﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class Vector2Tests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(Vector2Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(Vector2Proxy)));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticProperties()
        {

            string code = @"
                local a = Vector2.right
                local b = Vector2.left
                local c = Vector2.down
                local d = Vector2.up
                local e = Vector2.one
                local f = Vector2.zero
                local g = Vector2.positiveInfinity
                local h = Vector2.negativeInfinity

                return a, b, c, d, e, f, g, h
            ";
            try
            {
                DynValue result = addonCreator.DoString(code);

                Assert.That((Vector2Proxy)result.Tuple[0].UserData.Object == new Vector2Proxy(Vector2.right));
                Assert.That((Vector2Proxy)result.Tuple[1].UserData.Object == new Vector2Proxy(Vector2.left));
                Assert.That((Vector2Proxy)result.Tuple[2].UserData.Object == new Vector2Proxy(Vector2.down));
                Assert.That((Vector2Proxy)result.Tuple[3].UserData.Object == new Vector2Proxy(Vector2.up));
                Assert.That((Vector2Proxy)result.Tuple[4].UserData.Object == new Vector2Proxy(Vector2.one));
                Assert.That((Vector2Proxy)result.Tuple[5].UserData.Object == new Vector2Proxy(Vector2.zero));
                Assert.That((Vector2Proxy)result.Tuple[6].UserData.Object == new Vector2Proxy(Vector2.positiveInfinity));
                Assert.That((Vector2Proxy)result.Tuple[7].UserData.Object == new Vector2Proxy(Vector2.negativeInfinity));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassProperties()
        {
            string code = @"
                local v1 = Vector2.__new(2, 4)

                local a = v1.sqrMagnitude
                local b = v1.normalized
                local c = v1.magnitude

                return a, b, c
            ";
            try
            {
                DynValue result = addonCreator.DoString(code);
                Vector2 v1 = new Vector2(2, 4);

                Assert.That(result.Tuple[0].Number == v1.sqrMagnitude);
                Assert.That((Vector2Proxy)result.Tuple[1].UserData.Object == new Vector2Proxy(v1.normalized));
                Assert.That(result.Tuple[2].Number == v1.magnitude);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            //public float sqrMagnitude { get { return target.Unwrap.sqrMagnitude; } }
            //public Vector2Proxy normalized { get { return new Vector2Proxy(target.Unwrap.normalized); } }
            //public float magnitude { get { return target.Unwrap.magnitude; } }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local a = Vector2.Angle(Vector2.left, Vector2.right)
                local b = Vector2.ClampMagnitude(Vector2.one * 10, 5)
                local c = Vector2.Distance(Vector2.one * 10, Vector2.left)
                local d = Vector2.Dot(Vector2.right, Vector2.left)
                local e = Vector2.Lerp(Vector2.zero, Vector2.one, 0.6)
                local f = Vector2.LerpUnclamped(Vector2.zero, Vector2.one, 0.6)
                local g = Vector2.Max(Vector2.zero, Vector2.one)
                local h = Vector2.Min(Vector2.zero, Vector2.one)
                local i = Vector2.MoveTowards(Vector2.zero, Vector2.one, 0.6)
                local j = Vector2.Perpendicular(Vector2.one)
                local k = Vector2.Reflect(Vector2.one, Vector2.left)
                local l = Vector2.Scale(Vector2.one, Vector2.left)
                local m = Vector2.SignedAngle(Vector2.one, Vector2.left)
                local n, o = Vector2.SmoothDamp(Vector2.zero, Vector2.one, Vector2.left, 0.5)
                local p, q = Vector2.SmoothDamp(Vector2.zero, Vector2.one, Vector2.left, 0.5, 4)
                local r, s = Vector2.SmoothDamp(Vector2.zero, Vector2.one, Vector2.left, 0.5, 4, 6)
                local t = Vector2.SqrMagnitude(Vector2.one)

                return a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t
            ";

            DynValue result = addonCreator.DoString(code);

            Assert.That((float)result.Tuple[0].Number == Vector2.Angle(Vector2.left, Vector2.right));
            Assert.That((Vector2Proxy)result.Tuple[1].UserData.Object == new Vector2Proxy(Vector2.ClampMagnitude(Vector2.one * 10f, 5f)));
            Assert.That((float)result.Tuple[2].Number == Vector2.Distance(Vector2.one * 10f, Vector2.left));
            Assert.That((float)result.Tuple[3].Number == Vector2.Dot(Vector2.right, Vector2.left));
            Assert.That((Vector2Proxy)result.Tuple[4].UserData.Object == new Vector2Proxy(Vector2.Lerp(Vector2.zero, Vector2.one, 0.6f)));
            Assert.That((Vector2Proxy)result.Tuple[5].UserData.Object == new Vector2Proxy(Vector2.LerpUnclamped(Vector2.zero, Vector2.one, 0.6f)));
            Assert.That((Vector2Proxy)result.Tuple[6].UserData.Object == new Vector2Proxy(Vector2.Max(Vector2.zero, Vector2.one)));
            Assert.That((Vector2Proxy)result.Tuple[7].UserData.Object == new Vector2Proxy(Vector2.Min(Vector2.zero, Vector2.one)));
            Assert.That((Vector2Proxy)result.Tuple[8].UserData.Object == new Vector2Proxy(Vector2.MoveTowards(Vector2.zero, Vector2.one, 0.6f)));
            Assert.That((Vector2Proxy)result.Tuple[9].UserData.Object == new Vector2Proxy(Vector2.Perpendicular(Vector2.one)));
            Assert.That((Vector2Proxy)result.Tuple[10].UserData.Object == new Vector2Proxy(Vector2.Reflect(Vector2.one, Vector2.left)));
            Assert.That((Vector2Proxy)result.Tuple[11].UserData.Object == new Vector2Proxy(Vector2.Scale(Vector2.one, Vector2.left)));
            Assert.That((float)result.Tuple[12].Number == Vector2.SignedAngle(Vector2.one, Vector2.left));
            Vector2 o = Vector2.left;
            Vector2 n = Vector2.SmoothDamp(Vector2.zero, Vector2.one, ref o, 0.5f);
            Assert.That((Vector2Proxy)result.Tuple[13].UserData.Object == new Vector2Proxy(n));
            Assert.That((Vector2Proxy)result.Tuple[14].UserData.Object == new Vector2Proxy(o));

            Vector2 p = Vector2.left;
            Vector2 q = Vector2.SmoothDamp(Vector2.zero, Vector2.one, ref p, 0.5f, 4f);
            Assert.That((Vector2Proxy)result.Tuple[15].UserData.Object == new Vector2Proxy(q));
            Assert.That((Vector2Proxy)result.Tuple[16].UserData.Object == new Vector2Proxy(p));

            Vector2 r = Vector2.left;
            Vector2 s = Vector2.SmoothDamp(Vector2.zero, Vector2.one, ref r, 0.5f, 4f, 6f);
            Assert.That((Vector2Proxy)result.Tuple[17].UserData.Object == new Vector2Proxy(s));
            Assert.That((Vector2Proxy)result.Tuple[18].UserData.Object == new Vector2Proxy(r));

            Assert.That((float)result.Tuple[19].Number == Vector2.SqrMagnitude(Vector2.one));
            return;
        }



    }
}
