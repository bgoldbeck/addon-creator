﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MoonSharp.Interpreter.Interop;
using MoonSharp.Interpreter;
using NUnit.Framework;
using System;
using System.Linq;
using UnityEngine.UI;


namespace AddonCreator.Tests
{
    public class BaseObjectTests
    {

        [SetUp]
        public void Setup()
        {


			return;
        }

        [TearDown]
        public void Teardown()
        {
            return;
        }

        [Test]
        public void BaseObjectCreateNewSubClassInstance()
        {
            UnityEngine.Object script = Resources.LoadAll("AddonCreator/Libraries/Utilities/").FirstOrDefault(x => x.name == "BaseObject");

            Assert.IsFalse(script is null);

            // Make sure we can create a subclass from the BaseObject.
            string code = @"
			    local DerivedClass = BaseObject:Subclass()

                function DerivedClass:Add(a, b)
	                return a + b
                end

                return DerivedClass:Add(24, 2)";

            DynValue res = Script.RunString(string.Format("{0}{1}{2}", script, System.Environment.NewLine, code));
            Assert.IsTrue(res.Type == DataType.Number);
            Assert.IsTrue(res.Number == 26);
            return;
        }
    }
}
