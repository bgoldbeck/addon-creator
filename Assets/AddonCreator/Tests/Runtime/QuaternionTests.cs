﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    /// <summary>
    /// https://docs.unity3d.com/ScriptReference/Quaternion.html
    /// </summary>
    public class QuaternionTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(QuaternionProxy), typeof(Vector3Proxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();


            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(QuaternionProxy)));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessMetaMethods()
        {
            string code = @"
                local q1 = Quaternion.__new()
                local q2 = Quaternion.__new()

                q1:SetFromToRotation(Vector3.left, Vector3.up)
                q2:SetFromToRotation(Vector3.right, Vector3.up)

                local a = q1 == q2 -- false
                local b = q1 ~= q2 -- true
                local c = q1 * q2

                return a, b, c
            ";
            DynValue result = addonCreator.DoString(code);

            Quaternion q1 = Quaternion.identity;
            Quaternion q2 = Quaternion.identity;

            q1.SetFromToRotation(Vector3.left, Vector3.up);
            q2.SetFromToRotation(Vector3.right, Vector3.up);

            bool a = q1 == q2;
            bool b = q1 != q2;
            Quaternion c = q1 * q2;

            // a
            Assert.That(result.Tuple[0].Boolean == a);

            // b
            Assert.That(result.Tuple[1].Boolean == b);

            // c
            Assert.That((QuaternionProxy)result.Tuple[2].UserData.Object == new QuaternionProxy(q1 * q2));

            return;
        }
        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessStaticMethods()
        {
            string code = @"
                local q1 = Quaternion.__new()
                local q2 = Quaternion.__new()

                q1:SetFromToRotation(Vector3.left, Vector3.up)
                q2:SetFromToRotation(Vector3.right, Vector3.up)

                local a = Quaternion.Angle(q1, q2)
                local b = Quaternion.AngleAxis(90, Vector3.up)
                local c = Quaternion.Dot(q1, q2)
                local d = Quaternion.Euler(10, 20, 30)
                local e = Quaternion.FromToRotation(Vector3.left, Vector3.up)
                local f = Quaternion.Inverse(q1)
                local g = Quaternion.Lerp(q1, q2, 0.5)
                local h = Quaternion.LerpUnclamped(q1, q2, 0.5)
                local i = Quaternion.LookRotation(Vector3.forward, Vector3.up)
                local j = Quaternion.Normalize(q1)
                local k = Quaternion.RotateTowards(q1, q2, 15)
                local l = Quaternion.Slerp(q1, q2, 0.5)
                local m = Quaternion.SlerpUnclamped(q1, q2, 0.5)

                return a, b, c, d, e, f, g, h, i, j, k, l, m
            ";

            DynValue result = addonCreator.DoString(code);
            Quaternion q1 = Quaternion.identity;
            Quaternion q2 = Quaternion.identity;

            q1.SetFromToRotation(Vector3.left, Vector3.up);
            q2.SetFromToRotation(Vector3.right, Vector3.up);

            // a
            float a = Quaternion.Angle(q1, q2);
            Assert.That(result.Tuple[0].Number == a);

            // b
            Assert.That((QuaternionProxy)result.Tuple[1].UserData.Object == new QuaternionProxy(Quaternion.AngleAxis(90f, Vector3.up)));

            // c
            Assert.That(result.Tuple[2].Number == Quaternion.Dot(q1, q2));

            // d
            Assert.That((QuaternionProxy)result.Tuple[3].UserData.Object == new QuaternionProxy(Quaternion.Euler(10f, 20f, 30f)));

            // e
            Assert.That((QuaternionProxy)result.Tuple[4].UserData.Object == new QuaternionProxy(Quaternion.FromToRotation(Vector3.left, Vector3.up)));

            // f
            Assert.That((QuaternionProxy)result.Tuple[5].UserData.Object == new QuaternionProxy(Quaternion.Inverse(q1)));

            // g
            Assert.That((QuaternionProxy)result.Tuple[6].UserData.Object == new QuaternionProxy(Quaternion.Lerp(q1, q2, 0.5f)));

            // h
            Assert.That((QuaternionProxy)result.Tuple[7].UserData.Object == new QuaternionProxy(Quaternion.LerpUnclamped(q1, q2, 0.5f)));

            // i
            Assert.That((QuaternionProxy)result.Tuple[8].UserData.Object == new QuaternionProxy(Quaternion.LookRotation(Vector3.forward, Vector3.up)));

            // j
            Assert.That((QuaternionProxy)result.Tuple[9].UserData.Object == new QuaternionProxy(Quaternion.Normalize(q1)));

            // k
            Assert.That((QuaternionProxy)result.Tuple[10].UserData.Object == new QuaternionProxy(Quaternion.RotateTowards(q1, q2, 15.0f)));

            // l
            Assert.That((QuaternionProxy)result.Tuple[11].UserData.Object == new QuaternionProxy(Quaternion.Slerp(q1, q2, 0.5f)));

            // m
            Assert.That((QuaternionProxy)result.Tuple[12].UserData.Object == new QuaternionProxy(Quaternion.SlerpUnclamped(q1, q2, 0.5f)));

            return;
        }
        /// <summary>
        ///
        /// </summary>
        [Test]
        public void CanAccessClassMethods()
        {
            // https://docs.unity3d.com/ScriptReference/Quaternion.ToAngleAxis.html

            string code = @"
                local q1 = Quaternion.__new()
                local q2 = Quaternion.__new()
                local q3 = Quaternion.__new()


                local angle, axis = q1:ToAngleAxis()
                q1:Set(1, 1, 1, 1)
                q2:SetFromToRotation(Vector3.left, Vector3.up)
                q3:SetLookRotation(Vector3.forward, Vector3.up)
                local q3tostring = q3.ToString()

                return angle, axis, q1, q2, q3, q3tostring
                ";

            DynValue result = addonCreator.DoString(code);

            Quaternion.identity.ToAngleAxis(out float angle, out Vector3 axis);

            // angle, axis
            Assert.That(result.Tuple[0].Number == angle);
            Assert.That((Vector3Proxy)result.Tuple[1].UserData.Object == new Vector3Proxy(axis));
            // q1
            Assert.That((QuaternionProxy)result.Tuple[2].UserData.Object == new QuaternionProxy(new Quaternion(1f, 1f, 1f, 1f)));
            // q2
            Quaternion q2 = new Quaternion();
            q2.SetFromToRotation(Vector3.left, Vector3.up);
            Assert.That((QuaternionProxy)result.Tuple[3].UserData.Object == new QuaternionProxy(q2));
            // q3
            Quaternion q3 = new Quaternion();
            q3.SetLookRotation(Vector3.forward, Vector3.up);
            Assert.That((QuaternionProxy)result.Tuple[4].UserData.Object == new QuaternionProxy(q3));
            // q4tostring
            Assert.That(result.Tuple[5].String == q3.ToString());

            return;
        }
    }
}
