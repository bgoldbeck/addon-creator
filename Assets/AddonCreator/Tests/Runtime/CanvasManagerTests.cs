﻿using System;
using UnityEngine;
using NUnit.Framework;
using System.Collections.Generic;
using MoonSharp.Interpreter;


namespace AddonCreator.Tests
{
    public class CanvasManagerTests : ProxyTester
    {
        private AddonCreator addonCreator = null;

        /// <summary>
        ///
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(CanvasManagerProxy), typeof(UIElementProxy) });

            // Grab the AddonCreator instance after we do setup.
            addonCreator = AddonCreator.Instance();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void ProxyObjectWasEnabled()
        {
            Assert.IsInstanceOf(typeof(AddonCreator), addonCreator);
            Assert.IsInstanceOf(typeof(ProxyLibrary), proxies);
            Assert.IsTrue(proxies.IsEnabled(typeof(CanvasManagerProxy)));

            return;
        }

        /// <summary>
        ///
        /// </summary>
        [Test]
        public void SetParentOfElementToAnotherControl()
        {
            string code = @"
                local childElement = CANVAS_MANAGER:CreateTopLevelControl('child')
                local parentElement = ANVAS_MANAGER:CreateTopLevelControl('parent')
               ";
            try
            {
                DynValue result = addonCreator.DoString(code);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            return;
        }
    }
}
