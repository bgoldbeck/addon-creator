﻿using System.Collections;
using System.IO;
using System.Linq;
using MoonSharp.Interpreter;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace AddonCreator.Tests
{
    public class AddonReaderTests
    {
        private static AddonCreator addonCreator = null;

        [SetUp]
        public void Setup()
        {
            addonCreator = AddonCreator.Instance();
            return;
        }

        [TearDown]
        public void Teardown()
        {
            AddonCreator.Destroy();
            return;
        }

        [Test]
        public void ReadModuleFromDisk()
        {
            TextAsset moduleData = new TextAsset();
            moduleData = (TextAsset)Resources.Load("Lua/AddonReaderLuaTest", typeof(TextAsset));
            AddonModule module = new AddonModule("AddonReaderLuaTest", moduleData.text);

            AddonReader addonReader = new AddonReader();

            Script script = new Script();

            Assert.That(addonReader.ReadModule(module, script) == true);
            return;
        }

        [Test]
        public void ReadManifestFile()
        {
            TextAsset xmlData = new TextAsset();
            xmlData = (TextAsset)Resources.Load("Lua/AddonManifestTest", typeof(TextAsset));

            AddonReader addonReader = new AddonReader();

           addonReader.ParseAddonManifestFromContents(xmlData.text, out AddonManifest addonManifest);


            Assert.That(addonManifest.title.Equals("AddonManifestTest"));
            Assert.That(addonManifest.version.Equals("1.0"));
            Assert.That(addonManifest.api.Equals("11223344"));
            Assert.That(addonManifest.date.Equals("2177/2/23"));
            Assert.That(addonManifest.description.Equals("Describe the addon here"));
            Assert.That(addonManifest.author.Equals("Rando"));
            Assert.That(addonManifest.email.Equals("user@domain.tld"));
            Assert.That(addonManifest.isLibrary.Equals(false));
            Assert.That(addonManifest.dependencies.Count == 1);
            Assert.That(addonManifest.dependencies[0].name == "AddonManifestDependency");
            Assert.That(addonManifest.dependencies[0].version == "1.2");

            Assert.That(addonManifest.modules.Count == 0);


            return;
        }

    }
}
