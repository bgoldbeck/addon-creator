﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using AddonCreator;
using NUnit.Framework;
using MoonSharp.Interpreter;
using System.Collections.Generic;


namespace AddonCreator.Tests
{
    public class DebugProxyTests : ProxyTester
    {
        // Arrange.
        // Act.
        // Assert.

        private static AddonCreator addonCreator;
        //private static AddonCreatorDebug addonCreatorDebug;
        private LogType lastLogType;

        private string lastLogString;

        [SetUp]
        public void Setup()
        {
            Setup(new List<Type> { typeof(DebugProxy) });

            addonCreator = AddonCreator.Instance();

            return;
        }

        [TearDown]
        public void Teardown()
        {
            Deconstruct();
            AddonCreator.Destroy();
            return;
        }

        [Test]
        public void LogToOutputStreamWhenLogIsCalled()
        {

            string testString = "LogToOutputStreamWhenLogIsCalled";

            string code = String.Format("Debug:Log('{0}')", testString);

            addonCreator.DoString(code);

            LogAssert.Expect(LogType.Log, testString);
            return;
        }

        [Test]
        public void LogToOutputStreamWhenLogWarningIsCalled()
        {

            string testString = "LogToOutputStreamWhenLogWarningIsCalled";

            string code = String.Format("Debug:LogWarning('{0}')", testString);

            addonCreator.DoString(code);

            LogAssert.Expect(LogType.Warning, testString);

            return;
        }

        [Test]
        public void LogToOutputStreamWhenLogErrorIsCalled()
        {
            string testString = "LogToOutputStreamWhenLogErrorIsCalled";

            string code = String.Format("Debug:LogError('{0}')", testString);

            addonCreator.DoString(code);

            // Without expecting an error log, the test would fail
            LogAssert.Expect(LogType.Error, testString);
            return;
        }
    }
}
