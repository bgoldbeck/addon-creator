﻿using UnityEditor;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;


namespace AddonCreator
{
    [CustomEditor(typeof(AddonCreator))]
    [System.Serializable]
    public class AddonCreatorEditor : Editor
    {

        private bool outerFoldout = true;

        /// <summary>
        /// <key, (types, categoryEnabled)>
        /// </summary>
        private Dictionary<string, Tuple<List<Type>, bool>> proxyCategories = new Dictionary<string, Tuple<List<Type>, bool>>();

        //private Dictionary<string, bool> activeCategoryFoldouts = new Dictionary<string, bool>();
        private Dictionary<string, bool> proxyFoldouts = new Dictionary<string, bool>();
        private Dictionary<string, bool> proxyToggles = new Dictionary<string, bool>();

        private AddonCreator addonCreator = null;

        private GUIStyle foldoutStyle;
        private GUIStyle proxyLabelStyle;

        protected AddonCreatorEditorModel model = new AddonCreatorEditorModel();


        public void OnInspectorUpdate()
        {
            // Redraw any inspectors that shows this editor.
            Repaint();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void OnEnable()
        {
            addonCreator = target as AddonCreator;

            if (EditorPrefs.HasKey(nameof(outerFoldout)))
            {
                outerFoldout = EditorPrefs.GetBool(nameof(outerFoldout));
            }

            if (proxyLabelStyle == null)
            {
                proxyLabelStyle = new GUIStyle(EditorStyles.label)
                {
                    richText = true
                };
            }

            if (foldoutStyle == null)
            {
                foldoutStyle = new GUIStyle(EditorStyles.foldout)
                {
                    fixedWidth = 5
                };
            }

            LoadProxyData();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void OnDisable()
        {
            EditorPrefs.SetBool(nameof(outerFoldout), outerFoldout);

            foreach (KeyValuePair<string, Tuple<List<Type>, bool>> entry in proxyCategories)
            {
                EditorPrefs.SetBool(entry.Key, entry.Value.Item2);
            }


            foreach (KeyValuePair<string, bool> entry in proxyToggles)
            {
                string proxyName = entry.Key;
                bool proxyEnabled = entry.Value;
                EditorPrefs.SetBool(proxyName, proxyEnabled);
            }

            return;
        }

        /// <summary>
        ///
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.BeginHorizontal();

            outerFoldout = EditorGUILayout.Foldout(outerFoldout, "", foldoutStyle);

            EditorGUILayout.LabelField("Proxy Objects", EditorStyles.boldLabel);

            GUILayout.EndHorizontal();

            EditorGUI.indentLevel++;


            if (outerFoldout && addonCreator != null)
            {

                for (int i = 0; i < proxyCategories.Count; ++i)
                {
                    KeyValuePair<string, Tuple<List<Type>, bool>> entry = proxyCategories.ToList().ElementAt(i);
                    List<Type> proxiesInCategory = entry.Value.Item1;
                    bool categoryEnabled = entry.Value.Item2;
                    string category = entry.Key;

                    // Foldout for category.
                    proxyCategories[category] = new Tuple<List<Type>, bool>(proxiesInCategory, EditorGUILayout.Foldout(categoryEnabled, category, foldoutStyle));

                    EditorGUI.indentLevel++;

                    if (categoryEnabled)
                    {
                        foreach (Type proxyType in proxiesInCategory)
                        {
                            AddonCreatorAttribute proxyAttribute = Attribute.GetCustomAttribute(proxyType, typeof(AddonCreatorAttribute)) as AddonCreatorAttribute;


                            GUILayout.BeginHorizontal();

                            string proxyKey = AddonCreator.ProxyContainerKey(proxyType);

                            // Foldout for a proxy.
                            proxyFoldouts[proxyKey] = EditorGUILayout.Foldout(proxyFoldouts[proxyKey], "", foldoutStyle);

                            EditorGUIUtility.labelWidth = 10;

                            bool previousToggle = proxyToggles[proxyType.Name];

                            proxyToggles[proxyType.Name] = GUILayout.Toggle(proxyToggles[proxyType.Name], GUIContent.none, GUILayout.ExpandWidth(false));

                            if (previousToggle != proxyToggles[proxyType.Name])
                            {
                                // OnToggleValueChanged
                                model.GetProxyLibrary().SetProxyEnabled(proxyType, proxyToggles[proxyType.Name]);
                            }

                            string color = model.GetProxyLibrary().CanEnable(proxyType) ? "white" : "red";
                            string textFieldValue = "<color=" + color + ">" + proxyKey + "</color>";


                            EditorGUILayout.TextField(textFieldValue, proxyLabelStyle);

                            GUILayout.EndHorizontal();

                            // For this proxy. Show this stuff.
                            if (proxyFoldouts[proxyKey] == true && proxyAttribute != null)
                            {
                                // Description.
                                if (proxyAttribute.Description != null && proxyAttribute.Description != "")
                                {
                                    EditorGUILayout.LabelField(proxyAttribute.Description, EditorStyles.label);
                                }

                                EditorGUILayout.LabelField("Dependencies", EditorStyles.label);

                                // Dependencies.
                                foreach (Type dependency in ProxyLibrary.GetProxyDependenciesForType(proxyType))
                                {
                                    EditorGUILayout.LabelField("   " + dependency.Name.Replace("Proxy", ""), EditorStyles.linkLabel);
                                }
                            }

                        }
                    }
                    EditorGUI.indentLevel--;
                }
            }

            EditorGUI.indentLevel++;
            // Redraw any inspectors that shows this editor.
            Repaint();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void OnValidate()
        {
            return;
        }

        /// <summary>
        ///
        /// </summary>
        private void DisplayDependencyDrowndownList()
        {
            SortedSet<Type> dependencies = new SortedSet<Type>();

            ProxyLibrary.GetAllProxyTypes().ForEach(x => dependencies.UnionWith(ProxyLibrary.GetProxyDependenciesForType(x)));

            foreach (Type dependency in dependencies)
            {
                // Sometimes, AddonCreatorAttributes have types we wish to register on application startup.
                // There are types that inherit from ProxyObject, but others that do not, these are the types
                // that do not.

            }
            return;
        }

        public void DisplayDropDownProxyList(List<Type> types)
        {

        }

        /// <summary>
        ///
        /// </summary>
        private void LoadProxyData()
        {
            List<Type> proxyTypes = ProxyLibrary.GetAllProxyTypes();

            proxyFoldouts.Clear();

            foreach (Type proxyType in proxyTypes)
            {
                // Get the proxy key name, such as (Audio Manager).
                string proxyKey = AddonCreator.ProxyContainerKey(proxyType);

                AddonCreatorAttribute proxyAttribute = ProxyLibrary.GetProxyAddonCreatorAttribute(proxyType);

                if (proxyAttribute.HideInInspector == false)
                {
                    string category = proxyAttribute == null ? "General" : proxyAttribute.Category;

                    // Check if we need to create a new list for a category not seen yet.
                    if (!proxyCategories.ContainsKey(category))
                    {
                        proxyCategories.Add(category, new Tuple<List<Type>, bool>(new List<Type>(), true));

                        //activeCategoryFoldouts.Add(category, true);

                        // Note, Editor Preferences won't be stored with version control.
                        if (EditorPrefs.HasKey(category))
                        {
                            proxyCategories[category] = new Tuple<List<Type>, bool>(proxyCategories[category].Item1, EditorPrefs.GetBool(category));
                            //activeCategoryFoldouts[category] = EditorPrefs.GetBool(category);
                        }
                    }

                    // Get the list for this category.
                    List<Type> proxyTypesInCategory = proxyCategories[category].Item1;

                    // Add this type to the list.
                    proxyTypesInCategory.Add(proxyType);

                    bool proxyEnabled = true;

                    if (EditorPrefs.HasKey(proxyType.Name))
                    {
                        proxyEnabled = EditorPrefs.GetBool(proxyType.Name);
                    }

                    proxyToggles.Add(proxyType.Name, proxyEnabled);

                    model.GetProxyLibrary().SetProxyEnabled(proxyType, proxyEnabled);

                    proxyCategories[category] = new Tuple<List<Type>, bool>(proxyTypesInCategory, proxyCategories[category].Item2); ;

                    proxyFoldouts.Add(proxyKey, EditorPrefs.HasKey(proxyKey) ? EditorPrefs.GetBool(proxyKey) : false);

                    EditorPrefs.SetBool(proxyKey, false);
                }
            }
        }

    }
}
