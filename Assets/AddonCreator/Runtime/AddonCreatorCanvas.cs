﻿using MoonSharp.Interpreter.Interop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace AddonCreator
{
    public class AddonCreatorCanvas : Singleton<AddonCreatorCanvas>
    {
        private Canvas canvas = null;
        private CanvasRenderer canvasRenderer = null;
        private CanvasScaler canvasScaler = null;


        public void Awake()
        {
            canvas = new GameObject().AddComponent<Canvas>();

            SetCanvasName("AddonCreator.CanvasRenderer");

            canvasRenderer = canvas.gameObject.AddComponent<CanvasRenderer>();

            canvasScaler = canvas.gameObject.AddComponent<CanvasScaler>();

            SetRenderMode(RenderMode.ScreenSpaceOverlay);

            SetScaleMode(CanvasScaler.ScaleMode.ScaleWithScreenSize);

            SetReferenceResolution(new Vector2(1680, 1050));

            SetScreenMatchMode(CanvasScaler.ScreenMatchMode.MatchWidthOrHeight);

            SetScalerMatch(0.5f);

            return;
        }

        [MoonSharpVisible(false)]
        public void SetCanvasName(string name)
        {
            canvas.name = name;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetRenderMode(RenderMode renderMode)
        {
            canvas.renderMode = renderMode;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetScaleMode(CanvasScaler.ScaleMode scaleMode)
        {
            canvasScaler.uiScaleMode = scaleMode;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetScaleFactor(float scaleFactor)
        {
            canvasScaler.scaleFactor = scaleFactor;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetReferenceResolution(Vector2 resolution)
        {
            canvasScaler.referenceResolution = resolution;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetScreenMatchMode(CanvasScaler.ScreenMatchMode screenMatchMode)
        {
            canvasScaler.screenMatchMode = screenMatchMode;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetScalerMatch(float match)
        {
            canvasScaler.matchWidthOrHeight = match;
            return;
        }

        [MoonSharpVisible(false)]
        public void SetReferencePixelsPerUnit(float referencePixelsPerUnit)
        {
            canvasScaler.referencePixelsPerUnit = referencePixelsPerUnit;
            return;
        }

        [MoonSharpVisible(false)]
        public Canvas GetCanvas()
        {
            return canvas;
        }
    }
}
