﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using MoonSharp.Interpreter;
using System.Linq;
using System;
using System.IO;
using System.Text.RegularExpressions;
using MoonSharp.Interpreter.Loaders;

using MoonSharp.Interpreter.Interop;
using System.Xml;


namespace AddonCreator
{
	public enum ClientLanguage
	{
		English,
		Spanish,
		French,
		German
	}

	/// <summary>
	///
	/// </summary>
	public class AddonCreator : Singleton<AddonCreator>
	{
		/// <summary>
		///
		/// </summary>
		public static List<IDebugOutput> debugOutputs = new List<IDebugOutput>();

		/// <summary>
		/// All the addon manifests that could be found.
		/// </summary>
		protected SortedSet<AddonManifest> manifests = new SortedSet<AddonManifest>();

		/// <summary>
		/// Set the client to a particual language and the addons can read this value via variable expansion.
		/// </summary>
		[SerializeField]
		protected ClientLanguage language = ClientLanguage.English;

		/// <summary>
		/// <field>apiVersion</field> is the current version of the API. It is expected that addons
		/// conform to this version.
		/// </summary>
		[SerializeField]
		protected string apiVersion;

		/// <summary>
		/// Does this application require that addons match the expected API version?
		/// </summary>
		[SerializeField]
		protected bool requireUpToDateApiVersion;

		[SerializeField]
		protected List<ManifestDirective> requiredDirectives = null;

		[SerializeField]
		protected bool logWarnings = true;

		[SerializeField]
		protected CoreModules coreModules = CoreModules.None;


		private Dictionary<Type, IAddonCreatorBehavior> globalProxyTargets = new Dictionary<Type, IAddonCreatorBehavior>();

		public EventManagerTarget eventManager;

		protected Script globalScript = null;

		protected Script GlobalScript
		{
			get
			{
				return globalScript;
			}
			private set
			{
				globalScript = value;
				return;
			}
		}

		public Table GlobalTable
		{
			get
			{
				return GlobalScript.Globals;
			}
		}

		public List<ProxyObject> RegisteredProxyObjects
		{
			get;
			protected set;
		} = new List<ProxyObject>();


		public bool IsProxyRegistered()
		{
			return false;
		}


		/// <summary>
		/// // Exposed C# function to lua.
		/// </summary>
		/// <param name="message"></param>
		public virtual void Print(string message)
		{
			Debug.Log(message);
			return;
		}

		/// <summary>
		///
		/// </summary>
		public virtual void OnDestroy()
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		public virtual void OnEnable()
		{

			return;
		}

		/// <summary>
		///
		/// </summary>
		public virtual void Awake()
		{
			if (requiredDirectives == null || requiredDirectives.Count == 0)
			{
				requiredDirectives = new List<ManifestDirective>
				{
					ManifestDirective.Title,
				};
			}

			// Remove any duplicate required directives.
			requiredDirectives = requiredDirectives.Distinct().ToList();

			//addonCreatorCanvas = AddonCreatorCanvas.Instance();
			//addonCreatorCanvas.GetCanvas().transform.SetParent(this.transform);

			// https://github.com/Semaeopus/Unity-Lua
			//eventManagerProxy = new EventManagerProxy(EventManager.Instance());

			// The following can be used to specify that all newly created scripts
			// should use this script loader:

			GlobalScript = new Script();
			GlobalScript.Options.UseLuaErrorLocations = true;
			GlobalScript.Options.ScriptLoader = new FileSystemScriptLoader();

			Script.DefaultOptions.UseLuaErrorLocations = true;

			//LoadProxyToggledData();

			UserData.RegisterAssembly();

			LoadDebugOutputs();

			LoadDataTypes(GlobalScript);

			LoadGlobals(GlobalScript);

			// These are objects that exist [CanvasManager Proxy, Enum Types (Left, Bottom, TopLeft, Etc)]
			GlobalScript.Globals.Values.ToList().ForEach(

				x =>
				{
					if (x.UserData != null && x.UserData.Object != null)
					{
						//Debug.Log(x.UserData.Object.GetType() + " : " + x.UserData.Object);
					}
				});

			Debug.Log(" -- -- -- --");
			Debug.Log(" -- -- -- --");
			Debug.Log(" -- -- -- --");


			UserData.GetRegisteredTypes().ToList().ForEach(
				x =>
				{
					if (x != null )
					{
						Debug.Log(x);
					}
				});

			// We use the Streaming Assets Folder to allow these addons to be visible to the users.
			// TODO: We may need some special folder for hidden addon lua files?
			// Maybe I can compiled lua scripts?
			// We can also have two locations where we store scripts. One in unity assets, so they are hidden
			// from the User's user.
			try
			{
				// [Protected Functions]
				// Try to implement some form of way owner creators can protect their functions.
				// CallSecureFunction("funcName", param1, param2, etc..)
				// SecureFunction would have a bool permissable field that would be set by in-game events.
				// For example, a bool that is set when the user is in combat.

				// TODO: Let the script loader handle this part????
				// https://www.moonsharp.org/scriptloaders.html

				LoadInternalLuaScripts();

			}
			catch (Exception exception)
			{
				Debug.LogError(string.Format("AddonCreator could not load internal lua scripts. {0}", exception.Message));
			}

			try
			{
				SortedSet<AddonManifest> externalAddons = LoadExternalLuaAddons();

				//eventManager = globalProxyTargets[typeof(EventManager)] as EventManager;

				eventManager = new EventManagerTarget(GlobalScript);

				foreach (AddonManifest addonManifest in externalAddons)
				{
					// Call AddonLoaded event for this addon.
					if (eventManager != null)
					{
						eventManager.RaiseCallback(GameEvent.AddonLoaded, new DynValue[] { DynValue.NewString(addonManifest.title) });
					}
				}
			}
			catch (Exception exception)
			{
				Debug.LogError(string.Format("AddonCreator could not load external lua scripts. {0}", exception.Message));
			}

			if (eventManager == null)
			{
				Debug.LogError("EventManager does not exist.");
			}
			return;
		}

		/// <summary>
		/// Initialize addons in start, this gives us time to build the scene object references that may
		/// be used in addons.
		/// </summary>
		public virtual void Start()
		{

			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="directory"></param>
		protected void LoadInternalLuaScripts()
		{
			// Internal addons shouldn't need any specific directives defined. Probably.
			List<ManifestDirective> directives = new List<ManifestDirective>() { };

			// Read in all the internal manifests from the resources folder.
			SortedSet<AddonManifest> internalManifests = ParseAddonManifestsInternal(directives);

			// Load all scripts.
			internalManifests.ToList().ForEach(x =>
			{
				LoadManifest(x);
			});

			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="directory"></param>
		protected SortedSet<AddonManifest> LoadExternalLuaAddons()
		{
			// Read in all the manifests from the streaming assets folder.
			SortedSet<AddonManifest> externalManifests = GetAllExternalManifests(Path.Combine(Application.streamingAssetsPath, "Addons"), requiredDirectives);

			if (externalManifests.Count > 0)
			{
				// Load all scripts.
				foreach (AddonManifest manifest in externalManifests)
				{
					LoadManifest(manifest);
				}

				foreach (AddonManifest manifest in externalManifests)
				{
					// Load the saved vars for this addon.
					AddonLoadSavedVars(manifest);
				}
			}
			return externalManifests;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="addonManifest"></param>
		protected virtual void AddonLoadSavedVars(AddonManifest addonManifest)
		{
			if (addonManifest.title == null || addonManifest.title == "")
			{
				throw new ArgumentException("Addon's title was missing. Can't load saved vars for an addon that has not title.");
			}

			AddonReader addonReader = new AddonReader();
			addonReader.ReadModule(Path.Combine(Application.streamingAssetsPath, "SavedVars" + addonManifest.title + ".lua"), GlobalScript);
			return;
		}

		/// <summary>
		///
		/// </summary>
		protected bool LoadManifest(AddonManifest addonManifest)
		{
			if (addonManifest == null)
			{
				return false;
			}

			bool loaded = true;

			// We need to read dependencies first.
			foreach (AddonDependency dependency in addonManifest.dependencies)
			{
				// Call recursive.
				LoadManifest(FindManifest(dependency.name, dependency.version));
			}


			AddonReader addonReader = new AddonReader();
			foreach (AddonModule module in addonManifest.modules)
			{
				bool success = addonReader.ReadModule(module, GlobalScript);
				if (!success)
				{
					loaded = false;
				}
			}

			return loaded;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="name"></param>
		/// <param name="version"></param>
		/// <returns></returns>
		public AddonManifest FindManifest(string name, string version)
		{
			AddonManifest manifest = manifests.FirstOrDefault(x => x.title.Equals(name) && x.version == version);
			return manifest ?? null;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="directory"></param>
		/// <returns></returns>
		protected SortedSet<AddonManifest> ParseAddonManifestsInternal(IEnumerable<ManifestDirective> directives)
		{
			SortedSet<AddonManifest> manifests = new SortedSet<AddonManifest>();

			string path = "AddonCreator";

			TextAsset[] textAssets = Resources.LoadAll<TextAsset>(path);

			foreach (TextAsset asset in textAssets)
			{
				if (asset.text.StartsWith("<?xml version="))
				{
					AddonReader reader = new AddonReader();
					if (!reader.ParseAddonManifestFromContents(asset.text, out AddonManifest manifest))
					{

					}
					else
					{
						manifests.Add(manifest);
					}
					//AddonManifest addonManifest = ReadAddonManifest(filepath);
					//XmlDocument xmldoc = new XmlDocument();
					//xmldoc.LoadXml(asset.text);
					//Debug.Log(asset.name);
				}
			}

			textAssets.Where(x => {
				return x.text.StartsWith("<?xml ");
			}).ToList().ForEach(x =>
			{
				TextAsset assett = x as TextAsset;
				XmlDocument xmldoc = new XmlDocument();
				xmldoc.LoadXml(assett.text);
			});


			return manifests;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="directory"></param>
		/// <returns></returns>
		protected SortedSet<AddonManifest> GetAllExternalManifests(string directory, IEnumerable<ManifestDirective> directives)
		{
			SortedSet<AddonManifest> manifests = new SortedSet<AddonManifest>();

			try
			{
				if (!Directory.Exists(directory))
				{
					throw new DirectoryNotFoundException("Could not find addon folder with path: " + directory);
				}

				string[] directories = Directory.GetDirectories(directory, "*", SearchOption.AllDirectories);

				foreach (string subDirectory in directories)
				{
					IEnumerable<string> files = Directory.GetFiles(subDirectory).Where(x => x.EndsWith(".xml"));

					foreach (string filepath in files)
					{
						AddonManifest addonManifest = ParseManifestMetadata(filepath);

						if (!CheckRequiredDirectives(addonManifest, directives))
						{
							if (logWarnings)
							{
								Debug.LogWarning(string.Format("Addon Manifest missing requirements. {0}", addonManifest));
							}
						}
						else
						{
							try
							{
								manifests.Add(addonManifest);
							}
							catch (ArgumentNullException exception)
							{
								if (logWarnings)
								{
									Debug.LogWarning(exception.Message);
								}
							}
							catch (Exception exception)
							{
								Debug.LogError(exception.Message);
							}
						}
					}
				}

			}
			catch (Exception exception)
			{
				Debug.LogErrorFormat("Error initializing AddonCreatorAssetLoader : {0}", exception);
			}

			return manifests;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="addonManifest"></param>
		/// <param name="directives"></param>
		/// <returns></returns>
		private bool CheckRequiredDirectives(AddonManifest addonManifest, IEnumerable<ManifestDirective> directives)
		{
			if (requireUpToDateApiVersion && !addonManifest.api.Equals(apiVersion))
			{
				if (logWarnings)
				{
					Debug.LogWarning(string.Format("API Version for {0} does not match application's API Verson: {1}", addonManifest.title, apiVersion));
				}
			}

			bool accepted = true;

			foreach (ManifestDirective directive in directives)
			{
				if (accepted)
				{
					switch (directive)
					{
						case ManifestDirective.Title:
							// Require that the Title must not be null or empty.
							accepted = !String.IsNullOrEmpty(addonManifest.title);
							break;
						case ManifestDirective.Date:
							accepted = DateTime.TryParse(addonManifest.date, out DateTime dateValue);
							break;
						case ManifestDirective.ApiVersion:
							accepted = addonManifest.api.Equals(apiVersion);
							break;
						case ManifestDirective.Description:
							accepted = !String.IsNullOrEmpty(addonManifest.description);
							break;
						default:
							break;
					}
				}
			}
			return accepted;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="filepath"></param>
		/// <returns></returns>
		public virtual AddonManifest ParseManifestMetadata(string filepath)
		{
			AddonManifest addonManifest = null;
			AddonReader addonReader = new AddonReader
			{
				logWarnings = logWarnings
			};

			if (!filepath.EndsWith(".xml"))
			{
				throw new ArgumentException("File is not an XML Document!");
			}
			try
			{
				 addonReader.ParseAddonManifestFromPath(filepath, out addonManifest);
			}
			catch (Exception exception)
			{
				Debug.LogError("Could not read AddonManifest. " + exception.Message);
			}
			return addonManifest;
		}

		/// <summary>
		///
		/// </summary>
		public void Update()
		{
			// Each ProxyTarget will have its Update() function called from the abstract ProxyTarget class.
			foreach (IAddonCreatorBehavior target in globalProxyTargets.Values)
			{
				IAddonCreatorBehavior proxyObject = target as IAddonCreatorBehavior;
				proxyObject?.Update();
			}
			return;
		}

		public void LoadEnums(Script script)
		{

		}

		public delegate dynamic MyDelegate(dynamic target);

		/// <summary>
		///
		/// </summary>
		/// <param name="script"></param>
		public void LoadDataTypes(Script script)
		{
			Debug.Log("AddonCreator: LoadDataTypes()");


			// Check entire project for Enums with [AddonCreatorAttribute]
			IEnumerable<Type> addonCreatorEnumAttributes = Assembly.GetExecutingAssembly()
					.GetTypes()
					.Where(t => t.IsEnum && t.IsPublic && ProxyLibrary.HasAttribute<AddonCreatorEnum>(t));

			foreach (Type enumType in addonCreatorEnumAttributes)
			{
				UserData.RegisterType(enumType);
				RegisterEnum(enumType, GlobalScript);
			}

			IEnumerable<Type> addonCreatorProxyAttributes = Assembly.GetExecutingAssembly()
					.GetTypes()
					.Where(t => t.IsPublic && Attribute.GetCustomAttribute(t, typeof(AddonCreatorAttribute)) != null);

			IEnumerable<Type> proxyWrappers = Assembly.GetExecutingAssembly()
					.GetTypes()
					.Where(t => t.IsPublic && typeof(IProxyFactory).IsAssignableFrom(t));

			// Registering the asseMblys you need to add [MoonSharpUserData] to the data you want to expose more under http://www.moonsharp.org/objects.html

			foreach (Type proxyType in addonCreatorProxyAttributes)
			{
				//IProxyFactory iProxyFactory = (IProxyFactory)Activator.CreateInstance(proxyType);
				//Assembly assembly = Assembly.GetExecutingAssembly();

				MethodInfo method = proxyType.GetMethod("Register");

				//As the comment points out, you may want to ensure the method is static when calling GetMethod:

				// tempClass.GetMethod("Run", BindingFlags.Public | BindingFlags.Static).Invoke(null, null)

				if (method != null)
				{
					method.Invoke(null, null);
					Debug.Log("Registering proxy type: " + proxyType);
					script.Globals[proxyType.Name.Replace("Proxy", "")] = proxyType;
				}


				//UserData.RegisterProxyType()

				// Replace all proxy names in the Global table. Example: Vector3Proxy => Vector3
				// This exposes Vector.__new(), Vector.zero, Vector3.one, etc..

				//if (proxy != null)
				//{
				//IAddonCreatorBehavior luaInstanceOfObject = null;
				//if (luaInstanceOfObject != null)
				//{
				//globalProxyTargets.Add(luaInstanceOfObject.GetType(), luaInstanceOfObject);

				//luaInstanceOfObject.Start();
				//}
				//}

			}

			// Check entire project for Functions with [AddonCreatorFunction]
			MethodInfo[] methods = Assembly.GetExecutingAssembly().GetTypes()
					  .SelectMany(t => t.GetMethods())
					  .Where(m => m.GetCustomAttributes(typeof(AddonCreatorFunction), false).Length > 0)
					  .ToArray();

			// Register all the types we need for CLR conversion.
			ProxyLibrary.GetAllProxyTypes().ForEach(proxyType => ProxyLibrary.GetProxyRegisterTypesForType(proxyType).ForEach(
				clrType => UserData.RegisterType(clrType))
			);

			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="script"></param>
		public virtual void LoadGlobals(Script script)
		{
			Debug.Log("AddonCreator: LoadGlobals()");

			//script.Globals["CANVAS_MANAGER"] = new CanvasManagerProxy();


			return;
		}

		/// <summary>
		///
		/// </summary>
		public void LoadDebugOutputs()
		{
			Debug.Log("AddonCreator: LoadDebugOutputs()");
			IEnumerable<Type> debugOutputs = Assembly.GetExecutingAssembly()
				.GetTypes().Where(t => t.IsPublic && typeof(IDebugOutput).IsAssignableFrom(t));
			debugOutputs.ToList().ForEach(
				x => RegisterDebugOutput(x)
				);
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <returns></returns>
		private float DeltaTime()
		{
			return Time.deltaTime;
		}

		/// <summary>
		///
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="script"></param>
		protected void RegisterEnum(Type type,  Script script)
		{
			foreach (object enumValue in Enum.GetValues(type))
			{
				script.Globals[Enum.GetName(type, enumValue)] = enumValue;
			}
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="addonName"></param>
		/// <param name="script"></param>
		protected void LoadAddonSavedVars(string addonName, ref Script script)
		{
			string savedVarsPath = Path.Combine(GetSavedVarsPath(addonName));

			Debug.Log("SavedVarsPath: " + savedVarsPath);
			if (System.IO.File.Exists(savedVarsPath))
			{
				Debug.Log("SavedVarsPath: " + savedVarsPath);
				string luaCode = File.ReadAllText(savedVarsPath);
				script.DoString(luaCode);
			}
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="addonName"></param>
		/// <returns></returns>
		public string GetSavedVarsPath(string addonName)
		{
			return Path.Combine(Application.streamingAssetsPath, "Addons/SavedVariables/" + addonName + ".lua");
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="proxyType"></param>
		/// <returns></returns>
		public static string ProxyContainerKey(Type proxyType)
		{
			// https://stackoverflow.com/questions/272633/add-spaces-before-capital-letters
			return Regex.Replace(proxyType.Name.Replace("Proxy", ""), "([a-z])([A-Z])", "$1 $2");
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="output"></param>
		public static void RegisterDebugOutput(Type output)
		{
			if (output == typeof(IDebugOutput))
			{
				// Don't create instance of the interface directly.
				return;
			}

			if (typeof(IDebugOutput).IsAssignableFrom(output))
			{
				debugOutputs.Add((IDebugOutput)Activator.CreateInstance(output));
			}
			else
			{
				Debug.LogError("Type: " + output + " is not assignable from" + typeof(IDebugOutput));
			}

			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <returns></returns>
		public static List<IDebugOutput> MoonsharpIOs()
		{
			return debugOutputs;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="code"></param>
		public DynValue DoString(string code)
		{
 			DynValue result = null;

			Debug.Assert(GlobalScript != null);
			Debug.Assert(GlobalTable != null);

			try
			{
				result = GlobalScript.DoString(code);
			}
			catch (Exception exception)
			{
				Debug.Log(exception.Message);
			}



			return result;
		}
	}
}
