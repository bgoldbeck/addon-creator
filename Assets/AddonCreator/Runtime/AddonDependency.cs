﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public class AddonDependency
    {
        public readonly string name;
        public readonly string version;

        public AddonDependency(string name, string version)
        {
            this.name = name;
            this.version = version;
        }
    }
}
