﻿using MoonSharp.Interpreter;
using System;
using System.IO;
using System.Xml;
using UnityEngine;


namespace AddonCreator
{
	/// <summary>
	///
	/// </summary>
	public enum ManifestDirective
	{
		Title = 0,
		ApiVersion = 1,
		Version = 2,
		Date = 3,
		Description = 4,
	}

	/// <summary>
	///
	/// </summary>
	public class AddonReader
	{
		public bool logWarnings = true;
		public AddonManifest addonManifest;
		public ClientLanguage language = ClientLanguage.English;


		public AddonReader()
		{
		}


		/// <summary>
		///
		/// </summary>
		/// <param name="module"></param>
		/// <param name="script">The script context</param>
		/// <returns>True, if the script file could be read on disk.</returns>
		public virtual bool ReadModule(AddonModule module, Script script)
		{
			if (script == null)
			{
				return false;
			}

			bool success = true;

			try
			{
				script.DoString(module.content, script.Globals);
				module.SetLoaded(true);
			}
			catch (ScriptRuntimeException exception)
			{
				Debug.LogWarning(exception + " : " +  Environment.NewLine + exception.DecoratedMessage);
				success = false;
			}
			catch (Exception exception)
			{
				Debug.LogWarning(exception);
				success = false;
			}
			return success;
		}

		/// <summary>
		/// Parse the contents of a file as code.
		/// </summary>
		/// <param name="filepath">The file to read as a script file</param>
		/// <param name="script">The script context</param>
		/// <returns>True, if the script file could be read on disk.</returns>
		public virtual bool ReadModule(string filepath, Script script)
		{
			if (!File.Exists(filepath))
			{
				return false;
			}

			return ReadModule(new AddonModule(Path.GetFileName(filepath).Replace(".lua", ""), File.ReadAllText(filepath)), script);
		}

		/// <summary>
		/// Read the contents of a text file as an addon manifest.
		/// </summary>
		/// <param name="filepath">The text file to read.</param>
		/// <returns>True, if the Addon Manifest file could be read correctly.</returns>
		public virtual bool ParseAddonManifestFromPath(string filepath, out AddonManifest addonManifest)
		{
			return ParseAddonManifestFromContents(File.ReadAllText(filepath), out addonManifest, filepath);
		}

		/// <summary>
		/// Parse the contents of an addon manifest file.
		/// </summary>
		/// <param name="filepath">The text file to read.</param>
		/// <param name="fileContents"></param>
		/// <param name="addonManifest"></param>
		/// <returns>True, if the Addon Manifest file contents could be parsed correctly.</returns>
		public virtual bool ParseAddonManifestFromContents(string fileContents, out AddonManifest addonManifest, string filepath = "")
		{
			addonManifest = new AddonManifest();
			addonManifest.xmlFilepath = filepath;

			bool success = true;

			try
			{
				XmlReader xmlReader = XmlReader.Create(new StringReader(fileContents));

				while (xmlReader.Read())
				{
					if (xmlReader.HasAttributes)
					{
						if (xmlReader.NodeType == XmlNodeType.Element)
						{
							if (xmlReader.Name == "Module")
							{
								string path = xmlReader.GetAttribute("path");
								string moduleContents = "";

								if (path.StartsWith("AddonCreator/"))
								{
									// This means we are loading an internal addon.
									path = System.IO.Path.ChangeExtension(path, null);
									TextAsset textAsset = Resources.Load<TextAsset>(path);
									if (textAsset == null)
									{
										throw new NullReferenceException("TextAsset at resource path: " + path + " was null.");
									}
									moduleContents = textAsset.text;
								}
								else
								{
									// string path is the relative path IE: SomeModule.lua
									moduleContents = File.ReadAllText(Path.Combine(Path.GetDirectoryName(filepath), path));
								}

								if (!String.IsNullOrEmpty(moduleContents))
								{
									// The contents of the module.

									addonManifest.modules.Add(new AddonModule(Path.GetFileNameWithoutExtension(path), moduleContents));
								}
							}
							else if (xmlReader.Name == "Dependency")
							{
								string name = xmlReader.GetAttribute("name");
								string version = xmlReader.GetAttribute("version");

								if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(version))
								{
									addonManifest.dependencies.Add(new AddonDependency(name, version));
								}
							}
							else if (xmlReader.Name == "Directives")
							{
								addonManifest.title = xmlReader.GetAttribute("title");
								addonManifest.version = xmlReader.GetAttribute("version");
								addonManifest.api = xmlReader.GetAttribute("api");
								addonManifest.date = xmlReader.GetAttribute("date");
								addonManifest.description = xmlReader.GetAttribute("description");
								addonManifest.author = xmlReader.GetAttribute("author");
								addonManifest.email = xmlReader.GetAttribute("email");

								try
								{
									// Get state of addon and determine if it's a library type or otherwise.
									if (xmlReader.MoveToAttribute("isLibrary"))
									{
										addonManifest.isLibrary = xmlReader.ReadContentAsBoolean();
									}
								}
								catch (Exception exception)
								{
									if (logWarnings)
									{
										Debug.LogWarning("Could not read isLibrary Attribute in xml file. " + exception.Message);
									}
								}

							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				success = false;
				Debug.LogWarning("Could not read manifest file. " + exception.Message);
			}

			return success;
		}

	}
}
