﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp;
using MoonSharp.Interpreter;


namespace AddonCreator
{
	[AddonCreatorEnum]
	public enum GameEvent
	{
		/// <summary>
		/// This event fires whenever an AddOn is loaded (fires once for each AddOn loaded if multiple
		/// AddOns are being loaded), whether that is during the inital Loading Phase.
		/// </summary>
		AddonLoaded,

		/// <summary>
		///
		/// </summary>
		OnUpdate,

		// Combat
		UpdateTarget,
		PowerUpdate,
		EffectChanged,

		// Inventory
		InventoryFull,
		InventoryOverWeightCapacity,
		InventoryItemUpdate,
	}


	/// <summary>
	///
	/// </summary>
	public class EventManagerTarget : ProxyTarget<EventManagerTarget>, IAddonCreatorBehavior
	{

		private Dictionary<GameEvent, List<Tuple<string, DynValue>>> callbacks =
			new Dictionary<GameEvent, List<Tuple<string, DynValue>>>();

		protected Script script = null;


		public EventManagerTarget()
		{
			Wrap(this);
		}

		public EventManagerTarget(Script script)
		{
			if (script == null)
			{
				Debug.LogError("Script instance was null.");
			}
			this.script = script;
		}

		/// <summary>
		///
		/// </summary>
		public void Start()
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		public void Update()
		{
			RaiseCallback(GameEvent.OnUpdate, null);
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="addonName"></param>
		/// <param name="gameEvent"></param>
		/// <param name="dynValue"></param>
		public void RegisterEvent(string addonName, GameEvent gameEvent, DynValue dynValue)
		{
			if (dynValue.Type != DataType.Function)
			{
				Debug.LogError("Registering non function type");
				return;
			}

			if (addonName == "" || addonName == null)
			{
				Debug.LogError("Missing addon name.");
				return;
			}

			if (!callbacks.ContainsKey(gameEvent))
			{
				callbacks.Add(gameEvent, new List<Tuple<string, DynValue>>());
			}

			callbacks[gameEvent].Add(new Tuple<string, DynValue>(addonName, dynValue));

			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="gameEvent"></param>
		public void RaiseCallback(GameEvent gameEvent, DynValue[] args = null)
		{
			if (callbacks.ContainsKey(gameEvent))
			{
				foreach (Tuple<string, DynValue> callback in callbacks[gameEvent])
				{
					if (callback != null)
					{
						try
						{
							if (args != null)
							{
								script.Call(callback.Item2, args);
							}
							else
							{
								script.Call(callback.Item2);
							}
						}
						catch (ScriptRuntimeException exception)
						{
							Debug.LogWarning(
								"Doh! An error occured!" + System.Environment.NewLine + exception.DecoratedMessage +
								gameEvent + " args: " + args);
						}
						catch (SyntaxErrorException exception)
						{
							Debug.LogWarning("Doh! A syntax error occured!" + System.Environment.NewLine + exception.DecoratedMessage);
						}
						catch (Exception exception)
						{
							Debug.LogWarning("Doh! An error occured!" + System.Environment.NewLine + exception);
						}
					}
				}
			}
			return;
		}


		/*
		/// <summary>
		///
		/// </summary>
		/// <param name="target"></param>
		public void UpdateTarget(Entity target)
		{
			Debug.Log("RaiseCallback GameEvent - Target targeted: " + target);
			RaiseCallback(GameEvent.UpdateTarget);
			if (target != null)
			{
				target.GetComponent<ResourcePools>().ResourcePoolUpdateEvent.AddListener(TargetResourcePoolUpdated);
			}
			else
			{
				target.GetComponent<ResourcePools>().ResourcePoolUpdateEvent.RemoveListener(TargetResourcePoolUpdated);
			}
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="powerType"></param>
		/// <param name="power"></param>
		/// <param name="powerMax"></param>
		public void TargetResourcePoolUpdated(PowerType powerType, int power, int powerMax)
		{
			ResourcePoolUpdated("target", powerType, power, powerMax);
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="powerType"></param>
		/// <param name="power"></param>
		/// <param name="powerMax"></param>
		public void PlayerResourcePoolUpdated(PowerType powerType, int power, int powerMax)
		{
			ResourcePoolUpdated("player", powerType, power, powerMax);
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="powerType"></param>
		/// <param name="power"></param>
		/// <param name="powerMax"></param>
		public void ResourcePoolUpdated(string unitTag, PowerType powerType, int power, int powerMax)
		{
			RaiseCallback(
				GameEvent.PowerUpdate,
				new DynValue[] {
				DynValue.NewString(unitTag),
				DynValue.NewNumber(System.Convert.ToInt32(powerType)),
				DynValue.NewNumber(power),
				DynValue.NewNumber(powerMax)
				});
			return;
		}

		/// <summary>
		///
		/// </summary>
		public void PlayerLoaded(Player player)
		{
			player.GetComponent<ResourcePools>().ResourcePoolUpdateEvent.AddListener(PlayerResourcePoolUpdated);
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newHealth"></param>
		public void HealthChanged(int newHealth)
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newMaxHealth"></param>
		public void HealthMaxChanged(int newMaxHealth)
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newMana"></param>
		public void ManaChanged(int newMana)
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newMaxMana"></param>
		public void ManaMaxChanged(int newMaxMana)
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newStamina"></param>
		public void StaminaChanged(int newStamina)
		{
			return;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="newMaxStamina"></param>
		public void StaminaMaxChanged(int newMaxStamina)
		{
			return;
		}
		*/
	}
}
