﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;


namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class DragHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField]
        private bool clampedToScreen = true;

        [SerializeField]
        private bool disableOnRightClick = false;

        [SerializeField]
        private bool destroyOnRightClick = false;

        private CanvasGroup canvasGroup = null;

        private List<IDragHandlerListener> callbacks = new List<IDragHandlerListener>();

        private Vector3 clickedPosition;
        private Vector3 offset;
        private RectTransform rectTransform;
        private Canvas canvas = null;

        /// <summary>
        ///
        /// </summary>
        /// <param name="listener"></param>
        public void AddListener(IDragHandlerListener listener)
        {
            callbacks.Add(listener);
            return;
        }

        public void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
            canvas = GetComponentInParent<Canvas>();
            return;
        }

        public void Start()
        {
            return;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                transform.position = Input.mousePosition + offset;
                if (clampedToScreen)
                {
                    ClampToScreen();
                }
            }
            return;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                Camera.main.GetComponent<ICamera>().DisallowInput();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                canvasGroup.blocksRaycasts = false;
                foreach (IDragHandlerListener callback in callbacks)
                {
                    callback.OnDragStart();
                }
                clickedPosition = eventData.position;
                offset = transform.position - clickedPosition;
            }
            return;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                Camera.main.GetComponent<ICamera>().AllowInput();
                canvasGroup.blocksRaycasts = true;
                foreach (IDragHandlerListener callback in callbacks)
                {
                    callback.OnDragEnd();
                }
            }
            else if (destroyOnRightClick && eventData.button == PointerEventData.InputButton.Right)
            {
                Destroy(gameObject);
            }
            else if (disableOnRightClick && eventData.button == PointerEventData.InputButton.Right)
            {
                gameObject.SetActive(false);
            }
            return;
        }

        /// <summary>
        /// Clamp panel to area of canvas.
        /// </summary>
        void ClampToScreen()
        {
            Vector3 clampedPositionScreenSpace = rectTransform.anchoredPosition * canvas.scaleFactor;
            Vector2 sizeInScreenSpace = rectTransform.sizeDelta * canvas.scaleFactor;

            Debug.Log("+canvas xMax " + canvas.pixelRect.xMax);
            clampedPositionScreenSpace.x = Mathf.Clamp(clampedPositionScreenSpace.x, 0f, Screen.width - sizeInScreenSpace.x);
            clampedPositionScreenSpace.y = Mathf.Clamp(clampedPositionScreenSpace.y, -Screen.height + sizeInScreenSpace.y, 0f);

            // Convert back to cavnas space.
            rectTransform.anchoredPosition = clampedPositionScreenSpace / canvas.scaleFactor;
            return;
        }
    }
}
