﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;


namespace AddonCreator
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ItemDragHandler : MonoBehaviour //, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        //[SerializeField]
        //protected ActionItemUI actionItemUI = null;

        [SerializeField]
        protected GameObject ownerContainer = null;

        //[SerializeField]
        //private bool removeFromOriginal = false;

        //private CanvasGroup canvasGroup = null;

        //private Transform originalParent = null;

        //private bool isHovering = false;

        //public ActionItemUI ActionItemUI => actionItemUI;

        //private ActionItemUI payload = null;
        /*
        /// <summary>
        ///
        /// </summary>
        public GameObject OwnerContainer
        {
            get
            {
                return ownerContainer;
            }
            private set
            {
                ownerContainer = value;
                return;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void Start()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            ownerContainer = gameObject;
            return;
        }

        /// <summary>
        ///
        /// </summary>
        private void OnDisable()
        {
            if (isHovering)
            {
                //onMouseEndHoverItem.Raise();
                isHovering = false;
            }
            return;
        }

        /// <summary>
        /// Invoked when the left mouse button is pressed down.
        /// </summary>
        /// <param name="eventData"></param>
        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                originalParent = transform.parent;
                if (removeFromOriginal)
                {
                    transform.SetParent(transform.root);
                }
                else
                {
                    payload = GameObject.Instantiate(actionItemUI);
                    payload.GetComponentInChildren<UnityEngine.UI.RawImage>().raycastTarget = false;
                    payload.transform.SetParent(transform.parent);
                    payload.transform.localScale = Vector3.one;
                    payload.transform.position = eventData.position;
                }
                Camera.main.GetComponent<ICamera>().DisallowInput();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                canvasGroup.blocksRaycasts = false;
            }
            return;
        }

        /// <summary>
        /// Invoked when the mouse is dragging this context and has moved.
        /// </summary>
        /// <param name="eventData"></param>
        public virtual void OnDrag(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (removeFromOriginal)
                {
                    transform.position = Input.mousePosition;
                }
                else
                {
                    payload.transform.position = Input.mousePosition;
                }
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="eventData"></param>
        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (removeFromOriginal)
                {
                    transform.SetParent(originalParent);
                    transform.localPosition = Vector3.zero;
                }
                else
                {
                    Destroy(payload.gameObject);
                    payload = null;
                }
                Camera.main.GetComponent<ICamera>().AllowInput();
                canvasGroup.blocksRaycasts = true;
            }
            return;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            //onMouseStartHoverItem.Raise(actionItemUI.SlotItem);
            isHovering = true;
            return;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //onMouseEndHoverItem.Raise();
            isHovering = false;
            return;
        }
        */
    }
}
