﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;
using MoonSharp.Interpreter.Interop;

/*
namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        // Category.
        "Graphic User Interface",
        // Description.
        "Base class proxy for all UI controls.",
        // Dependencies
        new Type[] { },
        // Register Types
        new Type[] { },
        // Should be hidden in inspector?
        true
    )]
    public class AddonCreatorGlobalProxy : MoonsharpProxy<AddonCreatorGlobalProxy, AddonCreatorGlobalTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            AddonCreatorGlobalProxy proxyDelegate(CanvasManagerTarget r) => new CanvasManagerProxy(r);
            UserData.RegisterProxyType((Func<CanvasManagerTarget, CanvasManagerProxy>)proxyDelegate);
            return;
        }

        public AddonCreatorGlobalProxy() : base(target => { return new AddonCreatorGlobalProxy(); })
        {
        }


        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        protected static float GetGameTimeInSeconds()
        {
            return Time.time;
        }

    }
}
*/
