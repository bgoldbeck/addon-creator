﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


namespace AddonCreator
{

    /// <summary>
    ///
    /// </summary>
    [
    AddonCreatorAttribute(
        "Graphical User Interface", // Category
        "Base class for all UI Elements.", // Description
        new Type[] { }, // Dependency Types
        new Type[] { }, // CLR Types
        false // HideInInspector
    )]
    public class UIElementProxy : MoonsharpProxy<UIElementProxy, UIElementTarget>//, IDragHandlerListener
    {

        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            UIElementProxy proxyDelegate(UIElementTarget r) => new UIElementProxy(r);
            UserData.RegisterProxyType((Func<UIElementTarget, UIElementProxy>)proxyDelegate);
            return;
        }

        protected UIElementProxy parent = null;

        // We don't want script makers to be able to use the constructor directly.
        [MoonSharpHidden]
        public UIElementProxy()
        {
        }

        // We don't want script makers to be able to use the constructor directly.
        [MoonSharpHidden]
        public UIElementProxy(UIElementTarget t)
        {
        }

        /// <summary>
        /// Set the parent of this control to the paramter called parent.
        /// </summary>
        /// <param name="parent"></param>
        public virtual void SetParent(UIElementProxy parent)
        {
            this.parent = parent;
            UIElement thisOne = this.target.Unwrap;
            UIElement parentTo = parent.target.Unwrap;
            thisOne.transform.SetParent(parentTo.transform);
            return;
        }

        /// <summary>
        /// Get the parent of this control.
        /// </summary>
        public virtual UIElementProxy GetParent()
        {
            return parent;
        }

        //[MoonSharpVisible(true)]
        //public virtual ControlType GetControlType()
        //{
        //    // ControlType should always be registered.
        //    UnityEngine.Assertions.Assert.IsTrue(UserData.IsTypeRegistered(typeof(ControlType)));

        //    return controlType;
        //}

        ///// <summary>
        ///// level 0 would be the highest priority.
        ///// </summary>
        ///// <param name="level"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetDrawLevel(int level)
        //{
        //    if (level < 0)
        //    {
        //        return;
        //    }

        //    int numDrawLevels = proxyTarget.transform.parent.childCount;

        //    if (level < numDrawLevels)
        //    {
        //        proxyTarget.transform.SetSiblingIndex((numDrawLevels - 1) - level);
        //    }

        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="width"></param>
        ///// <param name="height"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetDimensions(float width, float height)
        //{
        //    proxyTarget.RectTransform.sizeDelta = new Vector2(width, height);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="hidden"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetHidden(bool hidden)
        //{
        //    proxyTarget.gameObject.SetActive(!hidden);
        //    return;
        //}

        //public virtual bool IsHidden()
        //{
        //    return !proxyTarget.gameObject.activeInHierarchy;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="scale"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetScale(float scale)
        //{
        //    proxyTarget.RectTransform.localScale = Vector3.one * scale;
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetScale()
        //{
        //    return proxyTarget.RectTransform.localScale.x;
        //}

        ///// <summary>
        /////
        ///// </summary>
        //[MoonSharpVisible(true)]
        //public virtual void SetWidth(float width)
        //{
        //    proxyTarget.RectTransform.sizeDelta = new Vector2(width, proxyTarget.RectTransform.sizeDelta.y);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        //[MoonSharpVisible(true)]
        //public virtual void SetHeight(float height)
        //{
        //    proxyTarget.RectTransform.sizeDelta = new Vector2(proxyTarget.RectTransform.sizeDelta.x, height);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual int GetNumChildren()
        //{
        //    return proxyTarget.transform.childCount;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        ////public IEnumerable<UIElementProxy> GetChildren()
        ////{

        ////}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="point"></param>
        ///// <param name="anchorTargetControl"></param>
        ///// <param name="relativePoint"></param>
        ///// <param name="offsetX"></param>
        ///// <param name="offsetY"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetAnchor(AnchorPosition point, UIElement anchorTargetControl, AnchorPosition relativePoint, float offsetX, float offsetY)
        //{
        //    Tuple<Vector2, Vector2, Vector2> anchorPositions = GetAnchorPositions(point, relativePoint);

        //    proxyTarget.RectTransform.anchorMin = anchorPositions.Item1;
        //    proxyTarget.RectTransform.anchorMax = anchorPositions.Item2;
        //    proxyTarget.RectTransform.pivot = anchorPositions.Item3;
        //    proxyTarget.RectTransform.anchoredPosition = new Vector2(offsetX, offsetY);

        //    SetParent(anchorTargetControl);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="movable"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetMovable(bool movable)
        //{
        //    dragHandler.enabled = movable;
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual bool IsMovable()
        //{
        //    return dragHandler.enabled;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns>The left coordinate in canvas space.</returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetLeft()
        //{

        //    return proxyTarget.RectTransform.anchoredPosition.x;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetRight()
        //{
        //    return proxyTarget.RectTransform.GetWorldRect().x + GetWidth();
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetTop()
        //{
        //    return proxyTarget.RectTransform.anchoredPosition.y;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetBottom()
        //{
        //    return proxyTarget.RectTransform.anchoredPosition.y + GetHeight();
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetWidth()
        //{
        //    return proxyTarget.RectTransform.sizeDelta.x;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <returns></returns>
        //[MoonSharpVisible(true)]
        //public virtual float GetHeight()
        //{
        //    return proxyTarget.RectTransform.sizeDelta.y;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="handler"></param>
        ///// <param name="function"></param>
        //[MoonSharpVisible(true)]
        //public virtual void SetHandler(string handler, DynValue function)
        //{
        //    if (function == null || function.Type != DataType.Function)
        //    {
        //        return;
        //    }

        //    //DynValue test = AddonManager.Instance.Script.Globals.Get(function);

        //    switch (handler)
        //    {
        //        case "OnEnter":
        //            break;
        //        case "OnMouseUp":
        //            break;
        //        case "OnMouseDown":
        //            break;
        //        case "OnMoveStart":
        //            RegisterCallback(ref onMoveStartCallbacks, function);
        //            break;
        //        case "OnMoveStop":
        //            RegisterCallback(ref onMoveEndCallbacks, function);
        //            break;
        //        default:
        //            break;
        //    }
        //}

        ///// <summary>
        /////
        ///// </summary>
        //[MoonSharpVisible(false)]
        //public virtual void OnDragEnd()
        //{
        //    InvokeCallbacks(onMoveEndCallbacks);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        //[MoonSharpVisible(false)]
        //public virtual void OnDragStart()
        //{
        //    InvokeCallbacks(onMoveStartCallbacks);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="callbacks"></param>
        ///// <param name="args"></param>
        //[MoonSharpVisible(false)]
        //protected virtual void InvokeCallbacks(List<DynValue> callbacks, DynValue[] args = null)
        //{
        //    if (callbacks == null)
        //    {
        //        return;
        //    }
        //    foreach (DynValue callback in callbacks)
        //    {
        //        if (args == null)
        //        {
        //            callback.Function.Call();
        //        }
        //        else
        //        {
        //            callback.Function.Call(args);
        //        }
        //    }
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="callbacks"></param>
        ///// <param name="function"></param>
        //[MoonSharpVisible(false)]
        //protected virtual void RegisterCallback(ref List<DynValue> callbacks, DynValue function)
        //{
        //    if (callbacks == null)
        //    {
        //        callbacks = new List<DynValue>();
        //    }

        //    callbacks.Add(function);
        //    return;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="anchorPosition"></param>
        ///// <returns>A tuple of the form <anchorMin(Vec2), anchorMax(Vec2), pivot(Vec2)></anchorMin></minX></returns>
        //[MoonSharpVisible(false)]
        //protected virtual Tuple<Vector2, Vector2, Vector2> GetAnchorPositions(AnchorPosition point, AnchorPosition relativePoint)
        //{
        //    Vector2 anchorMin = Vector2.zero;
        //    Vector2 anchorMax = Vector2.zero;
        //    Vector2 pivot = Vector2.zero;

        //    switch (relativePoint)
        //    {
        //        case AnchorPosition.TopLeft:
        //            anchorMin = new Vector2(0f, 1f);
        //            anchorMax = new Vector2(0f, 1f);
        //            break;
        //        case AnchorPosition.BottomRight:
        //            anchorMin = new Vector2(1f, 0f);
        //            anchorMax = new Vector2(1f, 0f);
        //            break;
        //        case AnchorPosition.BottomLeft:
        //            anchorMin = new Vector2(0f, 0f);
        //            anchorMax = new Vector2(0f, 0f);
        //            break;
        //        case AnchorPosition.TopRight:
        //            anchorMin = new Vector2(1f, 1f);
        //            anchorMax = new Vector2(1f, 1f);
        //            break;
        //        case AnchorPosition.Top:
        //            anchorMin = new Vector2(0.5f, 1f);
        //            anchorMax = new Vector2(0.5f, 1f);
        //            break;
        //        case AnchorPosition.Center:
        //            anchorMin = new Vector2(0.5f, 0.5f);
        //            anchorMax = new Vector2(0.5f, 0.5f);
        //            break;
        //        case AnchorPosition.Bottom:
        //            anchorMin = new Vector2(0.5f, 0.0f);
        //            anchorMax = new Vector2(0.5f, 0.0f);
        //            break;
        //        case AnchorPosition.Left:
        //            anchorMin = new Vector2(0.0f, 0.5f);
        //            anchorMax = new Vector2(0.0f, 0.5f);
        //            break;
        //        case AnchorPosition.Right:
        //            anchorMin = new Vector2(1f, 0.5f);
        //            anchorMax = new Vector2(1f, 0.5f);
        //            break;
        //        default:
        //            break;
        //    }

        //    switch (point)
        //    {
        //        case AnchorPosition.TopLeft:
        //            pivot = new Vector2(0f, 1f);
        //            break;
        //        case AnchorPosition.BottomRight:
        //            pivot = new Vector2(1f, 0f);
        //            break;
        //        case AnchorPosition.BottomLeft:
        //            pivot = new Vector2(0f, 0f);
        //            break;
        //        case AnchorPosition.TopRight:
        //            pivot = new Vector2(1f, 1f);
        //            break;
        //        case AnchorPosition.Top:
        //            pivot = new Vector2(0.5f, 1f);
        //            break;
        //        case AnchorPosition.Center:
        //            pivot = new Vector2(0.5f, 0.5f);
        //            break;
        //        case AnchorPosition.Bottom:
        //            pivot = new Vector2(0.5f, 0f);
        //            break;
        //        case AnchorPosition.Left:
        //            pivot = new Vector2(0f, 0.5f);
        //            break;
        //        case AnchorPosition.Right:
        //            pivot = new Vector2(1f, 0.5f);
        //            break;
        //        default:
        //            break;
        //    }

        //    return new Tuple<Vector2, Vector2, Vector2>(anchorMin, anchorMax, pivot);
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="path"></param>
        ///// <returns></returns>
        //[MoonSharpVisible(false)]
        //protected virtual Texture2D LoadTextureFromPath(string path)
        //{
        //    Texture2D texture = new Texture2D(2, 2);
        //    string fullpath = Path.Combine(Application.streamingAssetsPath + "/Addons/", path);
        //    try
        //    {
        //        byte[] img = File.ReadAllBytes(fullpath);
        //        texture.LoadImage(img);
        //    }
        //    catch (Exception exception)
        //    {
        //        Debug.LogWarning(exception);
        //    }
        //    return texture;
        //}

    }

    /// <summary>
    ///
    /// </summary>
    public class UIElementTarget : ProxyTarget<UIElement>
    {
        public UIElementTarget() : base()
        {
        }

        public UIElementTarget(UIElement e) : base(e)
        {
        }
    }
}
