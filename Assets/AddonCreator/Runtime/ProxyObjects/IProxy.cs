﻿using MoonSharp.Interpreter;


namespace AddonCreator
{
    public interface IProxy
    {
        IAddonCreatorBehavior Register(Script script);
    }
}
