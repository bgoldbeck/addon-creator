﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System.IO;
using System;


namespace AddonCreator
{
    public enum SliderType
    {
        SliderTypeRadial,
        SliderTypeHorizontal,
        SliderTypeVertical,
    }

    /*
    /// <summary>
    ///
    /// </summary>
    [
        AddonCreatorAttribute(
        "Graphic User Interface", // Category
        "A texture-filled backdrop by a percentage." // Description
    )]
    public class StatusBarElementProxy : UIElementProxy
    {
        private Slider slider = null;
        private RawImage rawImage = null;
        private Image image = null;
        private RectTransform fillRect = null;
        private static Sprite radialSprite = null;

        public StatusBarElementProxy()
        {
        }

        public StatusBarElementProxy(UIElement target, ControlType controlType)
            : base(target, controlType)
        {
            if (radialSprite == null)
            {
                radialSprite = Resources.Load<Sprite>("Sprites/sprite_Circle");
            }
            fillRect = new GameObject("fillRect", typeof(RectTransform)).GetComponent<RectTransform>();
            fillRect.transform.SetParent(target.transform);
            fillRect.localScale = Vector2.one;

            fillRect.sizeDelta = Vector2.zero;


            fillRect.anchorMin = Vector2.zero;
            fillRect.anchorMax = Vector2.zero;
            fillRect.pivot = Vector2.zero;

            fillRect.anchoredPosition = Vector2.zero;

            this.rawImage = fillRect.gameObject.AddComponent<RawImage>();

            this.slider = target.gameObject.AddComponent<Slider>();
            this.slider.transition = Selectable.Transition.None;
            this.slider.interactable = false;
            this.slider.navigation = new Navigation
            {
                mode = Navigation.Mode.None
            };
            this.slider.fillRect = fillRect;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sliderType"></param>
        public void SetSliderType(SliderType sliderType)
        {
            switch (sliderType)
            {
                case SliderType.SliderTypeRadial:
                    if (fillRect.GetComponent<RawImage>() != null)
                    {
                        GameObject.DestroyImmediate(fillRect.GetComponent<RawImage>());
                        rawImage = null;
                    }
                    image = fillRect.gameObject.AddComponent<Image>();
                    image.sprite = radialSprite;
                    image.type = Image.Type.Filled;
                    image.fillMethod = Image.FillMethod.Radial360;
                    image.fillOrigin = (int)(Image.Origin360.Top);
                    image.fillClockwise = true;
                    image.fillAmount = 1.0f;
                    break;
                case SliderType.SliderTypeHorizontal:
                    break;
                case SliderType.SliderTypeVertical:
                    break;
                default:
                    break;
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aMin"></param>
        /// <param name="aMax"></param>
        public void SetMinMax(float min, float max)
        {
            this.slider.minValue = min;
            this.slider.maxValue = max;
            return;
        }


        /// <summary>
        ///
        /// </summary>
        public DynValue GetMinMax()
        {
            return DynValue.NewTuple(
                DynValue.NewNumber(this.slider.minValue),
                DynValue.NewNumber(this.slider.maxValue));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        public void SetValue(float value)
        {
            slider.value = value;
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="direction"></param>
        public void SetDirection(Slider.Direction direction)
        {
            slider.direction = direction;
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public Slider.Direction GetDirection()
        {
            return slider.direction;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public float GetValue()
        {
            return slider.value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public override void SetDimensions(float width, float height)
        {
            proxyTarget.RectTransform.sizeDelta = new Vector2(width, height);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="path"></param>
        public void SetTexture(string path)
        {

            //if (StatusEffect.TryGetIcon(path, out Texture texture))
            //{
            //    rawImage.texture = texture;
            //}
            //else
            //{
            //    rawImage.texture = LoadTextureFromPath(path);
            //}

            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <param name="a"></param>
        public void SetColor(float r, float g, float b, float a)
        {
            rawImage.color = new Color(r, g, b, a);
            return;
        }
    }
*/
}
