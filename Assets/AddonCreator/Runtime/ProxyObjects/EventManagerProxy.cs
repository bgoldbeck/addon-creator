﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public enum GameEventFilterType
    {

    }

    /// <summary>
    ///
    /// </summary>
    ///
    [
    AddonCreatorAttribute(
        "General", // Category
        "Generate event calls.", // Description
        new Type[] { }, // Dependencies
        new Type[] { }, // Register Types
        true // HideInInspector
    )]
    public class EventManagerProxy : MoonsharpProxy<EventManagerProxy, EventManagerTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            EventManagerProxy proxyDelegate(EventManagerTarget r) => new EventManagerProxy(r);
            UserData.RegisterProxyType((Func<EventManagerTarget, EventManagerProxy>)proxyDelegate);
            return;
        }

        public EventManagerProxy() : base(target => { return new EventManagerProxy(); })
        {
        }

        public EventManagerProxy(EventManagerTarget t) : base(target => { return new EventManagerProxy(); })
        {
            target = t;
        }


        /*
        public override IGameObject Register(Script script)
        {
            // Register the proxy, using a function which creates a proxy from the target type.
            UserData.RegisterProxyType<EventManagerProxy, EventManager>(target =>
            {
                return new EventManagerProxy();
            });

            EventManager result = new EventManager(script);

            script.Globals[typeof(EventManager).Name] = result;
            RegisterTypes(script, new Type[] { typeof(EventManager) });
            return result;
        }
        */

        ///// <summary>
        ///// Specific game events can be bound to a callback which will be triggered once the event occurs.
        ///// </summary>
        ///// <param name="addonName"></param>
        ///// <param name="gameEvent"></param>
        ///// <param name="dynValue"></param>
        public void RegisterEvent(string addonName, GameEvent gameEvent, DynValue dynValue)
        {
            //RegisterEvent(addonName, gameEvent, dynValue);
                return;
        }

        ///// <summary>
        /////
        ///// </summary>
        //public void AddFilterForEvent(string addonName, GameEvent gameEvent, GameEventFilterType filterType)
        //{
        //    return;
        //}

        //public new static Type GetTargetType()
        //{
        //    return typeof(EventManager);
        //}
    }
}
