﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;
using System.Runtime.CompilerServices;
using MoonSharp.Interpreter.Interop;


namespace AddonCreator
{
    /// <summary>
    /// By making this a double generic class, I can force TTarget to be a class. I can also
    /// force TProxy to implement a default constructor. This is necessary so I can create a
    /// default instance of the MoonsharpProxy.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MoonsharpProxy<TProxy, TTarget> : ProxyObject, IProxyFactory
        where TTarget : class
        where TProxy : new()
    {
        protected Func<TTarget, TProxy> wrapDelegate;

        protected TTarget target = null;


        //public static void Register()
        //{
            // CANNOT REGISTER HERE, TPROXY MUST BE A REFERENCE (INSTANCE OF IT MUST EXIST)
            //TProxy wrapDelegate(TTarget r) => new TProxy(r);
            //UserData.RegisterProxyType((Func<TTarget, TProxy>)wrapDelegate);
            //return;
        //}

        //public void Register()
        //{
            // CANNOT REGISTER HERE, TPROXY MUST BE A REFERENCE (INSTANCE OF IT MUST EXIST)
            //UserData.RegisterProxyType(wrapDelegate);
            //return;
        //}

        [MoonSharpHidden]
        protected MoonsharpProxy()
        {
        }

        [MoonSharpHidden]
        public MoonsharpProxy(Func<TTarget, TProxy> wrapDelegate)
        {
            this.wrapDelegate = wrapDelegate;
            //AddonCreator.Instance().glo
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="script"></param>
        [MoonSharpHidden]
        protected static IEnumerable<IUserDataDescriptor> RegisterTypes(Script script, IEnumerable<Type> registerTypes)
        {
            List<IUserDataDescriptor> descriptors = new List<IUserDataDescriptor>();

            foreach (Type typeToRegister in registerTypes)
            {
                IUserDataDescriptor descriptor = UserData.RegisterType(typeToRegister);
                descriptors.Add(descriptor);
                script.Globals[typeToRegister.Name] = typeToRegister;
            }

            return descriptors;
        }

        /// <summary>
        /// Takes an instance of a target object and returns a proxy object wrapping it
        /// </summary>
        [MoonSharpHidden]
        public TProxy CreateProxyObject(TTarget target)
        {
            return wrapDelegate(target);
        }

        /// <summary>
        /// Takes an instance of a target object and returns a proxy object wrapping it
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        [MoonSharpHidden]
        public object CreateProxyObject(object o)
        {
            return CreateProxyObject((TTarget)o);
        }

        [MoonSharpHidden]
        public Type TargetType
        {
            get { return typeof(TTarget); }
        }

        [MoonSharpHidden]
        public Type ProxyType
        {
            get { return typeof(TProxy); }
        }

        [MoonSharpHidden]
        public TTarget Target
        {
            get
            {
                return target;
            }
        }

        //public void AssignWrapDelegateValue(Func<TTarget, TProxy> wrapDelegate)
        //{
        //    this.wrapDelegate = wrapDelegate;
        //    return;
        //}
    }


    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="TProxy"></typeparam>
    public abstract class MoonsharpProxy : IProxyFactory
    {
        [MoonSharpHidden]
        protected MoonsharpProxy()
        {
        }

        public Type TargetType
        {
            get
            {
                return GetType();
            }
        }

        public Type ProxyType
        {
            get
            {
                return GetType();
            }
        }

        public object CreateProxyObject(object o)
        {
            return o;
        }
    }
}
