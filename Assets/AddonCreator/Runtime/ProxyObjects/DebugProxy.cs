﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using System;
using MoonSharp.Interpreter.Interop;

namespace AddonCreator
{
    [
      AddonCreatorAttribute(
       "Unity", // Category.
       "", // Description.
        new Type[] { typeof(RayProxy), typeof(Vector3Proxy) }, // Dependency Types
        new Type[] { }, // CLR Types
        true // HideInInspector
    )]
    public class DebugProxy : MoonsharpProxy<DebugProxy, DebugTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            DebugProxy proxyDelegate(DebugTarget r) => new DebugProxy(r);
            UserData.RegisterProxyType((Func<DebugTarget, DebugProxy>)proxyDelegate);
            return;
        }

        public DebugProxy()
        {
        }

        public DebugProxy(DebugTarget t)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public static void Log(string output)
        {
            foreach (IDebugOutput debugOutput in AddonCreator.debugOutputs)
            {
                debugOutput.Log(output);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public static void LogWarning(string output)
        {
            foreach (IDebugOutput debugOutput in AddonCreator.debugOutputs)
            {
                debugOutput.LogWarning(output);
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public static void LogError(string output)
        {
            foreach (IDebugOutput debugOutput in AddonCreator.debugOutputs)
            {
                debugOutput.LogError(output);
            }
            return;
        }
    }

}
