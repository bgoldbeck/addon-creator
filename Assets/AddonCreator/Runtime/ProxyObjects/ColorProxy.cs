﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using UnityEngine;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
     AddonCreatorAttribute(
       "Unity", // Category.
       "", // Description.
       new Type[] { }, // Dependency Types.
       new Type[] { }, // Register Types.
       false // Hide in inspector?
    )]
    public class ColorProxy : MoonsharpProxy<ColorProxy, ColorTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            ColorProxy proxyDelegate(ColorTarget r) => new ColorProxy(r);
            UserData.RegisterProxyType((Func<ColorTarget, ColorProxy>)proxyDelegate);
            return;
        }

        public ColorProxy() // : base(t => { return new ColorProxy(t); })
        {
            this.target = new ColorTarget();
        }

        public ColorProxy(ColorProxy c) // : base(t => { return new ColorProxy(t); })
        {
            this.target = new ColorTarget();
            this.target.Wrap(c.target.Unwrap);
        }

        [MoonSharpVisible(false)]
        public ColorProxy(Color color) // : base(t => { return new ColorProxy(t); })
        {
            this.target = new ColorTarget(color);
        }

        public ColorProxy(float r, float g, float b)//  : base(t => { return new ColorProxy(t); })
        {
            // Constructs a new Color with given r,g,b components and sets a to 1.
            this.target = new ColorTarget(new UnityEngine.Color(r, g, b, 1.0f));
        }

        public ColorProxy(float r, float g, float b, float a) //  : base(t => { return new ColorProxy(t); })
        {
            this.target = new ColorTarget(new UnityEngine.Color(r, g, b, a));
        }


        public ColorProxy(ColorTarget target) // : base(t => { return new ColorProxy(t); })
        {
            this.target = target;
        }

        // Static Properies
        public static ColorProxy yellow { get { return new ColorProxy(UnityEngine.Color.yellow); } }
        public static ColorProxy clear { get { return new ColorProxy(UnityEngine.Color.clear); } }
        public static ColorProxy grey { get { return new ColorProxy(UnityEngine.Color.grey); } }
        public static ColorProxy gray { get { return new ColorProxy(UnityEngine.Color.gray); } }
        public static ColorProxy magenta { get { return new ColorProxy(UnityEngine.Color.magenta); } }
        public static ColorProxy cyan { get { return new ColorProxy(UnityEngine.Color.cyan); } }
        public static ColorProxy red { get { return new ColorProxy(UnityEngine.Color.red); } }
        public static ColorProxy black { get { return new ColorProxy(UnityEngine.Color.black); } }
        public static ColorProxy white { get { return new ColorProxy(UnityEngine.Color.white); } }
        public static ColorProxy blue { get { return new ColorProxy(UnityEngine.Color.blue); } }
        public static ColorProxy green { get { return new ColorProxy(UnityEngine.Color.green); } }

        // Class Properties
        public float a { get { return Unwrap.a; } }
        public float b { get { return Unwrap.b; } }
        public float g { get { return Unwrap.g; } }
        public float r { get { return Unwrap.r; } }
        public ColorProxy gamma { get { return Wrap(Unwrap.gamma); } }
        public ColorProxy linear { get { return Wrap(Unwrap.linear); } }
        public float grayscale { get { return Unwrap.grayscale; } }
        public float maxColorComponent { get { return Unwrap.maxColorComponent; } }

        // Static Methods
        public static ColorProxy HSVToRGB(float H, float S, float V)
        {
            return new ColorProxy(Color.HSVToRGB(H, S, V));
        }

        public static ColorProxy HSVToRGB(float H, float S, float V, bool hdr)
        {
            return new ColorProxy(Color.HSVToRGB(H, S, V, hdr));
        }

        public static ColorProxy Lerp(ColorProxy a, ColorProxy b, float t)
        {
            return new ColorProxy(Color.Lerp(a.Unwrap, b.Unwrap, t));
        }

        public static ColorProxy LerpUnclamped(ColorProxy a, ColorProxy b, float t)
        {
            return new ColorProxy(Color.LerpUnclamped(a.Unwrap, b.Unwrap, t));
        }

        public static DynValue RGBToHSV(ColorProxy rgbColor)
        {
            Color.RGBToHSV(rgbColor.Unwrap, out float H, out float S, out float V);

            return DynValue.NewTuple(DynValue.NewNumber(H), DynValue.NewNumber(S), DynValue.NewNumber(V));
        }

        // Class Methods
        public bool Equals(ColorProxy other)
        {
            if (other == null)
            {
                return false;
            }
            return Unwrap.Equals(other.Unwrap);
        }

        public override bool Equals(object other)
        {
            return Equals(other as ColorProxy);
        }

        public override int GetHashCode()
        {
            return Unwrap.GetHashCode();
        }

        public override string ToString()
        {
            return Unwrap.ToString();
        }

        public string ToString(string format)
        {
            return Unwrap.ToString(format);
        }

        // Operators
        [MoonSharpUserDataMetamethod("__add")]
        public static ColorProxy Addition(ColorProxy a, ColorProxy b)
        {
            return new ColorProxy(a.Unwrap + b.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__sub")]
        public static ColorProxy Subtraction(ColorProxy a, ColorProxy b)
        {
            return new ColorProxy(a.Unwrap - b.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static ColorProxy Multiplication(float b, ColorProxy a)
        {
            return new ColorProxy(b * a.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static ColorProxy Multiplication(ColorProxy a, float b)
        {
            return new ColorProxy(a.Unwrap * b);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static ColorProxy Multiplication(ColorProxy a, ColorProxy b)
        {
            return new ColorProxy(a.Unwrap * b.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__div")]
        public static ColorProxy Division(ColorProxy a, float b)
        {
            return new ColorProxy(a.Unwrap / b);
        }

        [MoonSharpVisible(false)]
        public static bool operator ==(ColorProxy lhs, ColorProxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(ColorProxy lhs, ColorProxy rhs)
        {
            return !(lhs == rhs);
        }

        public float this[int idx]
        {
            get
            {
                return Unwrap[idx];
            }

        }

        public ColorProxy Wrap(UnityEngine.Color color)
        {
            return new ColorProxy(color);
        }

        public UnityEngine.Color Unwrap
        {
            get
            {
                return (UnityEngine.Color)target.Unwrap;
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class ColorTarget : ProxyTarget<UnityEngine.Color>
    {
        public ColorTarget() : base()
        {
        }

        public ColorTarget(UnityEngine.Color color) : base(color)
        {
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
