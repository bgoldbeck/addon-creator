﻿using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using System.Collections.Generic;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    //public static class RandomExtension
    //{
    //    public static float Range(this UnityEngine.Random self, DynValue min, DynValue max)
    //    {
    //        return UnityEngine.Random.Range((float)min.Number, (float)max.Number);
    //    }
    //}

    [
     AddonCreatorAttribute(
       "Unity", // Category.
       "", // Description.
        new Type[] { }, // Dependencies
        new Type[] { typeof(UnityEngine.Random.State)}, // Register Types
       false // HideInInspector
   )]
    public class RandomProxy : MoonsharpProxy<RandomProxy, RandomTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            RandomProxy proxyDelegate(RandomTarget r) => new RandomProxy(r);
            UserData.RegisterProxyType((Func<RandomTarget, RandomProxy>)proxyDelegate);
            return;
        }


        public RandomProxy() : base(t => { return new RandomProxy(t); })
        {
            this.target = new RandomTarget();
        }

        public RandomProxy(RandomTarget target) : base(t => { return new RandomProxy(t); })
        {
            this.target = target;
        }

        /*
        /// <summary>
        ///
        /// </summary>
        /// <param name="script"></param>
        public override IGameObject Register(Script script)
        {
            // The issue here is Random.Range() is static, we cannot extend a static method of a class.
            // We can extend the class methods, but that won't solve our issue.
            // The problem is Random.Range() is overloaded by float and int parameters. Lua only recognizes "Number" values
            // and always interprets the int value.
            // Therefore, we must fully write the proxy class out for Random.

            IGameObject result = null;

            // Register the proxy, using a function which creates a proxy from the target type.
            ProxyUserDataDescriptor proxyDescriptor = (ProxyUserDataDescriptor)UserData.RegisterProxyType<RandomProxy, RandomTarget>(target =>
            {
                return new RandomProxy(target);
            });

            // Register Extension Type, maybe should use this?
            // I think Register Extension could be used to fix the Random.Range issue only using ints.
            // Vector3 I dont think this could work for Vector3. Problem is operator overloading.
            // ProxyDescriptor cant be modified manually.
            // https://github.com/moonsharp-devs/moonsharp/issues/100

            // typeof(UnityEngine.Color) // This needs to be proxied. It has operator overloads.

            script.Globals["Random"] = typeof(RandomProxy);

            RegisterTypes(script, new Type[] { typeof(UnityEngine.Random.State) });
            //UserData.RegisterExtensionType(typeof(RandomExtension));


            return result;
        }
        */

        // Static Properties.
        public static Vector3Proxy insideUnitCircle { get { return new Vector3Proxy(UnityEngine.Random.insideUnitCircle); } }
        public static Vector3Proxy insideUnitSphere { get { return new Vector3Proxy(UnityEngine.Random.insideUnitSphere); } }
        public static Vector3Proxy onUnitSphere { get { return new Vector3Proxy(UnityEngine.Random.onUnitSphere); } }

        public static QuaternionProxy rotation { get { return new QuaternionProxy(UnityEngine.Random.rotation); } }
        public static QuaternionProxy rotationUniform { get { return new QuaternionProxy(UnityEngine.Random.rotationUniform); } }


        public static UnityEngine.Random.State state { get { return UnityEngine.Random.state; } set { UnityEngine.Random.state = value; } }

        public static float value { get { return UnityEngine.Random.value; } }

        // Static Methods.
        public static ColorProxy ColorHSV()
        {
            return new ColorProxy(UnityEngine.Random.ColorHSV());
        }

        public static ColorProxy ColorHSV(float hueMin, float hueMax)
        {
            return new ColorProxy(UnityEngine.Random.ColorHSV(hueMin, hueMax));
        }

        public static ColorProxy ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax)
        {
            return new ColorProxy(UnityEngine.Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax));
        }

        public static ColorProxy ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax)
        {
            return new ColorProxy(UnityEngine.Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, valueMin, valueMax));
        }

        public static ColorProxy ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax, float alphaMin, float alphaMax)
        {
            return new ColorProxy(UnityEngine.Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, valueMin, valueMax, alphaMin, alphaMax));
        }

        public static void InitState(int seed)
        {
            UnityEngine.Random.InitState(seed);
            return;
        }

        public static float Range(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
