﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using MoonSharp.Interpreter;


namespace AddonCreator
{
    /*
    /// <summary>
    ///
    /// </summary>
    [
        AddonCreatorAttribute(
        "Graphic User Interface", // Category
        "A Textured backdrop image." // Description
    )]
    public class BackdropElementProxy : UIElementProxy
    {
        private RawImage rawImage = null;

        public BackdropElementProxy()
        {
        }

        public BackdropElementProxy(UIElement target, ControlType controlType)
            : base(target, controlType)
        {
            this.rawImage = target.gameObject.AddComponent<RawImage>();
        }

        /// <summary>
        /// Set the backdrop color of this instance.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public void SetCenterColor(float r, float g, float b, float a)
        {
            rawImage.color = new Color(r, g, b, a);
            return;
        }

        /// <summary>
        /// Retrieve the color the backdrop.
        /// </summary>
        /// <returns>A tuple consisting of values (r, g, b, a)</returns>
        public DynValue GetCenterColor()
        {
            if (rawImage == null)
            {
                Debug.LogWarning("Image component missing.");
                return null;
            }

            return DynValue.NewTuple(
                DynValue.NewNumber(rawImage.color.r),
                DynValue.NewNumber(rawImage.color.g),
                DynValue.NewNumber(rawImage.color.b),
                DynValue.NewNumber(rawImage.color.a));
        }

        public void SetAlpha(float a)
        {
            rawImage.color = new Color(rawImage.color.r, rawImage.color.g, rawImage.color.b, a);
            return;
        }

        public float GetAlpha()
        {
            return rawImage.color.a;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="filename"></param>
        public void SetCenterTexture(string path)
        {

            //if (StatusEffect.TryGetIcon(path, out Texture texture))
            //{
            //    rawImage.texture = texture;
            //}
            //else
            //{
            //    rawImage.texture = LoadTextureFromPath(path);
            //}

            return;
        }
    }
*/
}
