﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { typeof(Vector2Proxy) }, // Dependency Types
        new Type[] { }, // CLR Types
        false // HideInInspector
    )]
    public class RectProxy : MoonsharpProxy<RectProxy, RectTarget>
    {

        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            RectProxy proxyDelegate(RectTarget r) => new RectProxy(r);
            UserData.RegisterProxyType((Func<RectTarget, RectProxy>)proxyDelegate);
            return;
        }


        public RectProxy()
        {
            this.target = new RectTarget(new Rect());
        }

        public RectProxy(RectTarget t)
        {
            this.target = t;
        }

        public RectProxy(float x, float y, float width, float height)
        {
            this.target = new RectTarget(new Rect(x, y, width, height));
        }

        public RectProxy(Vector2Proxy position, Vector2Proxy size)
        {
            this.target = new RectTarget(new Rect(position.Target.Unwrap, size.Target.Unwrap));
        }

        [MoonSharpHidden]
        public RectProxy(Rect rect)
        {
            this.target = new RectTarget(rect);
        }

        // Static Properties.
        public static RectProxy zero { get { return new RectProxy(0f, 0f, 0f, 0f); } }

        // Class Properties.
        public Vector2Proxy center { get { return new Vector2Proxy(target.Unwrap.center); } }
        public float height { get { return target.Unwrap.height; } }
        public Vector2Proxy max { get { return new Vector2Proxy(target.Unwrap.max); } }
        public Vector2Proxy min { get { return new Vector2Proxy(target.Unwrap.min); } }
        public Vector2Proxy position { get { return new Vector2Proxy(target.Unwrap.position); } }
        public Vector2Proxy size { get { return new Vector2Proxy(target.Unwrap.size); } }
        public float width { get { return target.Unwrap.width; } }
        public float x { get { return target.Unwrap.x; } }
        public float xMax { get { return target.Unwrap.xMax; } }
        public float xMin { get { return target.Unwrap.xMin; } }
        public float y { get { return target.Unwrap.y; } }
        public float yMax { get { return target.Unwrap.yMax; } }
        public float yMin { get { return target.Unwrap.yMin; } }

        // Public Methods.
        public bool Contains(Vector2Proxy point)
        {
            return target.Unwrap.Contains(point.Target.Unwrap);
        }

        public bool Contains(Vector3Proxy point)
        {
            return target.Unwrap.Contains(point.Target.Unwrap);

        }

        public bool Contains(Vector3Proxy point, bool allowInverse)
        {
            return target.Unwrap.Contains(point.Target.Unwrap, allowInverse);
        }

        public bool Overlaps(RectProxy other)
        {
            return target.Unwrap.Overlaps(other.target.Unwrap);
        }

        public bool Overlaps(RectProxy other, bool allowInverse)
        {
            return target.Unwrap.Overlaps(other.target.Unwrap, allowInverse);
        }

        public void Set(float x, float y, float width, float height)
        {
            this.target = new RectTarget(new Rect(x, y, width, height));
            return;
        }

        public override string ToString()
        {
            return target.Unwrap.ToString();
        }

        public string ToString(string format)
        {
            return target.Unwrap.ToString(format);
        }

        public bool Equals(RectProxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        public override bool Equals(object other)
        {
            return Equals(other as RectProxy);
        }

        public override int GetHashCode()
        {
            return target.Unwrap.GetHashCode();
        }

        // Static Methods.
        public static RectProxy MinMaxRect(float xmin, float ymin, float xmax, float ymax)
        {
            return new RectProxy(Rect.MinMaxRect(xmin, ymin, xmax, ymax));
        }

        public static Vector2Proxy NormalizedToPoint(RectProxy rectangle, Vector2Proxy noramlizedRectCoordinates)
        {
            return new Vector2Proxy(Rect.NormalizedToPoint(rectangle.target.Unwrap, noramlizedRectCoordinates.Target.Unwrap));
        }

        public static Vector2Proxy PointToNormalized(RectProxy rectangle, Vector2Proxy point)
        {
            return new Vector2Proxy(Rect.PointToNormalized(rectangle.target.Unwrap, point.Target.Unwrap));
        }

        // Operators
        [MoonSharpVisible(false)]
        public static bool operator ==(RectProxy lhs, RectProxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(RectProxy lhs, RectProxy rhs)
        {
            return !(lhs == rhs);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(RectProxy p1, RectProxy p2)
        {
            return p1 == p2;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class RectTarget : ProxyTarget<UnityEngine.Rect>
    {
        public RectTarget() : base()
        {
        }

        public RectTarget(UnityEngine.Rect p) : base(p)
        {
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
