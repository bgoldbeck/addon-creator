﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using UnityEngine;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
     AddonCreatorAttribute(
       "Unity", // Category.
       "", // Description.
        new Type[] { typeof(RayProxy), typeof(Vector3Proxy) }, // Dependency Types
        new Type[] { }, // CLR Types
        false // HideInInspector
    )]
    public class PlaneProxy : MoonsharpProxy<PlaneProxy, PlaneTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            PlaneProxy proxyDelegate(PlaneTarget r) => new PlaneProxy();
            UserData.RegisterProxyType((Func<PlaneTarget, PlaneProxy>)proxyDelegate);
            return;
        }

        public PlaneProxy()
        {
            this.target = new PlaneTarget();
        }

        public PlaneProxy(Vector3Proxy inNormal, Vector3Proxy inPoint)
        {
            this.target = new PlaneTarget(new Plane(inNormal.Target.Unwrap, inPoint.Target.Unwrap));
        }

        public PlaneProxy(Vector3Proxy inNormal, float d)
        {
            this.target = new PlaneTarget(new Plane(inNormal.Target.Unwrap, d));
        }

        public PlaneProxy(Vector3Proxy a, Vector3Proxy b, Vector3Proxy c)
        {
            this.target = new PlaneTarget(new Plane(a.Target.Unwrap, b.Target.Unwrap, c.Target.Unwrap));
        }

        [MoonSharpVisible(false)]
        public PlaneProxy(Plane plane)
        {
            this.target = new PlaneTarget(plane);
        }

        [MoonSharpVisible(false)]
        public PlaneProxy(PlaneTarget target)
        {
            this.target = target;
        }


        // Class Properties
        public Vector3Proxy normal { get { return new Vector3Proxy(target.Unwrap.normal); } }

        public float distance { get { return target.Unwrap.distance; } }

        public PlaneProxy flipped { get { return new PlaneProxy(target.Unwrap.flipped); } }

        // Static Methods
        public static PlaneProxy Translate(PlaneProxy plane, Vector3Proxy translation)
        {
            return new PlaneProxy(Plane.Translate(plane.target.Unwrap, translation.Target.Unwrap));
        }

        // Class Methods
        public Vector3Proxy ClosestPointOnPlane(Vector3Proxy point) { return new Vector3Proxy(target.Unwrap.ClosestPointOnPlane(point.Target.Unwrap)); }

        public void Flip()
        {
            Plane p = target.Unwrap;
            p.Flip();
            target = new PlaneTarget(p);
            return;
        }

        public float GetDistanceToPoint(Vector3Proxy point) { return target.Unwrap.GetDistanceToPoint(point.Target.Unwrap); }

        public bool GetSide(Vector3Proxy point) { return target.Unwrap.GetSide(point.Target.Unwrap); }

        public DynValue Raycast(RayProxy ray, float enter)
        {
            bool result = target.Unwrap.Raycast(ray.Target.Unwrap, out enter);
            return DynValue.NewTuple(DynValue.NewBoolean(result), DynValue.NewNumber(enter));
        }

        public bool SameSide(Vector3Proxy inPt0, Vector3Proxy inPt1)
        {
            return Unwrap.SameSide(inPt0.Target.Unwrap, inPt1.Target.Unwrap);
        }

        public void Set3Points(Vector3Proxy a, Vector3Proxy b, Vector3Proxy c)
        {
            Plane temp = new Plane(a.Target.Unwrap, b.Target.Unwrap, c.Target.Unwrap);

            target = new PlaneTarget(temp);
            return;
        }

        public void SetNormalAndPosition(Vector3Proxy inNormal, Vector3Proxy inPoint)
        {
            Plane temp = new Plane(inNormal.Target.Unwrap, inPoint.Target.Unwrap);

            target = new PlaneTarget(temp);
            return;
        }

        public override string ToString() { return Unwrap.ToString(); }
        public string ToString(string format) { return Unwrap.ToString(format); }

        public void Translate(Vector3Proxy translation)
        {
            Plane temp = Unwrap;

            temp.Translate(translation.Target.Unwrap);

            target = new PlaneTarget(temp);
            return;
        }

        public bool Equals(PlaneProxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        public override bool Equals(object other)
        {
            return Equals(other as PlaneProxy);
        }

        public override int GetHashCode()
        {
            return Unwrap.GetHashCode();
        }

        // Operators

        [MoonSharpVisible(false)]
        public static bool operator ==(PlaneProxy lhs, PlaneProxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(PlaneProxy lhs, PlaneProxy rhs)
        {
            return !(lhs == rhs);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(PlaneProxy p1, PlaneProxy p2)
        {
            return p1 == p2;
        }

        public PlaneProxy Wrap(UnityEngine.Plane plane)
        {
            return new PlaneProxy(plane);
        }

        public UnityEngine.Plane Unwrap
        {
            get
            {
                return target.Unwrap;
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class PlaneTarget : ProxyTarget<UnityEngine.Plane>
    {
        public PlaneTarget() : base()
        {
        }

        public PlaneTarget(UnityEngine.Plane p) : base(p)
        {
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
