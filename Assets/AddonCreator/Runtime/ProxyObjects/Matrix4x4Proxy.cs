﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { typeof(QuaternionProxy), typeof(Vector3Proxy), typeof(Vector3Proxy) }, // Dependency Types
        new Type[] { typeof(FrustumPlanes) }, // Register Typess
        false // Hide in inspector?
    )]
    public class Matrix4x4Proxy : MoonsharpProxy<Matrix4x4Proxy, Matrix4x4Target>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            Matrix4x4Proxy proxyDelegate(Matrix4x4Target r) => new Matrix4x4Proxy();
            UserData.RegisterProxyType((Func<Matrix4x4Target, Matrix4x4Proxy>)proxyDelegate);
            return;
        }

        public float m00 { get { return target.Unwrap.m00; } }
        public float m33 { get { return target.Unwrap.m33; } }
        public float m23 { get { return target.Unwrap.m23; } }
        public float m13 { get { return target.Unwrap.m13; } }
        public float m03 { get { return target.Unwrap.m03; } }
        public float m32 { get { return target.Unwrap.m32; } }
        public float m22 { get { return target.Unwrap.m22; } }
        public float m02 { get { return target.Unwrap.m02; } }
        public float m12 { get { return target.Unwrap.m12; } }
        public float m21 { get { return target.Unwrap.m21; } }
        public float m11 { get { return target.Unwrap.m11; } }
        public float m01 { get { return target.Unwrap.m01; } }
        public float m30 { get { return target.Unwrap.m30; } }
        public float m20 { get { return target.Unwrap.m20; } }
        public float m10 { get { return target.Unwrap.m10; } }
        public float m31 { get { return target.Unwrap.m31; } }

        public Matrix4x4Proxy()
        {
            target = new Matrix4x4Target();
        }

        public Matrix4x4Proxy(Matrix4x4 m)
        {
            target = new Matrix4x4Target(m);
        }

        public Matrix4x4Proxy(Vector4Proxy column1, Vector4Proxy column2, Vector4Proxy column3, Vector4Proxy column4)
        {
            Matrix4x4 m = new Matrix4x4(column1.Target.Unwrap, column2.Target.Unwrap, column3.Target.Unwrap, column4.Target.Unwrap);
            target = new Matrix4x4Target(m);
        }

        public float this[int index]
        {
            get { return target.Unwrap[index]; }
            set
            {
                Matrix4x4 r = target.Unwrap;
                r[index] = value;
                target.Wrap(r);
                return;
            }

        }

        public float this[int row, int column]
        {
            get { return target.Unwrap[row + column * 4]; }
            set
            {
                Matrix4x4 r = target.Unwrap;
                r[row + column * 4] = value;
                target.Wrap(r);
                return;
            }
        }

        // Static Properties
        public static Matrix4x4Proxy zero { get { return new Matrix4x4Proxy(Matrix4x4.zero); } }
        public static Matrix4x4Proxy identity { get { return new Matrix4x4Proxy(Matrix4x4.identity); } }

        // Class Properties.
        public QuaternionProxy rotation { get { return new QuaternionProxy(target.Unwrap.rotation); } }
        public Vector3Proxy lossyScale { get { return new Vector3Proxy(target.Unwrap.lossyScale); } }
        public bool isIdentity { get { return target.Unwrap.isIdentity; } }
        public float determinant { get { return target.Unwrap.determinant; } }
        public Matrix4x4Proxy transpose { get { return new Matrix4x4Proxy(target.Unwrap.transpose); } }

        public FrustumPlanes decomposeProjection { get { return target.Unwrap.decomposeProjection; } }
        public Matrix4x4Proxy inverse { get { return new Matrix4x4Proxy(target.Unwrap.inverse); } }

        // Public Static Methods
        public static float Determinant(Matrix4x4Proxy m) { return Matrix4x4.Determinant(m.target.Unwrap); }

        public static Matrix4x4Proxy Frustum(float left, float right, float bottom, float top, float zNear, float zFar)
        {
            return new Matrix4x4Proxy(Matrix4x4.Frustum(left, right, bottom, top, zNear, zFar));
        }

        public static Matrix4x4Proxy Frustum(FrustumPlanes fp) { return new Matrix4x4Proxy(Matrix4x4.Frustum(fp)); }

        public static Matrix4x4Proxy Inverse(Matrix4x4Proxy m) { return new Matrix4x4Proxy(Matrix4x4.Inverse(m.target.Unwrap)); }

        public static DynValue Inverse3DAffine(Matrix4x4Proxy input)
        {
            Matrix4x4 result = Matrix4x4.identity;
            bool b = Matrix4x4.Inverse3DAffine(input.target.Unwrap, ref result);

            return DynValue.NewTuple(DynValue.NewBoolean(b), UserData.Create(new Matrix4x4Proxy(result)));
        }

        public static Matrix4x4Proxy LookAt(Vector3Proxy from, Vector3Proxy to, Vector3Proxy up)
        {
            return new Matrix4x4Proxy(Matrix4x4.LookAt(from.Target.Unwrap, to.Target.Unwrap, up.Target.Unwrap));
        }

        public static Matrix4x4Proxy Ortho(float left, float right, float bottom, float top, float zNear, float zFar)
        {
            return new Matrix4x4Proxy(Matrix4x4.Ortho(left, right, bottom, top, zNear, zFar));
        }

        public static Matrix4x4Proxy Perspective(float fov, float aspect, float zNear, float zFar)
        {
            return new Matrix4x4Proxy(Matrix4x4.Perspective(fov, aspect, zNear, zFar));
        }

        public static Matrix4x4Proxy Rotate(QuaternionProxy q)
        {
            return new Matrix4x4Proxy(Matrix4x4.Rotate(q.Target.Unwrap));
        }

        public static Matrix4x4Proxy Scale(Vector3Proxy vector)
        {
            return new Matrix4x4Proxy(Matrix4x4.Scale(vector.Target.Unwrap));
        }

        public static Matrix4x4Proxy Translate(Vector3Proxy vector)
        {
            return new Matrix4x4Proxy(Matrix4x4.Translate(vector.Target.Unwrap));
        }

        public static Matrix4x4Proxy Transpose(Matrix4x4Proxy m)
        {
            return new Matrix4x4Proxy(Matrix4x4.Transpose(m.Target.Unwrap));
        }

        public static Matrix4x4Proxy TRS(Vector3Proxy pos, QuaternionProxy q, Vector3Proxy s)
        {
            return new Matrix4x4Proxy(Matrix4x4.TRS(pos.Target.Unwrap, q.Target.Unwrap, s.Target.Unwrap));
        }

        // Public Methods
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Matrix4x4Proxy);
        }

        public bool Equals(Matrix4x4Proxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        public Vector4Proxy GetColumn(int index)
        {
            return new Vector4Proxy(target.Unwrap.GetColumn(index));
        }

        public override int GetHashCode()
        {
            return target.Unwrap.GetHashCode();
        }

        public Vector4Proxy GetRow(int index)
        {
            return new Vector4Proxy(target.Unwrap.GetRow(index));
        }

        public Vector3Proxy MultiplyPoint(Vector3Proxy point)
        {
            return new Vector3Proxy(target.Unwrap.MultiplyPoint(point.Target.Unwrap));
        }

        public Vector3Proxy MultiplyPoint3x4(Vector3Proxy point)
        {
            return new Vector3Proxy(target.Unwrap.MultiplyPoint3x4(point.Target.Unwrap));
        }

        public Vector3Proxy MultiplyVector(Vector3Proxy vector)
        {
            return new Vector3Proxy(target.Unwrap.MultiplyVector(vector.Target.Unwrap));
        }

        public void SetColumn(int index, Vector4Proxy column)
        {
            Matrix4x4 m = target.Unwrap;
            m.SetColumn(index, column.Target.Unwrap);
            target.Wrap(m);
            return;
        }

        public void SetRow(int index, Vector4Proxy row)
        {
            Matrix4x4 m = target.Unwrap;
            m.SetRow(index, row.Target.Unwrap);
            target.Wrap(m);
            return;
        }

        public void SetTRS(Vector3Proxy pos, QuaternionProxy q, Vector3Proxy s)
        {
            Matrix4x4 m = target.Unwrap;
            m.SetTRS(pos.Target.Unwrap, q.Target.Unwrap, s.Target.Unwrap);
            target.Wrap(m);
            return;
        }


        public override string ToString() { return target.Unwrap.ToString(); }

        public string ToString(string format) { return target.Unwrap.ToString(format); }

        public PlaneProxy TransformPlane(PlaneProxy plane)
        {
            return new PlaneProxy(target.Unwrap.TransformPlane(plane.Target.Unwrap));
        }

        public bool ValidTRS() { return target.Unwrap.ValidTRS(); }

        // Operator overloads and metamethods.
        [MoonSharpVisible(false)]
        public static bool operator ==(Matrix4x4Proxy lhs, Matrix4x4Proxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(Matrix4x4Proxy lhs, Matrix4x4Proxy rhs)
        {
            return !(lhs == rhs);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector4Proxy Multiplication1(Matrix4x4Proxy lhs, Vector4Proxy vector)
        {
            return new Vector4Proxy(lhs.target.Unwrap * vector.Target.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Matrix4x4Proxy Multiplication1(Matrix4x4Proxy lhs, Matrix4x4Proxy rhs)
        {
            return new Matrix4x4Proxy(lhs.target.Unwrap * rhs.Target.Unwrap);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(Matrix4x4Proxy p1, Matrix4x4Proxy p2)
        {
            return p1 == p2;
        }

    }

    public class Matrix4x4Target : ProxyTarget<UnityEngine.Matrix4x4>
    {
        public Matrix4x4Target() : base(Matrix4x4.identity)
        {
        }

        public Matrix4x4Target(UnityEngine.Matrix4x4 m) : base(m)
        {
        }

    }
}


#pragma warning restore IDE1006 // Naming Styles
