﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { }, // Dependencies.
        new Type[] { }, // Register Types.
        false // HideInInspector
    )]
    public class Vector4Proxy : MoonsharpProxy<Vector4Proxy, Vector4Target>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            Vector4Proxy proxyDelegate(Vector4Target r) => new Vector4Proxy(r);
            UserData.RegisterProxyType((Func<Vector4Target, Vector4Proxy>)proxyDelegate);
            return;
        }

        public const float kEpsilon = 1E-05F;

        public Vector4Proxy()
        {
            this.target = new Vector4Target();
        }

        public Vector4Proxy(float x, float y)
        {
            this.target = new Vector4Target(new Vector4(x, y, 0f));
        }

        public Vector4Proxy(float x, float y, float z)
        {
            this.target = new Vector4Target(new Vector4(x, y, z));
        }

        public Vector4Proxy(float x, float y, float z, float w)
        {
            this.target = new Vector4Target(new Vector4(x, y, z, w));
        }

        public Vector4Proxy(Vector4Proxy proxy)
        {
            this.target = new Vector4Target(proxy.Target.Unwrap);
        }

        [MoonSharpVisible(false)]
        public Vector4Proxy(Vector4Target target)
        {
            this.target = target;
        }

        [MoonSharpVisible(false)]
        public Vector4Proxy(Vector4 v)
        {
            this.target = new Vector4Target(v);
        }

        public float this[int index]
        {
            get
            {
                return target.Unwrap[index];
            }
        }

        // Properties
        public static Vector4Proxy one { get { return new Vector4Proxy(Vector4.one); } }

        public static Vector4Proxy zero { get { return new Vector4Proxy(Vector4.zero); } }

        public static Vector4Proxy positiveInfinity { get { return new Vector4Proxy(Vector4.positiveInfinity); } }

        public static Vector4Proxy negativeInfinity { get { return new Vector4Proxy(Vector4.negativeInfinity); } }

        // Class properties
        public Vector4Proxy normalized { get { return new Vector4Proxy(target.Unwrap.normalized); } }

        public float magnitude { get { return target.Unwrap.magnitude; } }

        public float sqrMagnitude { get { return target.Unwrap.sqrMagnitude; } }

        // Static methods
        public static float Distance(Vector4Proxy a, Vector4Proxy b) { return Vector4.Distance(a.target.Unwrap, b.target.Unwrap); }

        public static float Dot(Vector4Proxy a, Vector4Proxy b) { return Vector4.Dot(a.target.Unwrap, b.target.Unwrap); }

        public static Vector4Proxy Lerp(Vector4Proxy a, Vector4Proxy b, float t) { return new Vector4Proxy(Vector4.Lerp(a.target.Unwrap, b.target.Unwrap, t)); }

        public static Vector4Proxy LerpUnclamped(Vector4Proxy a, Vector4Proxy b, float t) { return new Vector4Proxy(Vector4.LerpUnclamped(a.target.Unwrap, b.target.Unwrap, t)); }

        public static float Magnitude(Vector4Proxy a) { return a.target.Unwrap.magnitude; }

        public static Vector4Proxy Max(Vector4Proxy lhs, Vector4Proxy rhs) { return new Vector4Proxy(Vector4.Max(lhs.target.Unwrap, rhs.target.Unwrap)); }

        public static Vector4Proxy Min(Vector4Proxy lhs, Vector4Proxy rhs) { return new Vector4Proxy(Vector4.Min(lhs.target.Unwrap, rhs.target.Unwrap)); }

        public static Vector4Proxy MoveTowards(Vector4Proxy current, Vector4Proxy target, float maxDistanceDelta)
        {
            return new Vector4Proxy(Vector4.MoveTowards(current.target.Unwrap, target.target.Unwrap, maxDistanceDelta));
        }

        public static Vector4Proxy Normalize(Vector4Proxy a) { return new Vector4Proxy(Vector4.Normalize(a.target.Unwrap)); }

        public static Vector4Proxy Project(Vector4Proxy a, Vector4Proxy b) { return new Vector4Proxy(Vector4.Project(a.target.Unwrap, b.target.Unwrap)); }

        public static Vector4Proxy Scale(Vector4Proxy a, Vector4Proxy b) { return new Vector4Proxy(Vector4.Scale(a.target.Unwrap, b.target.Unwrap)); }

        public static float SqrMagnitude(Vector4Proxy a) { return Vector4.SqrMagnitude(a.target.Unwrap); }

        // Class Methods
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Vector4Proxy);
        }

        public bool Equals(Vector4Proxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        public override int GetHashCode() { return target.Unwrap.GetHashCode(); }

        public void Normalize()
        {
            target = new Vector4Target(Vector4.Normalize(target.Unwrap));
        }

        public void Scale(Vector4Proxy scale)
        {
            target = new Vector4Target(Vector4.Scale(target.Unwrap, scale.target.Unwrap));
            return;
        }

        public void Set(float newX, float newY, float newZ, float newW)
        {
            target = new Vector4Target(new Vector4(newX, newY, newZ, newW));
        }

        public float SqrMagnitude()
        {
            return target.Unwrap.SqrMagnitude();
        }

        public override string ToString()
        {
            return target.Unwrap.ToString();
        }

        public string ToString(string format)
        {
            return target.Unwrap.ToString(format);
        }

        [MoonSharpVisible(false)]
        public static bool operator ==(Vector4Proxy lhs, Vector4Proxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(Vector4Proxy lhs, Vector4Proxy rhs)
        {
            return !(lhs == rhs);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(Vector4Proxy p1, Vector4Proxy p2)
        {
            return p1 == p2;
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector4Proxy Multiplication1(Vector4Proxy o, float p)
        {
            return new Vector4Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector4Proxy Multiplication2(float p, Vector4Proxy o)
        {
            return new Vector4Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__div")]
        public static Vector4Proxy Division(Vector4Proxy o, float p)
        {
            return new Vector4Proxy(o.Target.Unwrap / p);
        }

        [MoonSharpUserDataMetamethod("__add")]
        public static Vector4Proxy Addition(Vector4Proxy p1, Vector4Proxy p2)
        {
            return new Vector4Proxy(p1.Target.Unwrap + p2.Target.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__sub")]
        public static Vector4Proxy Subtraction(Vector4Proxy p1, Vector4Proxy p2)
        {
            return new Vector4Proxy(p1.Target.Unwrap - p2.Target.Unwrap);
        }

        // Unary Minus Operator
        [MoonSharpUserDataMetamethod("__unm")]
        public static Vector4Proxy UnaryMinus(Vector4Proxy p1)
        {
            return new Vector4Proxy(-p1.Target.Unwrap);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class Vector4Target : ProxyTarget<UnityEngine.Vector4>
    {
        public Vector4Target() : base()
        {
        }

        public Vector4Target(UnityEngine.Vector4 v) : base(v)
        {
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
