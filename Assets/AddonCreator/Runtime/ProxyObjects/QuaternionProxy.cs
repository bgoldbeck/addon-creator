﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { }, // Dependencies
        new Type[] { }, // Register Types
        false // HideInInspector
    )]
    public class QuaternionProxy : MoonsharpProxy<QuaternionProxy, QuaternionTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            QuaternionProxy proxyDelegate(QuaternionTarget r) => new QuaternionProxy(r);
            UserData.RegisterProxyType((Func<QuaternionTarget, QuaternionProxy>)proxyDelegate);
            return;
        }

        public const float kEpsilon = 1E-06F;

        public QuaternionProxy()// : base(t => { return new QuaternionProxy(t); })
        {
            this.target = new QuaternionTarget(Quaternion.identity);
        }

        public QuaternionProxy(Quaternion quaternion)// : base(t => { return new QuaternionProxy(t); })
        {
            this.target = new QuaternionTarget(quaternion);
        }

        public QuaternionProxy(QuaternionTarget quaternion)// : base(t => { return new QuaternionProxy(t); })
        {
            this.target = quaternion;
        }

        /*
        /// <summary>
        ///
        /// </summary>
        /// <param name="script"></param>
        public override IGameObject Register(Script script)
        {
            IGameObject result = null;

            // Register the proxy, using a function which creates a proxy from the target type.
            UserData.RegisterProxyType<QuaternionProxy, QuaternionTarget>(target =>
            {
                return new QuaternionProxy(target);
            });

            script.Globals["Quaternion"] = typeof(QuaternionProxy);

            return result;
        }
        */

        // Public Properties.
        public static QuaternionProxy identity
        {
            get { return new QuaternionProxy(Quaternion.identity); }
        }

        // Class Properties.
        public Vector3Proxy eulerAngles
        {
            get { return new Vector3Proxy(Value.eulerAngles); }
        }

        public QuaternionProxy normalized
        {
            get { return new QuaternionProxy(Value.normalized); }
        }

        public float w
        {
            get { return Value.w; }
        }

        public float x
        {
            get { return Value.x; }
        }
        public float y
        {
            get { return Value.y; }
        }
        public float z
        {
            get { return Value.z; }
        }

        // Class Methods
        public void Set(float x, float y, float z, float w)
        {
            target = new QuaternionTarget(new Quaternion(x, y, z, w));
            return;
        }

        public void SetFromToRotation(Vector3Proxy fromDirection, Vector3Proxy toDirection)
        {
            Quaternion temp = Quaternion.identity;
            temp.SetFromToRotation(fromDirection.Target.Unwrap, toDirection.Target.Unwrap);
            target.Wrap(temp);
            return;
        }

        public void SetLookRotation(Vector3Proxy view, Vector3Proxy up)
        {
            if (up == null)
            {
                up = new Vector3Proxy(Vector3.up);
            }

            Quaternion temp = Quaternion.identity;
            temp.SetLookRotation(view.Target.Unwrap, up.Target.Unwrap);
            target.Wrap(temp);
            return;
        }

        public DynValue ToAngleAxis()
        {
            Value.ToAngleAxis(out float angle, out Vector3 vAxis);

            return DynValue.NewTuple(new DynValue[] { DynValue.NewNumber(angle), UserData.Create(new Vector3Proxy(vAxis)) });
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        // Static Methods
        // Returns the angle in degrees between two rotations a and b.
        public static float Angle(QuaternionProxy a, QuaternionProxy b)
        {
            return Quaternion.Angle(a.Value, b.Value);
        }

        // Creates a rotation which rotates angle degrees around axis.
        public static QuaternionProxy AngleAxis(float angle, Vector3Proxy axis)
        {
            return new QuaternionProxy(Quaternion.AngleAxis(angle, axis.Target.Unwrap));
        }

        // The dot product between two rotations.
        public static float Dot(QuaternionProxy a, QuaternionProxy b)
        {
            return Quaternion.Dot(a.Value, b.Value);
        }

        // Returns a rotation that rotates z degrees around the z axis, x degrees around the x axis, and y degrees around the y axis; applied in that order.
        public static QuaternionProxy Euler(float x, float y, float z)
        {
            return new QuaternionProxy(Quaternion.Euler(x, y, z));
        }

        // Creates a rotation which rotates from fromDirection to toDirection.
        public static QuaternionProxy FromToRotation(Vector3Proxy fromDirection, Vector3Proxy toDirection)
        {
            return new QuaternionProxy(Quaternion.FromToRotation(fromDirection.Target.Unwrap, toDirection.Target.Unwrap));
        }

        // Returns the Inverse of rotation.
        public static QuaternionProxy Inverse(QuaternionProxy rotation)
        {
            return new QuaternionProxy(Quaternion.Inverse(rotation.Value));
        }

        // Interpolates between a and b by t and normalizes the result afterwards.The parameter t is clamped to the range [0, 1].
        public static QuaternionProxy Lerp(QuaternionProxy a, QuaternionProxy b, float t)
        {
            return new QuaternionProxy(Quaternion.Lerp(a.Value, b.Value, t));
        }

        // Interpolates between a and b by t and normalizes the result afterwards.The parameter t is not clamped.
        public static QuaternionProxy LerpUnclamped(QuaternionProxy a, QuaternionProxy b, float t)
        {
            return new QuaternionProxy(Quaternion.LerpUnclamped(a.Value, b.Value, t));
        }

        // Creates a rotation with the specified forward and upwards directions.
        public static QuaternionProxy LookRotation(Vector3Proxy forward, Vector3Proxy upwards)
        {
            return new QuaternionProxy(Quaternion.LookRotation(forward.Target.Unwrap, upwards.Target.Unwrap));
        }

        // Converts this quaternion to one with the same orientation but with a magnitude of 1.
        public static QuaternionProxy Normalize(QuaternionProxy q)
        {
            return new QuaternionProxy(Quaternion.Normalize(q.Value));
        }

        // Rotates a rotation from towards to.
        public static QuaternionProxy RotateTowards(QuaternionProxy from, QuaternionProxy to, float maxDegreesDelta)
        {
            return new QuaternionProxy(Quaternion.RotateTowards(from.Value, to.Value, maxDegreesDelta));
        }

        // Spherically interpolates between quaternions a and b by ratio t.The parameter t is clamped to the range [0, 1].
        public static QuaternionProxy Slerp(QuaternionProxy a, QuaternionProxy b, float t)
        {
            return new QuaternionProxy(Quaternion.Slerp(a.Value, b.Value, t));
        }

        // Spherically interpolates between a and b by t.The parameter t is not clamped.
        public static QuaternionProxy SlerpUnclamped(QuaternionProxy a, QuaternionProxy b, float t)
        {
            return new QuaternionProxy(Quaternion.Slerp(a.Value, b.Value, t));
        }

        // Metamethods
        public float this[int idx]
        {
            get
            {
                float result = 0f;
                if (idx == 0)
                {
                    result = Value.x;
                }
                else if (idx == 1)
                {

                }
                else if (idx == 2)
                {
                    result = Value.z;
                }
                else if (idx == 3)
                {
                    result = Value.w;
                }
                return result;
            }

        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as QuaternionProxy);
        }

        public bool Equals(QuaternionProxy proxy)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(proxy, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, proxy))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != proxy.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return proxy.Value.Equals(Value);
        }

        [MoonSharpVisible(false)]
        public static bool operator ==(QuaternionProxy lhs, QuaternionProxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(QuaternionProxy lhs, QuaternionProxy rhs)
        {
            return !(lhs == rhs);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static QuaternionProxy Multiplication(QuaternionProxy lhs, QuaternionProxy rhs)
        {
            return new QuaternionProxy(lhs.Value * rhs.Value);
        }

        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(QuaternionProxy lhs, QuaternionProxy rhs)
        {
            return lhs == rhs;
        }

        public Quaternion Value
        {
            get
            {
                return target.Unwrap;
            }
        }
    }

    public class QuaternionTarget : ProxyTarget<UnityEngine.Quaternion>
    {
        public QuaternionTarget() : base(Quaternion.identity)
        {
        }

        public QuaternionTarget(Quaternion q) : base(q)
        {
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
