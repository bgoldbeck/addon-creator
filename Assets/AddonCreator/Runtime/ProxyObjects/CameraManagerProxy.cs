﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        // Category.
        "",
        // Description.
        "The main scene camera."
    )]
    public class CameraManagerProxy : MoonsharpProxy<CameraManagerProxy, CameraManagerTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        [MoonSharpHidden]
        public static void Register()
        {
            CameraManagerProxy proxyDelegate(CameraManagerTarget r) => new CameraManagerProxy(r);
            UserData.RegisterProxyType((Func<CameraManagerTarget, CameraManagerProxy>)proxyDelegate);
            return;
        }

        [MoonSharpHidden]
        public CameraManagerProxy() : base(target => { return new CameraManagerProxy(); })
        {
        }

        [MoonSharpHidden]
        public CameraManagerProxy(CameraManagerTarget t) : base(target => { return new CameraManagerProxy(); })
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="script"></param>
        //public static void Register(Script script)
        //{
        // Register the proxy, using a function which creates a proxy from the target type.
        //UserData.RegisterProxyType<CameraManagerProxy, Camera>(target =>
        //{
        //return new CameraManagerProxy();
        //});

        //script.Globals[nameof(Camera)] = Camera.main;
        //return;
        //}

        /*
        public override IGameObject Register(Script script)
        {
            return null;
        }
        */

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public virtual Vector3 Position()
        {
            return Camera.main.transform.position;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public virtual Vector3 Euler()
        {
            return Camera.main.transform.rotation.eulerAngles;
        }

    }

    public class CameraManagerTarget : ProxyTarget<Camera>
    {

    }
}
