﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using MoonSharp.Interpreter;

/*
namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>
    [
        AddonCreatorAttribute(
        "GUI", // Category
        "A textual display of a UI control." // Description
    )]
    public class LabelElementProxy : UIElementProxy
    {
        private readonly Text text = null;

        public LabelElementProxy()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="target"></param>
        public LabelElementProxy(UIElement target)
            : base(target, ControlType.CtLabel)
        {
            this.text = target.gameObject.AddComponent<Text>();
            this.text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="script"></param>
        public static void Register(Script script)
        {
            // Register the proxy, using a function which creates a proxy from the target type.
            UserData.RegisterProxyType<LabelElementProxy, LabelElement>(target =>
            {
                return new LabelElementProxy(target);
            });


            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        public void SetText(string text)
        {
            this.text.text = text;
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="alignment"></param>
        public void SetAlignment(TextAnchor alignment)
        {
            this.text.alignment = alignment;
            return;
        }
    }

}
*/
