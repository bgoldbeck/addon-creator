﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;



namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { }, // Dependencies
        new Type[] { }, // Register Types
        false // HideInInspector
    )]
    public class MathfProxy : MoonsharpProxy
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            MathfProxy proxyDelegate(EventManagerTarget r) => new MathfProxy();
            UserData.RegisterProxyType((Func<EventManagerTarget, MathfProxy>)proxyDelegate);
            return;
        }

        // Static Properties
        public static float Deg2Rad { get { return UnityEngine.Mathf.Deg2Rad; } }
        public static float Epsilon { get { return UnityEngine.Mathf.Epsilon; } }
        public static float Infinity { get { return UnityEngine.Mathf.Infinity; } }
        public static float NegativeInfinity { get { return UnityEngine.Mathf.NegativeInfinity; } }
        public static float PI { get { return UnityEngine.Mathf.PI; } }
        public static float Rad2Deg { get { return UnityEngine.Mathf.Rad2Deg; } }


        // Static Methods
        // Moonsharp doesn't always pick the best option for overloaded functions.
        //public static int Abs(int value) { return UnityEngine.Mathf.Abs(value); }

        public static float Abs(float f) { return UnityEngine.Mathf.Abs(f); }

        public static float Acos(float f) { return UnityEngine.Mathf.Acos(f); }

        public static bool Approximately(float a, float b) { return UnityEngine.Mathf.Approximately(a, b); }

        public static float Asin(float f) { return UnityEngine.Mathf.Asin(f); }

        public static float Atan(float f) { return UnityEngine.Mathf.Atan(f); }

        public static float Atan2(float y, float x) { return UnityEngine.Mathf.Atan2(y, x); }

        public static float Ceil(float f) { return UnityEngine.Mathf.Ceil(f); }

        public static int CeilToInt(float f) { return UnityEngine.Mathf.CeilToInt(f); }

        // Moonsharp doesn't always pick the best option for overloaded functions.
        //public static int Clamp(int value, int min, int max) { return UnityEngine.Mathf.Clamp(value, min, max); }

        public static float Clamp(float value, float min, float max) { return UnityEngine.Mathf.Clamp(value, min, max); }

        public static float Clamp01(float value) { return UnityEngine.Mathf.Clamp01(value); }

        public static int ClosestPowerOfTwo(int value) { return UnityEngine.Mathf.ClosestPowerOfTwo(value); }

        public static ColorProxy CorrelatedColorTemperatureToRGB(float kelvin) { return new ColorProxy(UnityEngine.Mathf.CorrelatedColorTemperatureToRGB(kelvin)); }

        public static float Cos(float f) { return UnityEngine.Mathf.Cos(f); }

        public static float DeltaAngle(float current, float target) { return UnityEngine.Mathf.DeltaAngle(current, target); }

        public static float Exp(float power) { return UnityEngine.Mathf.Exp(power); }

        public static ushort FloatToHalf(float val) { return UnityEngine.Mathf.FloatToHalf(val); }

        public static float Floor(float f) { return UnityEngine.Mathf.Floor(f); }

        public static int FloorToInt(float f) { return UnityEngine.Mathf.FloorToInt(f); }

        public static float Gamma(float value, float absmax, float gamma) { return UnityEngine.Mathf.Gamma(value, absmax, gamma); }

        public static float GammaToLinearSpace(float value) { return UnityEngine.Mathf.GammaToLinearSpace(value); }

        public static float HalfToFloat(ushort val) { return UnityEngine.Mathf.HalfToFloat(val); }

        public static float InverseLerp(float a, float b, float value) { return UnityEngine.Mathf.InverseLerp(a, b, value); }

        public static bool IsPowerOfTwo(int value) { return UnityEngine.Mathf.IsPowerOfTwo(value); }

        public static float Lerp(float a, float b, float t) { return UnityEngine.Mathf.Lerp(a, b, t); }
        public static float LerpAngle(float a, float b, float t) { return UnityEngine.Mathf.LerpAngle(a, b, t); }
        public static float LerpUnclamped(float a, float b, float t) { return UnityEngine.Mathf.LerpUnclamped(a, b, t); }

        public static float LinearToGammaSpace(float value) { return UnityEngine.Mathf.LinearToGammaSpace(value); }

        public static float Log(float f, float p) { return UnityEngine.Mathf.Log(f, p); }

        public static float Log(float f) { return UnityEngine.Mathf.Log(f); }

        public static float Log10(float f) { return UnityEngine.Mathf.Log10(f); }

        // Moonsharp cant overload float and in for Max()
        //public static int Max(int a, int b) { return UnityEngine.Mathf.Max(a, b); }
        //public static int Max(params int[] values) { return UnityEngine.Mathf.Max(values); }

        public static float Max(params float[] values) { return UnityEngine.Mathf.Max(values); }
        //public static float Max(float a, float b) { return UnityEngine.Mathf.Max(a, b); }


        // Moonsharp cant overload float and in for Min()
        //public static int Min(params int[] values) { return UnityEngine.Mathf.Min(values); }
        //public static int Min(int a, int b) { return UnityEngine.Mathf.Max(a, b); }

        public static float Min(params float[] values) { return UnityEngine.Mathf.Min(values); }
        //public static float Min(float a, float b) { return UnityEngine.Mathf.Min(a, b); }

        public static float MoveTowards(float current, float target, float maxDelta) { return UnityEngine.Mathf.MoveTowards(current, target, maxDelta); }
        public static float MoveTowardsAngle(float current, float target, float maxDelta) { return UnityEngine.Mathf.MoveTowardsAngle(current, target, maxDelta); }

        public static int NextPowerOfTwo(int value) { return UnityEngine.Mathf.NextPowerOfTwo(value); }

        public static float PerlinNoise(float x, float y) { return UnityEngine.Mathf.PerlinNoise(x, y); }

        public static float PingPong(float t, float length) { return UnityEngine.Mathf.PingPong(t, length); }

        public static float Pow(float f, float p) { return UnityEngine.Mathf.Pow(f, p); }

        public static float Repeat(float t, float length) { return UnityEngine.Mathf.Repeat(t, length); }
        public static float Round(float f) { return UnityEngine.Mathf.Round(f); }
        public static int RoundToInt(float f) { return UnityEngine.Mathf.RoundToInt(f); }
        public static float Sign(float f) { return UnityEngine.Mathf.Sign(f); }
        public static float Sin(float f) { return UnityEngine.Mathf.Sin(f); }

        public static DynValue SmoothDamp(float current, float target, float currentVelocity, float smoothTime, float maxSpeed)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }

        public static DynValue SmoothDamp(float current, float target, float currentVelocity, float smoothTime)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }

        public static DynValue SmoothDamp(float current, float target, float currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }

        public static DynValue SmoothDampAngle(float current, float target, float currentVelocity, float smoothTime)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }

        public static DynValue SmoothDampAngle(float current, float target, float currentVelocity, float smoothTime, float maxSpeed)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }


        public static DynValue SmoothDampAngle(float current, float target, float currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
        {
            float val = UnityEngine.Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
            return DynValue.NewTuple(DynValue.NewNumber(val), DynValue.NewNumber(currentVelocity));
        }

        public static float SmoothStep(float from, float to, float t) { return UnityEngine.Mathf.SmoothStep(from, to, t); }

        public static float Sqrt(float f) { return UnityEngine.Mathf.Sqrt(f); }

        public static float Tan(float f) { return UnityEngine.Mathf.Tan(f); }
    }
}

