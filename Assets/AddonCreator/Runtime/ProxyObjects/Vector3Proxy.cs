﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using MoonSharp.Interpreter.Interop.BasicDescriptors;
using MoonSharp.Interpreter.Interop.StandardDescriptors;
using System;
using System.Linq;
using System.Reflection;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
      AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { }, // Dependencies
        new Type[] { }, // Register Types
        false // HideInInspector
    )]
    public class Vector3Proxy : MoonsharpProxy<Vector3Proxy, Vector3Target>
    {

        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            Vector3Proxy proxyDelegate(Vector3Target r) => new Vector3Proxy(r);
            UserData.RegisterProxyType((Func<Vector3Target, Vector3Proxy>)proxyDelegate);
            return;
        }

        public Vector3Proxy() : base(t => { return new Vector3Proxy(t); })
        {
            this.target = new Vector3Target();
        }

        public Vector3Proxy(float x, float y) : base(t => { return new Vector3Proxy(t); })
        {
            this.target = new Vector3Target(new Vector3(x, y, 0f));
        }

        public Vector3Proxy(float x, float y, float z) : base(t => { return new Vector3Proxy(t); })
        {
            this.target = new Vector3Target(new Vector3(x, y, z));
        }

        public Vector3Proxy(Vector3Proxy proxy) : base(t => { return new Vector3Proxy(t); })
        {
            this.target = new Vector3Target(proxy.Target.Unwrap);
        }

        [MoonSharpVisible(false)]
        public Vector3Proxy(Vector3Target target) : base(t => { return new Vector3Proxy(t); })
        {
            this.target = target;
        }

        [MoonSharpVisible(false)]
        public Vector3Proxy(Vector3 v) : base(t => { return new Vector3Proxy(t); })
        {
            this.target = new Vector3Target(v);
        }

        public const float kEpsilon = 1E-05F;
        public const float kEpsilonNormalSqrt = 1E-15F;

        public static Vector3Proxy left { get { return new Vector3Proxy(Vector3.left); } }
        public static Vector3Proxy right { get { return new Vector3Proxy(Vector3.right); } }
        public static Vector3Proxy up { get { return new Vector3Proxy(Vector3.up); } }
        public static Vector3Proxy down { get { return new Vector3Proxy(Vector3.down); } }
        public static Vector3Proxy forward { get { return new Vector3Proxy(Vector3.forward); } }
        public static Vector3Proxy back { get { return new Vector3Proxy(Vector3.back); } }

        public static Vector3Proxy negativeInfinity { get { return new Vector3Proxy(Vector3.negativeInfinity); } }
        public static Vector3Proxy positiveInfinity { get { return new Vector3Proxy(Vector3.positiveInfinity); } }

        public static Vector3Proxy one { get { return new Vector3Proxy(Vector3.one); } }
        public static Vector3Proxy zero { get { return new Vector3Proxy(Vector3.zero); } }

        public float x { get { return target.Unwrap.x; } }
        public float y { get { return target.Unwrap.y; } }
        public float z { get { return target.Unwrap.z; } }

        public float magnitude { get { return target.Unwrap.magnitude; } }

        public Vector3Proxy normalized { get { return new Vector3Proxy(target.Unwrap.normalized); } }

        public float sqrMagnitude { get { return target.Unwrap.sqrMagnitude; } }

        public static float Angle(Vector3Proxy from, Vector3Proxy to)
        {
            return Vector3.Angle(from.Target.Unwrap, to.Target.Unwrap);
        }

        public static Vector3Proxy ClampMagnitude(Vector3Proxy vector, float maxLength)
        {
            return new Vector3Proxy(Vector3.ClampMagnitude(vector.Target.Unwrap, maxLength));
        }

        public static Vector3Proxy Cross(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            return new Vector3Proxy(Vector3.Cross(lhs.Target.Unwrap, rhs.Target.Unwrap));
        }

        public static float Distance(Vector3Proxy a, Vector3Proxy b)
        {
            return Vector3.Distance(a.Target.Unwrap, b.Target.Unwrap);
        }

        public static float Dot(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            return Vector3.Dot(lhs.Target.Unwrap, rhs.Target.Unwrap);
        }

        public static Vector3Proxy Lerp(Vector3Proxy a, Vector3Proxy b, float t)
        {
            return new Vector3Proxy(Vector3.Lerp(a.Target.Unwrap, b.Target.Unwrap, t));
        }

        public static Vector3Proxy LerpUnclamped(Vector3Proxy a, Vector3Proxy b, float t)
        {
            return new Vector3Proxy(Vector3.LerpUnclamped(a.Target.Unwrap, b.Target.Unwrap, t));
        }

        public static float Magnitude(Vector3Proxy vector)
        {
            return Vector3.Magnitude(vector.Target.Unwrap);
        }

        public static Vector3Proxy Max(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            return new Vector3Proxy(Vector3.Max(lhs.Target.Unwrap, rhs.Target.Unwrap));
        }

        public static Vector3Proxy Min(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            return new Vector3Proxy(Vector3.Min(lhs.Target.Unwrap, rhs.Target.Unwrap));
        }

        public static Vector3Proxy MoveTowards(Vector3Proxy current, Vector3Proxy target, float maxDistanceDelta)
        {
            return new Vector3Proxy(Vector3.MoveTowards(current.Target.Unwrap, target.Target.Unwrap, maxDistanceDelta));
        }

        public static Vector3Proxy Normalize(Vector3Proxy v)
        {
            return new Vector3Proxy(Vector3.Normalize(v.Target.Unwrap));
        }

        public static DynValue OrthoNormalize(Vector3Proxy normal, Vector3Proxy tangent, Vector3Proxy binormal)
        {
            Vector3 n = normal.Target.Unwrap;
            Vector3 t = tangent.Target.Unwrap;
            Vector3 b = binormal.Target.Unwrap;

            Vector3.OrthoNormalize(ref n, ref t, ref b);

            normal.Set(n.x, n.y, n.z);
            tangent.Set(t.x, t.y, t.z);
            binormal.Set(b.x, b.y, b.z);
            return DynValue.NewTuple(new DynValue[] { UserData.Create(normal), UserData.Create(tangent), UserData.Create(binormal) });
        }

        public static DynValue OrthoNormalize(Vector3Proxy normal, Vector3Proxy tangent)
        {
            Vector3 n = normal.Target.Unwrap;
            Vector3 t = tangent.Target.Unwrap;

            Vector3.OrthoNormalize(ref n, ref t);

            normal.Set(n.x, n.y, n.z);
            tangent.Set(t.x, t.y, t.z);

            return DynValue.NewTuple(new DynValue[] { UserData.Create(normal), UserData.Create(tangent) });
        }

        public static Vector3Proxy Project(Vector3Proxy vector, Vector3Proxy onNormal)
        {
            return new Vector3Proxy(Vector3.Project(vector.Target.Unwrap, onNormal.Target.Unwrap));
        }

        public static Vector3Proxy ProjectOnPlane(Vector3Proxy vector, Vector3Proxy planeNormal)
        {
            return new Vector3Proxy(Vector3.ProjectOnPlane(vector.Target.Unwrap, planeNormal.Target.Unwrap));
        }

        public static Vector3Proxy Reflect(Vector3Proxy inDirection, Vector3Proxy inNormal)
        {
            return new Vector3Proxy(Vector3.Reflect(inDirection.Target.Unwrap, inNormal.Target.Unwrap));
        }

        public static Vector3Proxy RotateTowards(Vector3Proxy current, Vector3Proxy target, float maxRadiansDelta, float maxMagnitudeDelta)
        {
            return new Vector3Proxy(Vector3.RotateTowards(current.Target.Unwrap, target.Target.Unwrap, maxRadiansDelta, maxMagnitudeDelta));
        }

        public static Vector3Proxy Scale(Vector3Proxy a, Vector3Proxy b)
        {
            return new Vector3Proxy(Vector3.Scale(a.Target.Unwrap, b.Target.Unwrap));
        }

        public static float SignedAngle(Vector3Proxy from, Vector3Proxy to, Vector3Proxy axis)
        {
            return Vector3.SignedAngle(from.Target.Unwrap, to.Target.Unwrap, axis.Target.Unwrap);
        }

        public static Vector3Proxy Slerp(Vector3Proxy a, Vector3Proxy b, float t)
        {
            return new Vector3Proxy(Vector3.Slerp(a.Target.Unwrap, b.Target.Unwrap, t));
        }

        public static Vector3Proxy SlerpUnclamped(Vector3Proxy a, Vector3Proxy b, float t)
        {
            return new Vector3Proxy(Vector3.Slerp(a.Target.Unwrap, b.Target.Unwrap, t));
        }

        public static DynValue SmoothDamp(Vector3Proxy current, Vector3Proxy target, Vector3Proxy currentVelocity, float smoothTime)
        {
            Vector3 cv = currentVelocity.target.Unwrap;

            Vector3Proxy result = new Vector3Proxy(Vector3.SmoothDamp(current.Target.Unwrap, target.Target.Unwrap, ref cv, smoothTime));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector3Proxy(cv)));
        }

        public static DynValue SmoothDamp(Vector3Proxy current, Vector3Proxy target, Vector3Proxy currentVelocity, float smoothTime, float maxSpeed)
        {
            Vector3 cv = currentVelocity.target.Unwrap;

            Vector3Proxy result = new Vector3Proxy(Vector3.SmoothDamp(current.Target.Unwrap, target.Target.Unwrap, ref cv, smoothTime, maxSpeed));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector3Proxy(cv)));
        }

        public static DynValue SmoothDamp(Vector3Proxy current, Vector3Proxy target, Vector3Proxy currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
        {
            Vector3 cv = currentVelocity.target.Unwrap;

            Vector3Proxy result = new Vector3Proxy(Vector3.SmoothDamp(current.Target.Unwrap, target.Target.Unwrap, ref cv, smoothTime, maxSpeed, deltaTime));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector3Proxy(cv)));
        }

        public static float SqrMagnitude(Vector3Proxy vector)
        {
            return Vector3.SqrMagnitude(vector.Target.Unwrap);
        }

        // Class Methods
        public override int GetHashCode()
        {
            return Target.Unwrap.GetHashCode();
        }

        public override string ToString()
        {
            return Target.Unwrap.ToString();
        }

        public void Normalize()
        {
            target = new Vector3Target(Vector3.Normalize(target.Unwrap));
            return;
        }

        public void Scale(Vector3Proxy scale)
        {
            target = new Vector3Target(Vector3.Scale(target.Unwrap, scale.Target.Unwrap));
            return;
        }

        public void Set(float newX, float newY, float newZ)
        {
            target = new Vector3Target(new Vector3(newX, newY, newZ));
            return;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Vector3Proxy);
        }

        public bool Equals(Vector3Proxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        [MoonSharpVisible(false)]
        public static bool operator ==(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(Vector3Proxy lhs, Vector3Proxy rhs)
        {
            return !(lhs == rhs);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(Vector3Proxy p1, Vector3Proxy p2)
        {
            return p1 == p2;
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector3Proxy Multiplication1(Vector3Proxy o, float p)
        {
            return new Vector3Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector3Proxy Multiplication2(float p, Vector3Proxy o)
        {
            return new Vector3Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__div")]
        public static Vector3Proxy Division(Vector3Proxy o, float p)
        {
            return new Vector3Proxy(o.Target.Unwrap / p);
        }

        [MoonSharpUserDataMetamethod("__add")]
        public static Vector3Proxy Addition(Vector3Proxy p1, Vector3Proxy p2)
        {
            return new Vector3Proxy(p1.Target.Unwrap + p2.Target.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__sub")]
        public static Vector3Proxy Subtraction(Vector3Proxy p1, Vector3Proxy p2)
        {
            return new Vector3Proxy(p1.Target.Unwrap - p2.Target.Unwrap);
        }

        // Unary Minus Operator
        [MoonSharpUserDataMetamethod("__unm")]
        public static Vector3Proxy UnaryMinus(Vector3Proxy p1)
        {
            return new Vector3Proxy(-p1.Target.Unwrap);
        }


        public float this[int idx]
        {
            get
            {
                float result = 0f;
                if (idx == 0)
                {
                    result = target.Unwrap.x;
                }
                else if (idx == 1)
                {
                    result = target.Unwrap.y;
                }
                else if (idx == 2)
                {
                    result = target.Unwrap.z;
                }
                return result;
            }

        }
    }

    public class Vector3Target : ProxyTarget<UnityEngine.Vector3>
    {
        public Vector3Target() : base()
        {
        }

        public Vector3Target(UnityEngine.Vector3 v) : base(v)
        {
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
