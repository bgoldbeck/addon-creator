﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using UnityEngine;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
     AddonCreatorAttribute(
       "Unity", // Category.
       "", // Description.
        new Type[] { typeof(Vector3Proxy) }, // Dependency Types
        new Type[] {  }, // Register Types
       false // HideInInspector
    )]
    public class RayProxy : MoonsharpProxy<RayProxy, RayTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            RayProxy proxyDelegate(RayTarget r) => new RayProxy(r);
            UserData.RegisterProxyType((Func<RayTarget, RayProxy>)proxyDelegate);
            return;
        }

        public RayProxy()
        {
            this.target = new RayTarget();
        }

        public RayProxy(Vector3Proxy origin, Vector3Proxy direction)
        {
            this.target = new RayTarget(new Ray(origin.Target.Unwrap, direction.Target.Unwrap));
        }

        [MoonSharpVisible(false)]
        public RayProxy(Ray ray)
        {
            this.target = new RayTarget(ray);
        }

        [MoonSharpVisible(false)]
        public RayProxy(RayTarget target)
        {
            this.target = target;
        }


        // Class Properties
        public Vector3Proxy origin
        {
            get
            {
                return new Vector3Proxy(target.Unwrap.origin);
            }
        }

        public Vector3Proxy direction
        {
            get
            {
                return new Vector3Proxy(target.Unwrap.direction);
            }
        }

        // Class Methods
        public Vector3Proxy GetPoint(float distance)
        {
            return new Vector3Proxy(target.Unwrap.GetPoint(distance));
        }

        public bool Equals(RayProxy other)
        {
            if (other == null)
            {
                return false;
            }
            return target.Unwrap.Equals(other.target.Unwrap);
        }

        public override bool Equals(object other)
        {
            return Equals(other as RayProxy);
        }

        public override int GetHashCode()
        {
            return target.Unwrap.GetHashCode();
        }

        public override string ToString()
        {
            return target.Unwrap.ToString();
        }

        public string ToString(string format)
        {
            return target.Unwrap.ToString(format);
        }

        // Operators

        [MoonSharpVisible(false)]
        public static bool operator ==(RayProxy lhs, RayProxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(RayProxy lhs, RayProxy rhs)
        {
            return !(lhs == rhs);
        }

        // Metamethods
        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(RayProxy r1, RayProxy r2)
        {
            return r1 == r2;
        }

    }

    /// <summary>
    ///
    /// </summary>
    public class RayTarget : ProxyTarget<UnityEngine.Ray>
    {
        public RayTarget() : base()
        {
        }

        public RayTarget(UnityEngine.Ray r) : base(r)
        {
        }
    }
}
#pragma warning restore IDE1006 // Naming Styles
