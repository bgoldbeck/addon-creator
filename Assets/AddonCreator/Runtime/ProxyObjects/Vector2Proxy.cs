﻿using System;
using UnityEngine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
#pragma warning disable IDE1006 // Naming Styles


namespace AddonCreator
{
    [
        AddonCreatorAttribute(
        "Unity", // Category.
        "", // Description.
        new Type[] { }, // Dependencies
        new Type[] { }, // Register Types
        false // Hide In Inspector?
    )]
    public class Vector2Proxy : MoonsharpProxy<Vector2Proxy, Vector2Target>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            Vector2Proxy proxyDelegate(Vector2Target r) => new Vector2Proxy(r);
            UserData.RegisterProxyType((Func<Vector2Target, Vector2Proxy>)proxyDelegate);
            return;
        }

        public const float kEpsilon = 1E-05F;
        public const float kEpsilonNormalSqrt = 1E-15F;

        public Vector2Proxy()
        {
            this.target = new Vector2Target();
        }

        public Vector2Proxy(float x, float y)
        {
            this.target = new Vector2Target(new Vector2(x, y));
        }

        public Vector2Proxy(Vector2Proxy proxy)
        {
            this.target = new Vector2Target(proxy.Target.Unwrap);
        }

        [MoonSharpVisible(false)]
        public Vector2Proxy(Vector2Target target)
        {
            this.target = target;
        }

        [MoonSharpVisible(false)]
        public Vector2Proxy(Vector2 v)
        {
            this.target = new Vector2Target(v);
        }

        public float this[int index]
        {
            get
            {
                return target.Unwrap[index];
            }
        }

        // Static properties.
        public static Vector2Proxy right { get { return new Vector2Proxy(Vector2.right); } }
        public static Vector2Proxy left { get { return new Vector2Proxy(Vector2.left); } }
        public static Vector2Proxy down { get { return new Vector2Proxy(Vector2.down); } }
        public static Vector2Proxy up { get { return new Vector2Proxy(Vector2.up); } }
        public static Vector2Proxy one { get { return new Vector2Proxy(Vector2.one); } }
        public static Vector2Proxy zero { get { return new Vector2Proxy(Vector2.zero); } }
        public static Vector2Proxy positiveInfinity { get { return new Vector2Proxy(Vector2.positiveInfinity); } }
        public static Vector2Proxy negativeInfinity { get { return new Vector2Proxy(Vector2.negativeInfinity); } }

        // Class properties.
        public float sqrMagnitude { get { return target.Unwrap.sqrMagnitude; } }
        public Vector2Proxy normalized { get { return new Vector2Proxy(target.Unwrap.normalized); } }
        public float magnitude { get { return target.Unwrap.magnitude; } }

        // Public static methods.
        public static float Angle(Vector2Proxy from, Vector2Proxy to) { return Vector2.Angle(from.target.Unwrap, to.target.Unwrap); }
        public static Vector2Proxy ClampMagnitude(Vector2Proxy vector, float maxLength) { return new Vector2Proxy(Vector2.ClampMagnitude(vector.target.Unwrap, maxLength)); }
        public static float Distance(Vector2Proxy a, Vector2Proxy b) { return Vector2.Distance(a.target.Unwrap, b.target.Unwrap); }
        public static float Dot(Vector2Proxy lhs, Vector2Proxy rhs) { return Vector2.Dot(lhs.target.Unwrap, rhs.target.Unwrap); }
        public static Vector2Proxy Lerp(Vector2Proxy a, Vector2Proxy b, float t) { return new Vector2Proxy(Vector2.Lerp(a.target.Unwrap, b.target.Unwrap, t)); }
        public static Vector2Proxy LerpUnclamped(Vector2Proxy a, Vector2Proxy b, float t) { return new Vector2Proxy(Vector2.LerpUnclamped(a.target.Unwrap, b.target.Unwrap, t)); }
        public static Vector2Proxy Max(Vector2Proxy lhs, Vector2Proxy rhs) { return new Vector2Proxy(Vector2.Max(lhs.target.Unwrap, rhs.target.Unwrap)); }
        public static Vector2Proxy Min(Vector2Proxy lhs, Vector2Proxy rhs) { return new Vector2Proxy(Vector2.Min(lhs.target.Unwrap, rhs.target.Unwrap)); }
        public static Vector2Proxy MoveTowards(Vector2Proxy current, Vector2Proxy target, float maxDistanceDelta) { return new Vector2Proxy(Vector2.MoveTowards(current.target.Unwrap, target.target.Unwrap, maxDistanceDelta)); }
        public static Vector2Proxy Perpendicular(Vector2Proxy inDirection) { return new Vector2Proxy(Vector2.Perpendicular(inDirection.target.Unwrap)); }
        public static Vector2Proxy Reflect(Vector2Proxy inDirection, Vector2Proxy inNormal) { return new Vector2Proxy(Vector2.Perpendicular(inDirection.target.Unwrap)); }
        public static Vector2Proxy Scale(Vector2Proxy a, Vector2Proxy b) { return new Vector2Proxy(Vector2.Scale(a.target.Unwrap, b.target.Unwrap)); }
        public static float SignedAngle(Vector2Proxy from, Vector2Proxy to) { return Vector2.SignedAngle(from.target.Unwrap, to.target.Unwrap); }


        public static DynValue SmoothDamp(Vector2Proxy current, Vector2Proxy target, Vector2Proxy currentVelocity, float smoothTime)
        {
            Vector2 cv = currentVelocity.target.Unwrap;
            Vector2Proxy result = new Vector2Proxy(Vector2.SmoothDamp(current.target.Unwrap, target.target.Unwrap, ref cv, smoothTime));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector2Proxy(cv)));
        }

        public static DynValue SmoothDamp(Vector2Proxy current, Vector2Proxy target, Vector2Proxy currentVelocity, float smoothTime, float maxSpeed)
        {
            Vector2 cv = currentVelocity.target.Unwrap;
            Vector2Proxy result = new Vector2Proxy(Vector2.SmoothDamp(current.target.Unwrap, target.target.Unwrap, ref cv, smoothTime, maxSpeed));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector2Proxy(cv)));
        }

        public static DynValue SmoothDamp(Vector2Proxy current, Vector2Proxy target, Vector2Proxy currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
        {
            Vector2 cv = currentVelocity.target.Unwrap;
            Vector2Proxy result = new Vector2Proxy(Vector2.SmoothDamp(current.target.Unwrap, target.target.Unwrap, ref cv, smoothTime, maxSpeed, deltaTime));

            return DynValue.NewTuple(UserData.Create(result), UserData.Create(new Vector2Proxy(cv)));
        }

        public static float SqrMagnitude(Vector2Proxy a)
        {
            return Vector2.SqrMagnitude(a.target.Unwrap);
        }

        // Class methods
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Vector2Proxy);
        }

        public bool Equals(Vector2Proxy v)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(v, null))
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, v))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != v.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return Target.Unwrap.Equals(v.Target.Unwrap);
        }

        public override int GetHashCode() { return target.Unwrap.GetHashCode(); }

        public void Normalize()
        {
            target = new Vector2Target(target.Unwrap.normalized);
            return;
        }

        public void Scale(Vector2Proxy scale)
        {
            target = new Vector2Target(Vector2.Scale(target.Unwrap, scale.target.Unwrap));
            return;
        }

        public void Set(float newX, float newY)
        {
            target = new Vector2Target(new Vector2(newX, newY));
            return;
        }

        public float SqrMagnitude()
        {
            return target.Unwrap.SqrMagnitude();
        }

        public string ToString(string format)
        {
            return target.Unwrap.ToString(format);
        }

        public override string ToString()
        {
            return target.Unwrap.ToString();
        }

        // MetaMethods and operator overloads
        [MoonSharpVisible(false)]
        public static bool operator ==(Vector2Proxy lhs, Vector2Proxy rhs)
        {
            // Check for null on left side.
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    // null == null = true.
                    return true;
                }

                // Only the left side is null.
                return false;
            }
            // Equals handles case of null on right side.
            return lhs.Equals(rhs);
        }

        [MoonSharpVisible(false)]
        public static bool operator !=(Vector2Proxy lhs, Vector2Proxy rhs)
        {
            return !(lhs == rhs);
        }

        [MoonSharpUserDataMetamethod("__eq")]
        public static bool Equality(Vector2Proxy p1, Vector2Proxy p2)
        {
            return p1 == p2;
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector2Proxy Multiplication1(Vector2Proxy o, float p)
        {
            return new Vector2Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__mul")]
        public static Vector2Proxy Multiplication2(float p, Vector2Proxy o)
        {
            return new Vector2Proxy(o.Target.Unwrap * p);
        }

        [MoonSharpUserDataMetamethod("__div")]
        public static Vector2Proxy Division(Vector2Proxy o, float p)
        {
            return new Vector2Proxy(o.Target.Unwrap / p);
        }

        [MoonSharpUserDataMetamethod("__add")]
        public static Vector2Proxy Addition(Vector2Proxy p1, Vector2Proxy p2)
        {
            return new Vector2Proxy(p1.Target.Unwrap + p2.Target.Unwrap);
        }

        [MoonSharpUserDataMetamethod("__sub")]
        public static Vector2Proxy Subtraction(Vector2Proxy p1, Vector2Proxy p2)
        {
            return new Vector2Proxy(p1.Target.Unwrap - p2.Target.Unwrap);
        }

        // Unary Minus Operator
        [MoonSharpUserDataMetamethod("__unm")]
        public static Vector2Proxy UnaryMinus(Vector2Proxy p1)
        {
            return new Vector2Proxy(-p1.Target.Unwrap);
        }

    }

    /// <summary>
    ///
    /// </summary>
    public class Vector2Target : ProxyTarget<UnityEngine.Vector2>
    {
        public Vector2Target() : base()
        {
        }

        public Vector2Target(UnityEngine.Vector2 v) : base(v)
        {
        }
    }
}


#pragma warning restore IDE1006 // Naming Styles
