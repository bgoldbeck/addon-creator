﻿using MoonSharp.Interpreter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public class RandomTarget : ProxyTarget<UnityEngine.Random>
    {
        public RandomTarget() : base()
        {
        }

        public RandomTarget(UnityEngine.Random r) : base(r)
        {
        }

    }
}
