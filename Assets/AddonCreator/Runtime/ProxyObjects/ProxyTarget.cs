﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;


namespace AddonCreator
{
    public abstract class ProxyTarget<T>
    {
        private T val;

        public ProxyTarget()
        {
        }

        public ProxyTarget(T t)
        {
            Wrap(t);
        }

        public virtual T Unwrap
        {
            get
            {
                return val;
            }
        }

        public void Wrap(T obj)
        {
            this.val = obj;
            return;
        }
    }
}
