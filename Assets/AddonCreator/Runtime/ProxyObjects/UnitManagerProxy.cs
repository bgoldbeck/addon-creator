﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    /*
    /// <summary>
    ///
    /// </summary>
    public class UnitManagerProxy : MoonsharpProxy<UnitManager>
    {

        public UnitManagerProxy()
        {
        }

        public UnitManagerProxy(UnitManager target) : base(target)
        {
        }

        /*
        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public int GetUnitHealth(string tag)
        {
            int result = 0;
            if (GetResourcePoolsFromTag(tag, out ResourcePools resourcePools))
            {
                result = resourcePools.Health;
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public int GetUnitHealthMax(string tag)
        {
            int result = 0;
            if (GetResourcePoolsFromTag(tag, out ResourcePools resourcePools))
            {
                result = resourcePools.HealthMax;
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="powerType"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public int GetUnitPower(string tag, PowerType powerType)
        {
            int result = 0;
            if (GetResourcePoolsFromTag(tag, out ResourcePools resourcePools))
            {
                switch (powerType)
                {
                    case PowerType.PowerTypeHealth:
                        result = resourcePools.Health;
                        break;
                    case PowerType.PowerTypeMana:
                        result = resourcePools.Mana;
                        break;
                    case PowerType.PowerTypeStamina:
                        result = resourcePools.Stamina;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="powerType"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public int GetUnitPowerMax(string tag, PowerType powerType)
        {
            int result = 0;
            if (GetResourcePoolsFromTag(tag, out ResourcePools resourcePools))
            {
                switch (powerType)
                {
                    case PowerType.PowerTypeHealth:
                        result = resourcePools.HealthMax;
                        break;
                    case PowerType.PowerTypeMana:
                        result = resourcePools.ManaMax;
                        break;
                    case PowerType.PowerTypeStamina:
                        result = resourcePools.StaminaMax;
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public string GetUnitName(string tag)
        {
            string result = "";
            switch (tag)
            {
                case "player":
                    result = proxyTarget.LocalPlayer.NameOf;
                    break;
                case "target":
                    if (proxyTarget.LocalPlayer.Target != null)
                    {
                        result = proxyTarget.LocalPlayer.Target.NameOf;
                    }
                    break;
                default:
                    break;
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public DynValue GetBuffInfo(string tag, int i)
        {
            DynValue result = DynValue.NewTuple();

            if (GetNetworkStatusEffectsFromTag(tag, out NetworkStatusEffects networkStatusEffects))
            {
                StatusEffectItem statusEffectInfo = networkStatusEffects.GetStatusEffect(i);

                result = DynValue.NewTuple(new DynValue[]
                {
                DynValue.NewString(statusEffectInfo.effectName), // Name
                DynValue.NewNumber(statusEffectInfo.start), // Start
                DynValue.NewNumber(statusEffectInfo.end), // Finish
                DynValue.NewNumber(statusEffectInfo.stack), // Stack
                DynValue.NewString(statusEffectInfo.icon), // Icon
                });
            }

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [MoonSharpVisible(true)]
        public int GetNumBuffs(string tag)
        {
            int result = 0;
            if (GetNetworkStatusEffectsFromTag(tag, out NetworkStatusEffects networkStatusEffects))
            {
                result = networkStatusEffects.GetNumberEffects();
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        [MoonSharpVisible(false)]
        private bool GetResourcePoolsFromTag(string tag, out ResourcePools result)
        {
            result = null;
            switch (tag)
            {
                case "player":
                    result = proxyTarget.LocalPlayerResources;
                    break;
                case "target":
                    if (proxyTarget.LocalPlayer.Target != null)
                    {
                        result = proxyTarget.LocalPlayer.Target.GetComponent<ResourcePools>();
                    }
                    break;
                default:
                    break;
            }
            return (result != null);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        [MoonSharpVisible(false)]
        private bool GetNetworkStatusEffectsFromTag(string tag, out NetworkStatusEffects result)
        {
            result = null;
            switch (tag)
            {
                case "player":
                    result = proxyTarget.GetComponent<NetworkStatusEffects>(); ;
                    break;
                case "target":
                    if (proxyTarget.LocalPlayer.Target != null)
                    {
                        result = proxyTarget.LocalPlayer.Target.GetComponent<NetworkStatusEffects>();
                    }
                    break;
                default:
                    break;
            }
            return (result != null);
        }
    }
    */

}
