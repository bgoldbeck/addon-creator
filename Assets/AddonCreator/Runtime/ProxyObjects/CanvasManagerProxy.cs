﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp;
using MoonSharp.Interpreter.Interop;
using MoonSharp.Interpreter;
using UnityEngine.UI;


namespace AddonCreator
{

    /// <summary>
    /// The anchor position of this UI Element. [AddonCreatorEnum]
    /// </summary>
    [AddonCreatorEnum]
    public enum AnchorPosition
    {
        Left = 2,
        TopLeft = 3,
        BottomLeft = 6,
        Center = 128,
        Right = 8,
        TopRight = 9,
        BottomRight = 12,
        Top = 1,
        Bottom = 4,
    }

    /// <summary>
    /// The type of control of this UI Element. [AddonCreatorEnum]
    /// </summary>
    [AddonCreatorEnum]
    public enum ControlType
    {
        CtNone,
        CtTop,
        CtBackdrop,
        CtButton,
        CtLabel,
        CtStatusBar,
    }

    /// <summary>
    ///
    /// </summary>
    [
    AddonCreatorAttribute(
        "Graphical User Interface", // Category
        "Mangement of GUI Elements.", // Description
        new Type[] { typeof(UIElementProxy)}, // Dependency Types
        new Type[] { }, // CLR Types
        false // HideInInspector
    )]
    public class CanvasManagerProxy : MoonsharpProxy<CanvasManagerProxy, CanvasManagerTarget>
    {
        /// <summary>
        /// Required function for proxy objects to be used in Lua scripts.
        /// </summary>
        public static void Register()
        {
            CanvasManagerProxy proxyDelegate(CanvasManagerTarget r) => new CanvasManagerProxy(r);
            UserData.RegisterProxyType((Func<CanvasManagerTarget, CanvasManagerProxy>)proxyDelegate);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        [MoonSharpHidden]
        public CanvasManagerProxy()
        {
            Debug.Log("canvas create proxy");
        }
        /// <summary>
        ///
        /// </summary>
        [MoonSharpHidden]
        public CanvasManagerProxy(CanvasManagerTarget t)
        {
            Debug.Log("canvas create proxy");
            wrapDelegate = r => new CanvasManagerProxy(r);
        }

        /// <summary>
        ///
        /// </summary>
        [MoonSharpHidden]
        ~CanvasManagerProxy()
        {

            Debug.Log("canvas destroy proxy");
        }

        /// <summary>
        ///
        /// </summary>
        public UIElement Root { get; } = null;


        /// <summary>
        ///
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public UIElementProxy CreateTopLevelControl(string name)
        {
            return Target.InstantiateControl(name, Root, ControlType.CtTop);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="uiElementProxy"></param>
        public void Destroy(UIElementProxy uiElementProxy)
        {
            if (uiElementProxy == null)
            {
                Debug.LogWarning("UI Element is null");
                return;
            }
            return;
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        /// <param name="controlType"></param>
        /// <returns></returns>
        public UIElementProxy CreateControl(string name, UIElement parent, ControlType controlType)
        {
            UIElementProxy control = Target.InstantiateControl(name, parent, controlType);
            if (parent != null)
            {
                control.Target.Unwrap.transform.SetParent(parent.transform);
            }

            return control;
        }

    }

    /// <summary>
    ///
    /// </summary>
    public class CanvasManagerTarget : ProxyTarget<CanvasManager>
    {

        private Dictionary<string, UIElement> controls = new Dictionary<string, UIElement>();
        private readonly string name = "AddonCreator:CanvasManager";

        public CanvasManagerTarget(CanvasManager o) : base(o)
        {
            if (GameObject.Find(name))
            {
                Debug.LogWarning("There should be only 1 CanvasManager");
            }
        }

        ~CanvasManagerTarget()
        {
            GameObject go = GameObject.Find(name);
            if (go != null)
            {
                GameObject.Destroy(go);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public override CanvasManager Unwrap
        {
            get
            {
                if (!GameObject.Find(name))
                {
                    GameObject o = new GameObject(name, new Type[] { typeof(CanvasManager) });
                    Wrap(o.GetComponent<CanvasManager>());
                }
                return base.Unwrap;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public UIElementProxy InstantiateControl(string name, ControlType controlType)
        {
            GameObject go = new GameObject(name, typeof(RectTransform));

            // Set the canvas as the actual parent.
            go.transform.SetParent(Unwrap.transform);

            UIElement control = go.AddComponent<LabelElement>();

            return InstantiateControl(name, null, controlType);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public UIElementProxy InstantiateControl(string name, UIElement parent, ControlType controlType)
        {
            GameObject go = new GameObject(name, typeof(RectTransform));

            // Set the canvas as the actual parent.
            go.transform.SetParent(parent.transform);

            UIElement control = go.AddComponent<LabelElement>();
            UIElementProxy uIElementProxy = new UIElementProxy(new UIElementTarget(control));

            return uIElementProxy;
        }
    }
}
