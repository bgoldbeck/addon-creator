﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp;
using MoonSharp.Interpreter;
using System.IO;
using System;
using System.Text;


namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>
    public class AddonSavedVars : MonoBehaviour
    {
        //private readonly Dictionary<string, Tuple<string, string, Script>> saveTables;

        /// <summary>
        ///
        /// </summary>
        public void SaveTables()
        {

            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="addonName"></param>
        /// <param name="tableName"></param>
        /// <param name="defaults"></param>
        /// <returns></returns>
        public Table NewGlobal(string addonName, string tableName, Table defaults, Script script)
        {
            return New(addonName, tableName, "global", defaults, script);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="addonName"></param>
        /// <param name="tableName"></param>
        /// <param name="defaults"></param>
        /// <returns></returns>
        public Table New(string addonName, string tableName, string playerName, Table defaults, Script script)
        {
            Table result = null;
            //Script script = AddonCreator.Instance().GlobalScript;

            if (script != null)
            {
                string savedVarsPath = AddonCreator.Instance().GetSavedVarsPath(addonName);

                Table globalTable = (Table)script.Globals[tableName];

                Table playerTable;
                if (globalTable == null)
                {

                    globalTable = DynValue.NewTable(script, new DynValue { }).ToObject<Table>();
                    // Create a new table to add.
                    playerTable = defaults;
                }
                else
                {
                    // Try and get the player table (if it exists from the global table)
                    playerTable = globalTable.Get(playerName).ToObject<Table>();
                    if (playerTable == null)
                    {
                        playerTable = defaults;
                    }
                }
                // Finally

                // Set up all the defaults.
                globalTable.Set(playerName, DynValue.NewTable(playerTable));

                try
                {
                    WriteSavedVarsTableToLua(globalTable, tableName, savedVarsPath);
                }
                catch (Exception exception)
                {
                    Debug.LogWarning(exception);
                }

                //if (!saveTables.ContainsKey(addonName))
                //{
                    //saveTables.Add(addonName, new Tuple<string, string, Script>(tableName, savedVarsPath, script));
                //}
                result = playerTable;
            }
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="table"></param>
        /// <param name="tableName"></param>
        /// <param name="filepath"></param>
        public void WriteSavedVarsTableToLua(Table table, string tableName, string filepath)
        {
            using (System.IO.FileStream fs = new System.IO.FileStream(filepath, FileMode.OpenOrCreate))
            {
                StringBuilder lineOut = new StringBuilder();
                lineOut.AppendLine(tableName + " = {");
                WriteTableToLuaRec("    ", table, ref lineOut);
                lineOut.AppendLine("}");

                WriteTextOut(fs, lineOut);

                fs.Close();
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tab"></param>
        /// <param name="currentTable"></param>
        /// <param name="stringBuilder"></param>
        private void WriteTableToLuaRec(string tab, Table currentTable, ref StringBuilder stringBuilder)
        {
            if (currentTable == null)
            {
                return;
            }

            IEnumerable<DynValue> keys = currentTable.Keys;
            foreach (DynValue key in keys)
            {
                DynValue value = currentTable.Get(key.String);
                if (value.Type == DataType.Table)
                {
                    Table table = currentTable.Get(key.String).Table;

                    stringBuilder.AppendLine(tab + "[" + key + "] = {");
                    WriteTableToLuaRec(tab + "    ", table, ref stringBuilder);
                    stringBuilder.AppendLine(tab + "},");
                }
                else
                {
                    stringBuilder.AppendLine(tab + "[" + key + "] = " + value + ",");
                }
            }

            return;
        }

        private void WriteTextOut(FileStream fs, StringBuilder stringBuilder)
        {
            byte[] byteTableInfo = new UTF8Encoding(true).GetBytes(stringBuilder.ToString());
            fs.Write(byteTableInfo, 0, byteTableInfo.Length);
            return;
        }

    }
}
