﻿using UnityEngine;
using UnityEditor;


namespace AddonCreator
{
    // Helper Rect extension methods
    public static class RectTransformExtensions
    {
        public static RectTransform ZeroCenter(this RectTransform rect)
        {
            rect.anchoredPosition = Vector3.zero;
            rect.localScale = Vector3.one;
            rect.pivot = new Vector2(0.5f, 0.5f);
            rect.anchorMin = new Vector2(0.5f, 0.5f);
            rect.anchorMax = new Vector2(0.5f, 0.5f);
            return rect;
        }

    }
}
