﻿using UnityEngine;


namespace AddonCreator
{
	/// <summary>
	///
	/// </summary>
	public static class TransformExtensions
	{
		public static Transform FindChildRecursive(this Transform aParent, string aName)
		{
			var result = aParent.Find(aName);
			if (result != null)
				return result;

			foreach (Transform child in aParent)
			{
				result = child.FindChildRecursive(aName);
				if (result != null)
					return result;
			}
			return null;
		}

		/// <summary>
		/// Converts RectTransform.rect's local coordinates to world space
		/// Usage example RectTransformExt.GetWorldRect(myRect, Vector2.one);
		/// </summary>
		/// <returns>The world rect.</returns>
		/// <param name="rt">RectangleTransform we want to convert to world coordinates.</param>
		/// <param name="scale">Optional scale pulled from the CanvasScaler. Default to using Vector2.one.</param>
		static public Rect GetWorldRect(this RectTransform rt)
		{
			Vector3[] worldCorners = new Vector3[4];

			rt.GetWorldCorners(worldCorners);

			return new Rect(
				worldCorners[0].x,
				worldCorners[0].y,
				worldCorners[2].x - worldCorners[0].x,
				worldCorners[2].y - worldCorners[0].y);
		}


		/// <summary>
		///
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public static void Reset(this Transform transform)
		{
			transform.localPosition = Vector3.zero;
			transform.localScale = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			return;
		}
	}
}
