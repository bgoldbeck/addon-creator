using UnityEngine;
using UnityEditor;


namespace AddonCreator
{
    // Helper Rect extension methods
    public static class RectExtensions
    {
        public static Vector2 TopLeft(this Rect rect)
        {
            return (Vector2.right * rect.xMin) + (Vector2.up * rect.yMin);
        }

        public static Vector2 TopRight(this Rect rect)
        {
            return (Vector2.right * rect.xMax) + (Vector2.up * rect.yMin);
        }

        public static Vector2 BottomLeft(this Rect rect)
        {
            return new Vector2(rect.xMin, rect.yMax);
        }

        public static Vector2 BottomRight(this Rect rect)
        {
            return new Vector2(rect.xMax, rect.yMax);
        }

        public static Vector2 CenterLeft(this Rect rect)
        {
            return new Vector2(rect.xMin, rect.yMin + (rect.height / 2f));
        }

        public static Vector2 CenterRight(this Rect rect)
        {
            return new Vector2(rect.xMax, rect.yMin + (rect.height / 2f));
        }

        public static Vector2 CenterTop(this Rect rect)
        {
            return new Vector2(rect.xMin + (rect.width / 2f), rect.yMin);
        }

        public static Vector2 CenterBottom(this Rect rect)
        {
            return new Vector2(rect.xMin + (rect.width / 2f), rect.yMax);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Rect Set(this Rect rect, Vector2 position, Vector2 size)
        {
            Rect result = new Rect();
            result.x = position.x;
            result.y = position.y;
            result.width = size.x;
            result.height = size.y;
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static Rect ScaleSizeBy(this Rect rect, float scale)
        {
            return rect.ScaleSizeBy(scale, rect.center);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="scale"></param>
        /// <param name="pivotPoint"></param>
        /// <returns></returns>
        public static Rect ScaleSizeBy(this Rect rect, float scale, Vector2 pivotPoint)
        {
            Rect result = rect;
            result.x -= pivotPoint.x;
            result.y -= pivotPoint.y;
            result.xMin *= scale;
            result.xMax *= scale;
            result.yMin *= scale;
            result.yMax *= scale;
            result.x += pivotPoint.x;
            result.y += pivotPoint.y;
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static Rect ScaleSizeBy(this Rect rect, Vector2 scale)
        {
            return rect.ScaleSizeBy(scale, rect.center);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="scale"></param>
        /// <param name="pivotPoint"></param>
        /// <returns></returns>
        public static Rect ScaleSizeBy(this Rect rect, Vector2 scale, Vector2 pivotPoint)
        {
            Rect result = rect;
            result.x -= pivotPoint.x;
            result.y -= pivotPoint.y;
            result.xMin *= scale.x;
            result.xMax *= scale.x;
            result.yMin *= scale.y;
            result.yMax *= scale.y;
            result.x += pivotPoint.x;
            result.y += pivotPoint.y;
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="thickness"></param>
        /// <param name="color"></param>
        public static void DrawOutline(this Rect rect, int thickness, Color color)
        {
            DrawLine(rect.TopLeft(), rect.TopRight(), thickness, color);
            DrawLine(rect.TopRight(), rect.BottomRight(), thickness, color);
            DrawLine(rect.BottomRight(), rect.BottomLeft(), thickness, color);
            DrawLine(rect.BottomLeft(), rect.TopLeft(), thickness, color);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="thickness"></param>
        /// <param name="color"></param>
        private static void DrawLine(Vector3 p1, Vector3 p2, int thickness, Color color)
        {
#if UNITY_EDITOR
            Color oldColor = Handles.color;
            Handles.BeginGUI();
            Handles.color = color;
            UnityEditor.Handles.DrawLine(p1, p2);
            for (int i = 1; i < (thickness); ++i)
            {
                Vector3 offset = i * Vector3.one * .5f;
                Handles.DrawLine(p1 + offset, p2 + offset);
                Handles.DrawLine(p1 - offset, p2 - offset);
            }
            Handles.EndGUI();
            Handles.color = oldColor;
#endif
            return;
        }
    }
}
