﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>
    public class AddonModule
    {
        public readonly string name;
        public readonly string content;
        private bool isLoaded = false;

        public AddonModule(string name, string content)
        {
            this.name = name;
            this.content = content;

        }

        public void SetLoaded(bool isLoaded)
        {
            this.isLoaded = isLoaded;
            return;
        }

        public bool IsLoaded()
        {
            return isLoaded;
        }

    }
}
