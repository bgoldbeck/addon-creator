﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using MoonSharp.Interpreter.Loaders;
using MoonSharp.Interpreter;
using System.Linq;
using System.Xml;

/// <summary>
///
/// </summary>
public class AddonCreatorAssetLoader : ScriptLoaderBase
{
    // <string, string> => <filename, filecontents>
    private Dictionary<string, string> resources = new Dictionary<string, string>();

    // <string, string> => <addon_name, files>
    private Dictionary<string, string[]> groups = new Dictionary<string, string[]>();

    /// <summary>
    /// The default path where scripts are meant to be stored (if not changed)
    /// </summary>
    public const string DEFAULT_INTERNAL_PATH = "";

    /// <summary>
    /// The default path where scripts are meant to be stored (if not changed)
    /// </summary>
    public string DEFAULT_STREAMING_ASSET_PATH = Application.streamingAssetsPath;


    public AddonCreatorAssetLoader()
    {
        LoadResources(DEFAULT_STREAMING_ASSET_PATH);
    }

    /// <summary>
    /// </summary>
    /// <param name="file"></param>
    /// <param name="globalContext"></param>
    /// <returns></returns>
    public override object LoadFile(string file, Table globalContext)
    {
        file = GetFileName(file);

        if (resources.ContainsKey(file))
        {
            return resources[file];
        }
        else
        {
            string error = string.Format(
                @"Cannot load script '{0}'. By default, scripts should be .lua files placed under {1} or {2} directories.
                If you want scripts to be put in another directory or another way, use a custom instance of UnityAssetsScriptLoader or implement
                your own IScriptLoader (possibly extending ScriptLoaderBase).",
                file, // {0}
                DEFAULT_STREAMING_ASSET_PATH, // {1}
                DEFAULT_INTERNAL_PATH); // {2}

            throw new Exception(error);
        }
    }

    /// <summary>
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public override bool ScriptFileExists(string filename)
    {
        return resources.ContainsKey(GetFileName(filename));
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="assetsPath"></param>
    private void LoadResources(string assetsPath)
    {

        return;
    }




    /// <summary>
    ///
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    private string GetFileName(string filename)
    {
        int b = Math.Max(filename.LastIndexOf('\\'), filename.LastIndexOf('/'));

        if (b > 0)
        {
            filename = filename.Substring(b + 1);
        }

        return filename;
    }

    /// <summary>
    /// Gets the list of loaded scripts filenames (useful for debugging purposes).
    /// </summary>
    /// <returns></returns>
    public string[] GetLoadedScripts()
    {
        return resources.Keys.ToArray();
    }
}
