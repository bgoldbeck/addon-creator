﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public interface ICamera
    {
        void SetTarget(Transform target);

        void AllowInput();
        void DisallowInput();
    }
}
