UnitFrames.BuffFrame = {}

UnitFrames.BuffFrame.maxBuffSlots = 6


function UnitFrames.BuffFrame.CreateNewFrame(name, width, height, parent, offsetX, offsetY)
	local buffFrame = {}
	buffFrame["buffs"] = {}
	buffFrame["buffCount"] = 0
	buffFrame["previous"] = nil
	buffFrame["size"] = height
	buffFrame["spacing"] = 5
	
	local backdrop = CanvasManager:CreateControl(UnitFrames.name .. "_" .. name .. "_Backdrop", parent, CtBackdrop)
	backdrop:SetAnchor(Top, parent, Bottom, offsetX, offsetY)
	backdrop:SetDimensions(width, height)
	backdrop:SetCenterColor(0.0, 0.0, 0.0, 0.75)
	backdrop:SetDimensions(width, height)
	backdrop:SetAlpha(0.0)
	
	buffFrame.backdrop = backdrop
	
	for i = 0, UnitFrames.BuffFrame.maxBuffSlots, 1 do
		buffFrame.buffs[i] = UnitFrames.BuffFrame.CreateNewBuffSlot(buffFrame, name, i)
	end
	return buffFrame
end

function UnitFrames.BuffFrame.CreateNewBuffSlot(frame, name, i)
	local buff = {}
	local backdrop = CanvasManager:CreateControl(UnitFrames.name .. "_Backdrop_" .. name .. i, frame.backdrop, CtBackdrop)
	
	backdrop:SetAnchor(Left, frame.backdrop, Left, ((i+1) * frame.spacing) + (i * frame.size ), 0.0)
	backdrop:SetHidden(true)
	backdrop:SetDimensions(frame.size, frame.size)
	backdrop:SetCenterColor(1.0, 1.0, 1.0, 1.0)
	
	local statusBar = CanvasManager:CreateControl(UnitFrames.name .. "_StatusBar_" .. name .. i, backdrop, CtStatusBar)
	statusBar:SetHidden(false)
	statusBar:SetDimensions(frame.size, frame.size)
	statusBar:SetAnchor(Center, backdrop, Center, 0.0, 0.0)
	statusBar:SetSliderType(SliderTypeRadial)
	statusBar:SetValue(1.0)
		
	buff.backdrop = backdrop
	buff.statusBar = statusBar
	
	buff.name = name
	buff.icon = ""
	buff.slot = i
	buff.timeLeft = 0
	buff.duration = 0
	buff.startTime = 0
	buff.endTime = 0
	
	frame.buffCount = frame.buffCount + 1
	return buff
end

function UnitFrames.BuffFrame.UpdateBuffs(frame)
	local dt = DeltaTime()
	--Print("Update Buffs")
	for i = 0, (frame.buffCount - 1), 1 do
		if (frame.buffs[i].backdrop:IsHidden() == false) then
			frame.buffs[i].timeLeft = frame.buffs[i].endTime - GetServerTimeInSeconds()
			frame.buffs[i].statusBar:SetValue(frame.buffs[i].timeLeft / frame.buffs[i].duration)
			--Print(tostring(frame.buffs[i].timeLeft / frame.buffs[i].duration))
		end
	end
end

function UnitFrames.BuffFrame.ActivateBuff(frame, effectName, unitTag, startTime, endTime, stack, iconPath)
	local buff = UnitFrames.BuffFrame.NextAvaiblableBuff(frame)
	
	if (buff ~= nil) then
		--Print("Update Buff: " .. effectName)
		buff.name = effectName
		buff.icon = iconPath
		buff.duration = endTime - startTime
		buff.timeLeft = buff.duration
		buff.startTime = startTime
		buff.endTime = endTime
		Print("endTime" .. endTime)
		Print("nowTime" .. GetServerTimeInSeconds())
		
		buff.backdrop:SetCenterTexture(iconPath)
		buff.backdrop:SetHidden(false)
		buff.statusBar:SetValue(buff.timeLeft / buff.duration)
	end
	return
end

function UnitFrames.BuffFrame.NextAvaiblableBuff(frame)
	local result = nil
	local i = 0
	Print(tostring(frame))
	while (i < frame.buffCount and result == nil) do
		if (frame.buffs[i].backdrop:IsHidden() == true) then
			frame.buffs[i].slot = i
			result = frame.buffs[i]
		end
		i = i + 1
	end
	return result
end

function UnitFrames.BuffFrame.FindBuffByName(frame, effectName)
local result = nil
	local i = 0
	while (i < frame.buffCount and result == nil) do
		if (frame.buffs[i].name == effectName) then
			result = frame.buffs[i]
		end
		i = i + 1
	end
	return result
end

function UnitFrames.BuffFrame.RemoveBuff(frame, effectName)
	local buff = UnitFrames.BuffFrame.FindBuffByName(frame, effectName)
	
	if (buff ~= nil) then
		buff.backdrop:SetHidden(true)
		UnitFrames.BuffFrame.BuffShift(frame, buff.slot)
	end
end

function UnitFrames.BuffFrame.RemoveAllBuffs(frame)

	for i = 0, (frame.buffCount - 1), 1 do
		frame.buffs[i].backdrop:SetHidden(true)
	end
end

function UnitFrames.BuffFrame.BuffShift(frame, startIndex)
	for i = startIndex, (frame.buffCount-2), 1 do
		--frame.buffs[i] = frame.buffs[i+1]
		frame.buffs[i].slot = i
		frame.buffs[i].backdrop:SetAnchor(Left, frame.backdrop, Left, ((i+1) * frame.spacing) + (i * frame.size ), 0.0)
	end
end
