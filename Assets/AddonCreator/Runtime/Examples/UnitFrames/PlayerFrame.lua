-- Initialize

UnitFrames.PlayerFrame = {}
UnitFrames.PlayerFrame.dimensions = {200.0, 100.0}
UnitFrames.PlayerFrame.statusBarHeight = 16.0
UnitFrames.PlayerFrame.statusBarOffsetY = -5


function UnitFrames.PlayerFrame.Initialize()
	-- Top Control
	local topControl = CanvasManager:CreateTopLevelControl("PlayerFrame")
	topControl:SetAnchor(
		Center, -- WhereOnMe
		Root, -- Parent
		Center, -- WhereOnTarget
		0, -- OffSetX
		-200 -- OffSetY
	) 
	topControl:SetDimensions(UnitFrames.PlayerFrame.dimensions[1], UnitFrames.PlayerFrame.dimensions[2])
	topControl:SetHidden(false)
	topControl:SetMovable(true)
	topControl:SetHandler(
		"OnMoveStop", 
		function() 
			--Print("Saving Position: " .. topControl:GetLeft() .. ":" .. topControl:GetTop())
			--UnitFrames.SavedVars.PlayerFramePosition.x = topControl:GetLeft()
			--UnitFrames.SavedVars.PlayerFramePosition.y = topControl:GetTop()
			end)
	
	-- Backdrop
	local backdrop = CanvasManager:CreateControl("PlayerFrame_Backdrop", topControl, CtBackdrop)
	backdrop:SetAnchor(Center, topControl, Center, 0.0, 0.0)
	backdrop:SetDimensions(topControl:GetWidth(), topControl:GetHeight())
	backdrop:SetCenterColor(0.0, 0.0, 0.0, 0.75)
	
	-- Center Text
	local playerNameText = CanvasManager:CreateControl("PlayerFrame_PlayerNameText", topControl, CtLabel)
	playerNameText:SetAnchor(Top, topControl, Top, 0.0, 0.0)
	playerNameText:SetDimensions(UnitFrames.TargetFrame.dimensions[1], UnitFrames.TargetFrame.dimensions[2])
	playerNameText:SetText("HELLO THERE")
	playerNameText:SetAlignment(MiddleCenter)
	playerNameText:SetDrawLevel(0)
	playerNameText:SetText(UnitManager:GetUnitName("player"))
	
	-- Health Status Bar
	local healthStatus = CanvasManager:CreateControl("PlayerFrame_HealthStatus", playerNameText, CtStatusBar)
	healthStatus:SetAnchor(Top, playerNameText, Bottom, 0, UnitFrames.PlayerFrame.statusBarOffsetY)
	healthStatus:SetDimensions(topControl:GetWidth() - 10, UnitFrames.PlayerFrame.statusBarHeight)
	healthStatus:SetColor(1.0, 0.0, 0.0, 1.0)
	healthStatus:SetTexture(UnitFrames.name .. "/Textures/tex_StatusBar.png")
	healthStatus:SetValue(1.0)
	
	-- Mana Status Bar
	local manaStatus = CanvasManager:CreateControl("PlayerFrame_ManaStatus", healthStatus, CtStatusBar)
	manaStatus:SetAnchor(Top, healthStatus, Bottom, 0, UnitFrames.PlayerFrame.statusBarOffsetY)
	manaStatus:SetDimensions(topControl:GetWidth() - 10, UnitFrames.PlayerFrame.statusBarHeight)
	manaStatus:SetColor(0.0, 0.0, 1.0, 1.0)
	manaStatus:SetTexture(UnitFrames.name .. "/Textures/tex_StatusBar.png")
	manaStatus:SetValue(1.0)
	
	-- Stamina Status Bar
	local staminaStatus = CanvasManager:CreateControl("PlayerFrame_StaminaStatus", manaStatus, CtStatusBar)
	staminaStatus:SetAnchor(Top, manaStatus, Bottom, 0, UnitFrames.PlayerFrame.statusBarOffsetY)
	staminaStatus:SetDimensions(topControl:GetWidth() - 10, UnitFrames.PlayerFrame.statusBarHeight)
	staminaStatus:SetColor(0.0, 1.0, 0.0, 1.0)
	staminaStatus:SetTexture(UnitFrames.name .. "/Textures/tex_StatusBar.png")
	staminaStatus:SetValue(1.0)
	
	UnitFrames.PlayerFrame.topControl = topControl
	UnitFrames.PlayerFrame.healthStatus = healthStatus
	UnitFrames.PlayerFrame.manaStatus = manaStatus
	UnitFrames.PlayerFrame.staminaStatus = staminaStatus
	return
end

function UnitFrames.PlayerFrame.Update()

	return
end

function UnitFrames.PlayerFrame.PowerUpdate()
	--Print("PLAYER POWER UPDATE")
	-- Set Health Status
	local health = UnitManager:GetUnitHealth("player")
	local healthMax = UnitManager:GetUnitHealthMax("player")
	UnitFrames.PlayerFrame.healthStatus:SetValue(health / healthMax)
	
	-- Set Mana Status
	local mana = UnitManager:GetUnitPower("player", PowerTypeMana)
	local manaMax = UnitManager:GetUnitPowerMax("player", PowerTypeMana)
	UnitFrames.PlayerFrame.manaStatus:SetValue(mana / manaMax)
	
	-- Set Stamina Status
	local stamina = UnitManager:GetUnitPower("player", PowerTypeStamina)
	local staminaMax = UnitManager:GetUnitPowerMax("player", PowerTypeStamina)
	UnitFrames.PlayerFrame.staminaStatus:SetValue(stamina / staminaMax)
	
	return
end

EventManager:RegisterEvent(UnitFrames.name, PowerUpdate, UnitFrames.PlayerFrame.PowerUpdate)

Print("Loading PlayerFrame In Lua")


--EVENT_MANAGER:RegisterForEvent(CUF.name, EVENT_ADD_ON_LOADED, CUF.Initialize)
