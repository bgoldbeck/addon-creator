UnitFrames.PlayerBuffFrame = {}
UnitFrames.PlayerBuffFrame.dimensions = {200.0, 24.0}


function UnitFrames.PlayerBuffFrame.Initialize()
	local buffFrame = UnitFrames.BuffFrame.CreateNewFrame(
		"PlayerBuffs", 
		UnitFrames.PlayerBuffFrame.dimensions[1], -- Width
		UnitFrames.PlayerBuffFrame.dimensions[2], -- Height
		UnitFrames.PlayerFrame.topControl, -- Parents
		0.0, -- OffsetX
		-2.0 -- OffsetY
		)
	UnitFrames.PlayerBuffFrame.frame = buffFrame
	return
end

function UnitFrames.PlayerBuffFrame.Update()
	--Print("Update")
	--Print("numBuffs: " .. UnitManager.GetNumBuffs("player"))
	if UnitManager.GetNumBuffs("player") > 0 then
		UnitFrames.BuffFrame.UpdateBuffs(UnitFrames.PlayerBuffFrame.frame)
	end
	return
end

function UnitFrames.PlayerBuffFrame.EffectChanged(changeType, effectName, unitTag, startTime, endTime, stack, iconPath)
	if (unitTag ~= "player") then
		return
	end
	--Print(unitTag .. ": " .. changeType ..": " .. effectName)
	if (changeType == EffectGained) then
		--Print("Effect Gained " .. effectName)
		UnitFrames.BuffFrame.ActivateBuff(UnitFrames.PlayerBuffFrame.frame, effectName, unitTag, startTime, endTime, stack, iconPath)
	elseif (changeType == EffectLost) then
		--Print("Effect Lost " .. effectName)
		UnitFrames.BuffFrame.RemoveBuff(UnitFrames.PlayerBuffFrame.frame, effectName)
	elseif (changeType == EffectUpdated) then
		--Print("Effect Updated " .. effectName)
	end
end

-- TODO: Add this function in C#
--ESO VERSION
--EVENT_EFFECT_CHANGED (
--	integer eventCode, 
--	integer changeType, 
--	integer effectSlot, 
--	string effectName, 
--	string unitTag, 
--	number beginTime, 
--	number endTime, 
--	integer stackCount, 
--	string iconName, 
--	string buffType, 
-- integer effectType, 
-- integer abilityType, 
-- integer statusEffectType, 
-- string unitName, 
-- integer unitId, 
-- integer abilityId, 
-- integer sourceUnitType)
-- ACTUAL
--			  DynValue.NewNumber(System.Convert.ToInt32(changeType)), // Change type
--            DynValue.NewNumber(0), // Slot
--            DynValue.NewString(item.effectName), // EffectName
--            DynValue.NewString(unitTag), // UnitTag
--            DynValue.NewNumber(item.start), // Start
--            DynValue.NewNumber(item.end), // End
--            DynValue.NewNumber(item.stack), // StackCount
--            DynValue.NewString(item.icon), // IconPath
			
EventManager:RegisterEvent(UnitFrames.name, EffectChanged, UnitFrames.PlayerBuffFrame.EffectChanged)