UnitFrames.TargetBuffFrame = {}
UnitFrames.TargetBuffFrame.dimensions = {200.0, 24.0}


function UnitFrames.TargetBuffFrame.Initialize()
	local buffFrame = UnitFrames.BuffFrame.CreateNewFrame(
		"TargetBuffs", 
		UnitFrames.TargetBuffFrame.dimensions[1], -- Width
		UnitFrames.TargetBuffFrame.dimensions[2], -- Height
		UnitFrames.TargetFrame.topControl, -- Parent
		0.0, -- OffsetX
		-2.0 -- OffsetY
		)
		
	UnitFrames.TargetBuffFrame.frame = buffFrame
	return
end

function UnitFrames.TargetBuffFrame.Update()
	
	if UnitManager.GetNumBuffs("target") > 0 then
		UnitFrames.BuffFrame.UpdateBuffs(UnitFrames.PlayerBuffFrame.frame)
	end
	return
end

function UnitFrames.TargetBuffFrame.UpdateTarget()
	Print("Updated Target: " .. UnitManager:GetUnitName("target"))
	UnitFrames.BuffFrame.RemoveAllBuffs(UnitFrames.TargetBuffFrame.frame)
	
	for i = 0, (UnitManager:GetNumBuffs("target") - 1), 1 do
		local effectName, start, finish, stack, icon = UnitManager:GetBuffInfo("target", i)
		UnitFrames.BuffFrame.ActivateBuff(UnitFrames.TargetBuffFrame.frame, effectName, "target", start, finish, stack, icon)
	end
end

function UnitFrames.TargetBuffFrame.EffectChanged(changeType, effectName, unitTag, startTime, endTime, stack, iconPath)
	if (unitTag ~= "target") then
		return
	end
	--Print(unitTag .. ": " .. changeType ..": " .. effectName)
	if (changeType == EffectGained) then
		Print("Target Effect Gained " .. effectName)
		UnitFrames.BuffFrame.ActivateBuff(UnitFrames.TargetBuffFrame.frame, effectName, unitTag, startTime, endTime, stack, iconPath)
	elseif (changeType == EffectLost) then
		Print("Target Effect Lost " .. effectName)
		UnitFrames.BuffFrame.RemoveBuff(UnitFrames.TargetBuffFrame.frame, effectName)
	elseif (changeType == EffectUpdated) then
		Print("Target Effect Updated " .. effectName)
	end
end

-- TODO: Add this function in C#
--ESO VERSION
--EVENT_EFFECT_CHANGED (
--	integer eventCode, 
--	integer changeType, 
--	integer effectSlot, 
--	string effectName, 
--	string unitTag, 
--	number beginTime, 
--	number endTime, 
--	integer stackCount, 
--	string iconName, 
--	string buffType, 
-- integer effectType, 
-- integer abilityType, 
-- integer statusEffectType, 
-- string unitName, 
-- integer unitId, 
-- integer abilityId, 
-- integer sourceUnitType)
-- ACTUAL
--			  DynValue.NewNumber(System.Convert.ToInt32(changeType)), // Change type
--            DynValue.NewNumber(0), // Slot
--            DynValue.NewString(item.effectName), // EffectName
--            DynValue.NewString(unitTag), // UnitTag
--            DynValue.NewNumber(item.start), // Start
--            DynValue.NewNumber(item.end), // End
--            DynValue.NewNumber(item.stack), // StackCount
--            DynValue.NewString(item.icon), // IconPath
			
EventManager:RegisterEvent(UnitFrames.name, UpdateTarget, UnitFrames.TargetBuffFrame.UpdateTarget)
EventManager:RegisterEvent(UnitFrames.name, EffectChanged, UnitFrames.TargetBuffFrame.EffectChanged)