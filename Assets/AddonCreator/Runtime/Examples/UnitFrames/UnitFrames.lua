-- Initialize Global Variables
MyAddon = {}
MyAddon.name = "MyAddon"
MyAddon.updateFrequency = 10.0 -- Times per second
MyAddon.updateDelay = 1.0 / MyAddon.updateFrequency
MyAddon.TimerTable = {}

Print("Loading MyAddon In Lua")


MyAddon.Default = 
{
	["MyAddonPosition"] = 
	{
		x = 350,
		y = -32,
	},
	
}

MyAddon.SavedVars = MyAddon.Default

local function DelayedBuffer(key, buffer)
	if key == nil then 
		return 
	end

	if MyAddon.TimerTable[key] == nil then 
		MyAddon.TimerTable[key] = {} 
	end
	
	MyAddon.TimerTable[key].buffer = buffer or 0.2
	MyAddon.TimerTable[key].now = GetGameTimeInSeconds()
	
	if MyAddon.TimerTable[key].last == nil then 
		MyAddon.TimerTable[key].last = MyAddon.TimerTable[key].now 
	end
	
	MyAddon.TimerTable[key].diff = MyAddon.TimerTable[key].now - MyAddon.TimerTable[key].last
	MyAddon.TimerTable[key].result = MyAddon.TimerTable[key].diff >= MyAddon.TimerTable[key].buffer
	
	if MyAddon.TimerTable[key].result then 
		MyAddon.TimerTable[key].last = MyAddon.TimerTable[key].now 
	end
	return MyAddon.TimerTable[key].result
end

function MA_Initialize()
	MyAddon.SavedVars = SavedVars.New(MyAddon.name, MyAddon.name .. "_DB", UnitManager:GetUnitName("player"), MyAddon.Default)
	
	Print("MyAddon: I am initialized!")
	return
end

function MA_Update()
	--if not DelayedBuffer(MyAddon.name, MyAddon.updateDelay) then 
		--return
	--end
	return
end

EventManager:RegisterEvent(MyAddon.name, AddonLoaded, MA_Initialize)


