-- Initialize

UnitFrames.TargetFrame = {}
UnitFrames.TargetFrame.dimensions = {205.0, 32.0}
UnitFrames.TargetFrame.targetName = ""


function UnitFrames.TargetFrame.Initialize()
	-- Top Control
	local topControl = CanvasManager:CreateTopLevelControl("TargetFrame")
	topControl:SetAnchor(
		TopLeft, -- WhereOnMe
		Root, -- Parent
		TopLeft, -- WhereOnTarget
		UnitFrames.SavedVars.TargetFramePosition.x, -- OffSetX
		UnitFrames.SavedVars.TargetFramePosition.y -- OffSetY
	) 
		
	topControl:SetDimensions(UnitFrames.TargetFrame.dimensions[1], UnitFrames.TargetFrame.dimensions[2])
	topControl:SetHidden(true)
	topControl:SetMovable(true)
	topControl:SetHandler(
		"OnMoveStop", 
		function() 
			Print("Saving Position: " .. topControl:GetLeft() .. ":" .. topControl:GetTop())
			UnitFrames.SavedVars.TargetFramePosition.x = topControl:GetLeft()
			UnitFrames.SavedVars.TargetFramePosition.y = topControl:GetTop()
			end)
	
	-- Backdrop
	local backdrop = CanvasManager:CreateControl("TargetFrame_Backdrop", topControl, CtBackdrop)
	backdrop:SetAnchor(Center, topControl, Center, 0.0, 0.0)
	backdrop:SetDimensions(UnitFrames.TargetFrame.dimensions[1], UnitFrames.TargetFrame.dimensions[2])
	backdrop:SetCenterColor(0.0, 0.0, 0.0, 1.0)

	-- Status Bar
	local statusBar = CanvasManager:CreateControl("TargetFrame_StatusBar", topControl, CtStatusBar)
	statusBar:SetAnchor(Center, topControl, Center, 0.0, 0.0)
	statusBar:SetDimensions(UnitFrames.TargetFrame.dimensions[1], UnitFrames.TargetFrame.dimensions[2])
	statusBar:SetColor(1.0, 0.0, 0.0, 1.0)
	statusBar:SetTexture(UnitFrames.name .. "/Textures/tex_StatusBar.png")
	statusBar:SetValue(1.0)

	-- Center Text
	local centerText = CanvasManager:CreateControl("TargetFrame_CenterText", topControl, CtLabel)
	centerText:SetAnchor(Center, topControl, Center, 0.0, 0.0)
	centerText:SetDimensions(UnitFrames.TargetFrame.dimensions[1], UnitFrames.TargetFrame.dimensions[2])
	centerText:SetText("HELLO THERE")
	centerText:SetAlignment(MiddleCenter)
	centerText:SetDrawLevel(0)
	
	-- Save the controls globally
	UnitFrames.TargetFrame.topControl = topControl
	UnitFrames.TargetFrame.backdrop = backdrop
	UnitFrames.TargetFrame.centerText = centerText
	UnitFrames.TargetFrame.statusBar = statusBar
	return
end

function UnitFrames.TargetFrame.Update()
	UnitFrames.TargetFrame.UpdateTargetFrameValues()
	return
end

function UnitFrames.TargetFrame.UpdateTargetFrameValues()
	if (UnitFrames.TargetFrame.targetName ~= "") then
		local health = UnitManager:GetUnitHealth("target")
		local healthMax = UnitManager:GetUnitHealthMax("target")
		UnitFrames.TargetFrame.statusBar:SetValue(health / healthMax)
	end
	return
end

-- Called when the local player has targeted or untarget something.
function UnitFrames.TargetFrame.UpdateTargetName()
	
	UnitFrames.TargetFrame.targetName = UnitManager:GetUnitName("target")
	
	if (target ~= "") then
		UnitFrames.TargetFrame.topControl:SetHidden(false)
		UnitFrames.TargetFrame.centerText:SetText(UnitFrames.TargetFrame.targetName)
	else
		UnitFrames.TargetFrame.topControl:SetHidden(true)
	end
	return
end


function UnitFrames.TargetFrame.PowerUpdate(unitTag, powerType, value, valueMax)
	if powerType == PowerTypeHealth then
		UnitFrames.TargetFrame.UpdateTargetFrameValues()
	end
	
	return
end

function UnitFrames.TargetFrame.UpdateTarget()
	UnitFrames.TargetFrame.UpdateTargetName()
	UnitFrames.TargetFrame.UpdateTargetFrameValues()
	return
end

EventManager:RegisterEvent(UnitFrames.name, UpdateTarget, UnitFrames.TargetFrame.UpdateTarget)
EventManager:RegisterEvent(UnitFrames.name, PowerUpdate, UnitFrames.TargetFrame.PowerUpdate)

Print("Loading TargetFrame In Lua")


--EVENT_MANAGER:RegisterForEvent(CUF.name, EVENT_ADD_ON_LOADED, CUF.Initialize)
