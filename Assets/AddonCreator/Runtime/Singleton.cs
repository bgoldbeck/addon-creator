﻿using UnityEngine;

namespace AddonCreator
{
	/// <summary>
	/// Inherit from this base class to create a singleton.
	/// e.g. public class MyClassName : Singleton<MyClassName> {}
	/// </summary>
	public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		// Check to see if we're about to be destroyed.
		private static bool shuttingDown = false;
		private static object lockObject = new object();
		private static T instance;


		/// <summary>
		/// Access singleton instance through this propriety.
		/// </summary>
		public static T Instance()
		{
			if (shuttingDown)
			{
				Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
					"' already destroyed. Returning null.");
				return null;
			}

			lock (lockObject)
			{
				if (instance == null)
				{
					// Search for existing instance.
					instance = (T)FindObjectOfType(typeof(T));

					// Create new instance if one doesn't already exist.
					if (instance == null)
					{
						// Need to create a new GameObject to attach the singleton to.
						GameObject singletonObject = new GameObject();
						instance = singletonObject.AddComponent<T>();
						singletonObject.name = typeof(T).ToString();
					}
				}
				return instance;
			}

		}

		public static void Destroy()
		{
			if (instance != null)
			{
				GameObject.Destroy(instance);
				instance = null;
			}
			return;
		}

		/// <summary>
		///
		/// </summary>
		private void OnApplicationQuit()
		{
			Destroy();
			shuttingDown = true;
			return;
		}

		/// <summary>
		///
		/// </summary>
		private void OnDestroy()
		{
			Destroy();
			shuttingDown = true;
			return;
		}
	}
}
