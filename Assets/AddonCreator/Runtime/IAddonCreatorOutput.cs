﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>
    public interface IDebugOutput
    {
        void Log(string output);
        void LogWarning(string output);
        void LogError(string output);
    }

}
