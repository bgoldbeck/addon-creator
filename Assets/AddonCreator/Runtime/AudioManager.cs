﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


namespace AddonCreator
{
    /// <summary>
    ///
    /// </summary>]
    public class AudioManager : Singleton<AudioManager>
    {
        private static string masterSaveKey = "MasterVolume";
        private static string sfxSaveKey = "SfxVolume";
        private static string musicSaveKey = "MusicVolume";

        [SerializeField]
        private AudioMixer audioMixer = null;

        [SerializeField, Range(-80f, 20f)]
        private float masterVolume = 0f;

        [SerializeField, Range(-80f, 20f)]
        private float sfxVolume = 0f;

        [SerializeField, Range(-80f, 20f)]
        private float musicVolume = 0f;


        /// <summary>
        ///
        /// </summary>
        public float MasterVolume
        {
            get
            {
                return masterVolume;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public float SfxVolume
        {
            get
            {
                return sfxVolume;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public float MusicVolume
        {
            get
            {
                return musicVolume;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Start()
        {
            LoadPreferences();

            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void OnDestroy()
        {
            SavePreferences();
            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void LoadPreferences()
        {
            audioMixer = Resources.Load("AudioMixer") as AudioMixer;

            if (PlayerPrefs.HasKey(masterSaveKey))
            {
                masterVolume = PlayerPrefs.GetFloat(masterSaveKey);
            }

            if (PlayerPrefs.HasKey(sfxSaveKey))
            {
                sfxVolume = PlayerPrefs.GetFloat(sfxSaveKey);
            }

            if (PlayerPrefs.HasKey(musicSaveKey))
            {
                musicVolume = PlayerPrefs.GetFloat(musicSaveKey);
            }

            AudioListener.volume = 1f;


            if (audioMixer != null)
            {
                // UNITY_BUG: AudioMixer.SetFloat will not work in Unity Awake() function.
                if (!audioMixer.SetFloat(masterSaveKey, MasterVolume) ||
                    !audioMixer.SetFloat(sfxSaveKey, SfxVolume) ||
                    !audioMixer.SetFloat(musicSaveKey, MusicVolume))
                {
                    Debug.LogWarning("Couldnt adjust volume paramters.");
                }
            }
            else
            {
                Debug.LogWarning("Missing audio mixer.");
            }
            return;
        }

        /// <summary>
        ///
        /// </summary>
        public void SavePreferences()
        {

            Debug.Log("Saving player audio settings.");
            PlayerPrefs.SetFloat(masterSaveKey, MasterVolume);
            PlayerPrefs.SetFloat(sfxSaveKey, SfxVolume);
            PlayerPrefs.SetFloat(musicSaveKey, MusicVolume);
            PlayerPrefs.Save();
            return;
        }
    }
}
