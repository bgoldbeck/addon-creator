﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public class DebugTarget : IDebugOutput
    {
        public DebugTarget()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public void Log(string output)
        {
            Debug.Log(output);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public void LogWarning(string output)
        {
            Debug.LogWarning(output);
            return;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="output"></param>
        public void LogError(string output)
        {
            Debug.LogError(output);
            return;
        }

    }
}
