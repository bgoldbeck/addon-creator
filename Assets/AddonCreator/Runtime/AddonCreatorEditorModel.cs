﻿using AddonCreator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Holds the data for the AddonCreatorEditor Inspector.
/// </summary>
public class AddonCreatorEditorModel
{
    /// <summary>
    /// All the proxy data serialized on disk.
    /// </summary>
    protected ProxyLibrary proxyLibrary;


    public AddonCreatorEditorModel()
    {

    }

    public ProxyLibrary GetProxyLibrary()
    {
        if (proxyLibrary == null)
        {
            proxyLibrary = Resources.Load("ProxyLibrary") as ProxyLibrary;
        }
        return proxyLibrary;
    }

}
