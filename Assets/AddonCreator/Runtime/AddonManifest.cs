﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
	/// <summary>
	/// A collection of metadata detailing the context of an Addon.
	/// </summary>
	public class AddonManifest : IComparable<AddonManifest>, IEquatable<AddonManifest>
	{
		/// <summary>
		/// Title of the addon given by the addon author as read by the xml file.
		/// </summary>
		public string title = "";

		/// <summary>
		/// All the associated lua scripts for this addon.
		/// </summary>
		public List<AddonModule> modules = new List<AddonModule>();

		/// <summary>
		/// A list of addons required for this addon to function.
		/// </summary>
		public List<AddonDependency> dependencies = new List<AddonDependency>();

		/// <summary>
		/// Path to the addon's XML data.
		/// </summary>
		public string xmlFilepath = "";

		/// <summary>
		/// A Code that is intended to identify the correct addon version as read from the xml file.
		/// The addon system can require addons have a specific magic string attached to the xml file.
		/// </summary>
		public string api = "";

		/// <summary>
		/// The version of the addon as read by the xml file.
		/// </summary>
		public string version = "";

		/// <summary>
		/// The date of the addon by the addon's author as read by the xml file.
		/// </summary>
		public string date = "";

		/// <summary>
		/// The author's name as read by the xml file.
		/// </summary>
		public string author = "";

		/// <summary>
		/// The email of the author as read by the xml file.
		/// </summary>
		public string email = "";

		/// <summary>
		/// The description of the addon by the addon author as read by the xml file.
		/// </summary>
		public string description = "";

		/// <summary>
		/// Determine if the addon is a libary as read by the xml file.
		/// </summary>
		public bool isLibrary = false;

		/// <summary>
		/// Set to true if this addon was successfully loaded.
		/// </summary>
		public bool isLoaded = false;


		public AddonManifest()
		{
		}

		public int CompareTo(AddonManifest other)
		{
			if (other != null)
			{
				if (this.xmlFilepath != null)
				{
					return this.xmlFilepath.CompareTo(other.xmlFilepath);
				}
			}
			return 0;
		}

		public bool Equals(AddonManifest other)
		{
			if (other != null)
			{
				return this.xmlFilepath.Equals(other.xmlFilepath);
			}
			return false;
		}

		public bool Equals(AddonManifest first, AddonManifest second)
		{
			if (ReferenceEquals(first, second))
			{
				return true;
			}
			else if (first is null || second is null)
			{
				return false;
			}
			// Adjust according to requirements.
			return StringComparer.InvariantCultureIgnoreCase.Equals(first.xmlFilepath, second.xmlFilepath);
		}

		public int GetHashCode(AddonManifest manifest)
		{
			return StringComparer.InvariantCultureIgnoreCase.GetHashCode(manifest.xmlFilepath);
		}

		public override string ToString()
		{
			return string.Format("Title: {0}\nXML Filepath: {1}\nAPI Version: {2}\nDate: {3}\nAuthor: {4}\nEmail: {5}\nDescription: {6}\nIsLibrary: {7}\nIsLoaded: {8}",
				title, xmlFilepath, api, date, author, email, description, isLibrary, isLoaded);
		}

	}
}
