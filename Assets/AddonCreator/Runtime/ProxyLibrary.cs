﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Linq;


namespace AddonCreator
{
	[Serializable]
	public class ToggledProxyDictionary : SerializableDictionary<string, bool> { }


	/// <summary>
	///
	/// </summary>
	[Serializable]
	[CreateAssetMenu(fileName = "ProxyLibrary", menuName = "ProxyLibrary", order = 1)]
	public class ProxyLibrary : ScriptableObject
	{
		[SerializeField]
		private ToggledProxyDictionary proxyDictionary = ToggledProxyDictionary.New<ToggledProxyDictionary>();

		public bool CanEnable(Type proxyType)
		{
			// We can only enable a proxy if all its dependencies are also enabled.
			// So, get all the dependencies and check for any that are not enabled.
			return GetProxyDependenciesForType(proxyType).Where(x => !IsEnabled(x)).Count() == 0;
		}

		public bool IsEnabled(Type proxyType)
		{
			string proxyKey = GetProxyKey(proxyType);

			if (!proxyDictionary.dictionary.ContainsKey(proxyKey))
			{
				return false;
			}
			return proxyDictionary.dictionary[proxyKey];
		}

		public void SetProxyEnabled(Type proxyType, bool enabled)
		{
			string proxyKey = GetProxyKey(proxyType);

			if (!proxyDictionary.dictionary.ContainsKey(proxyKey))
			{
				proxyDictionary.dictionary.Add(proxyKey, enabled);
			}
			else
			{
				proxyDictionary.dictionary[proxyKey] = enabled;
			}

			return;
		}

		private string GetProxyKey(Type t)
		{
			return t.Name;
		}

		public static bool HasAttribute<T>(Type t) where T : Attribute
		{
			return Attribute.GetCustomAttribute(t, typeof(T)) != null;
		}

		public static List<Type> GetProxyRegisterTypesForType(Type proxyType)
		{
			AddonCreatorAttribute attribute = GetProxyAddonCreatorAttribute(proxyType);
			return attribute?.RegisterTypes.ToList();
		}

		public static List<Type> GetProxyRegisterTypesForType<T>()
		{
			return GetProxyRegisterTypesForType(typeof(T));
		}

		public static List<Type> GetProxyDependenciesForType(Type proxyType)
		{
			AddonCreatorAttribute attribute = GetProxyAddonCreatorAttribute(proxyType);
			return attribute?.Dependencies.ToList();
		}


		public static List<Type> GetAllProxyDependenciesForType<T>()
		{
			return GetProxyDependenciesForType(typeof(T));
		}

		/// <summary>
		/// Use reflection to check the assembly for all objects that are proxy types.
		/// </summary>
		/// <returns>A List<Type> where all the types are subclasses of ProxyObject.</returns>
		public static List<Type> GetAllProxyTypes()
		{
			List<Type> proxyObjects = new List<Type>();

			foreach (Type type in Assembly.GetAssembly(typeof(ProxyObject)).GetTypes()
				.Where(theType => theType.IsClass && !theType.IsAbstract && theType.IsSubclassOf(typeof(ProxyObject))))
			{
				proxyObjects.Add(type);
			}

			return proxyObjects;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="proxyType"></param>
		/// <returns></returns>
		public static AddonCreatorAttribute GetProxyAddonCreatorAttribute(Type proxyType)
		{
			return Attribute.GetCustomAttribute(proxyType, typeof(AddonCreatorAttribute)) as AddonCreatorAttribute;
		}

	}
}
