﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{

    /// <summary>
    ///
    /// </summary>
    public class UIElement : MonoBehaviour
    {
        private RectTransform rectTransform = null;


        public ControlType ControlType { get; set; }


        private List<DynValue> onMoveEndCallbacks;
        private List<DynValue> onMoveStartCallbacks;


        public RectTransform RectTransform
        {
            get
            {
                return rectTransform;
            }
            protected set
            {
                rectTransform = value;
                return;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public virtual void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            rectTransform = rectTransform.ZeroCenter(); // Center anchor and zero position.

            return;
        }

    }

}
