﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public interface IAddonCreatorBehavior
    {
        void Start();
        void Update();
    }
}
