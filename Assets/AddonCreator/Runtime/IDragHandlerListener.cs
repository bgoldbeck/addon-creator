﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AddonCreator
{
    public interface IDragHandlerListener
    {
        void OnDragEnd();
        void OnDragStart();
    }
}
