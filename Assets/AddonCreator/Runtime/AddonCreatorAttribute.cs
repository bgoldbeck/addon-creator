﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;


namespace AddonCreator
{
    public class AddonCreatorAttribute : Attribute
    {
        private bool hideInInspector;
        private string category;
        private string description;
        private Type[] dependencies;
        private Type[] registerTypes;


        /// <summary>
        ///
        /// </summary>
        /// <param name="category"></param>
        /// <param name="description"></param>
        public AddonCreatorAttribute(string category, string description = "", Type[] dependencies = null, Type[] registerTypes = null, bool hideInInspector = false)
        {
            this.category = category;
            this.description = description;
            this.hideInInspector = hideInInspector;
            this.dependencies = dependencies;
            this.registerTypes = registerTypes;

            if (dependencies == null)
            {
                this.dependencies = new Type[] { };
            }

            if (registerTypes == null)
            {
                this.registerTypes = new Type[] { };
            }
        }

        public string Category
        {
            get { return category; }
        }

        public string Description
        {
            get { return description; }
        }

        public bool HideInInspector
        {
            get { return hideInInspector; }
        }

        public Type[] Dependencies
        {
            get { return dependencies; }
        }

        public Type[] RegisterTypes
        {
            get { return registerTypes; }
        }
    }
}

