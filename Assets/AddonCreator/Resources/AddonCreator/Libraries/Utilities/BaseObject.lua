local BaseObject = {}
BaseObject.__index = BaseObject

function BaseObject:New()
    return setmetatable({}, self)
end

---
-- Call to create a new class that inherits from this one.
--
function BaseObject:Subclass()
    local newClass = setmetatable({}, self)
    newClass.__index = newClass
    return newClass
end

